/*
 * Copyright 2023 Valve Corporation
 * Copyright 2023 Ella Stanforth
 * based on vk_meta_blit_resolve.c
 * Copyright 2022 Collabora Ltd
 *
 * SPDX-License-Identifier: MIT
 */

#include "nir.h"
#include "vk_buffer.h"
#include "vk_command_buffer.h"
#include "vk_device.h"
#include "vk_image.h"
#include "vk_meta.h"
#include "vk_meta_private.h"
#include "vk_pipeline.h"

#include "compiler/glsl_types.h"
#include "compiler/nir_types.h"
#include "util/format/u_format.h"
#include "vulkan/util/vk_format.h"
#include "vulkan/vulkan_core.h"

#include "nir_builder.h"

struct vk_meta_copy_key {
   enum vk_meta_object_key_type key_type;
   enum glsl_sampler_dim dim;
   VkFormat dst_format;
   VkFormat src_format;
   VkImageAspectFlags aspects;
};

static VkResult
get_copy_descriptor_set_layout(struct vk_device *device,
                               VkDescriptorSetLayout *out_layout,
                               bool from_image)
{
   const struct vk_device_dispatch_table *disp = &device->dispatch_table;

   VkDescriptorSetLayoutBinding bindings[2];
   unsigned binding_count = from_image ? 2 : 1;

   for (unsigned i = 0; i < binding_count; ++i) {
      bindings[i] = (const VkDescriptorSetLayoutBinding){
         .binding = i,
         .descriptorCount = 1,
         .descriptorType = from_image ? VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE
                                      : VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER,
         .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
      };
   }

   const VkDescriptorSetLayoutCreateInfo info = {
      .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
      .bindingCount = binding_count,
      .pBindings = bindings,
   };

   return disp->CreateDescriptorSetLayout(vk_device_to_handle(device), &info,
                                          NULL, out_layout);
}

struct vk_meta_copy_push_data {
   int32_t x_off;
   int32_t y_off;
   int32_t z_off;
   uint32_t buffer_offset;
   uint32_t buffer_stride;
} PACKED;

#define get_push(b, name)                                                      \
   nir_load_push_constant(                                                     \
      b, 1, sizeof(((struct vk_meta_copy_push_data *)0)->name) * 8,            \
      nir_imm_int(b, offsetof(struct vk_meta_copy_push_data, name)))

static VkResult
get_copy_pipeline_layout(struct vk_device *device,
                         VkDescriptorSetLayout set_layout,
                         VkPipelineLayout *out_layout)
{
   const struct vk_device_dispatch_table *disp = &device->dispatch_table;
   const VkPushConstantRange push_range = {
      .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
      .offset = 0,
      .size = sizeof(struct vk_meta_copy_push_data),
   };

   const VkPipelineLayoutCreateInfo info = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
      .setLayoutCount = 1,
      .pSetLayouts = &set_layout,
      .pushConstantRangeCount = 1,
      .pPushConstantRanges = &push_range,
   };

   return disp->CreatePipelineLayout(vk_device_to_handle(device), &info, NULL,
                                     out_layout);
}

static nir_shader *
build_copy_shader(struct vk_meta_copy_key *key)
{
   nir_builder build = nir_builder_init_simple_shader(MESA_SHADER_FRAGMENT,
                                                      NULL, "vk-meta-copy");
   nir_builder *b = &build;

   nir_def *offset_xyz = nir_load_push_constant(
      b, 3, 32, nir_imm_int(b, offsetof(struct vk_meta_copy_push_data, x_off)));

   nir_def *offset_xy = nir_trim_vector(b, offset_xyz, 2);
   nir_def *dest_coord_xy =
      nir_f2u32(b, nir_trim_vector(b, nir_load_frag_coord(b), 2));

   nir_def *src_coord_xy = nir_iadd(b, dest_coord_xy, offset_xy);
   nir_def *src_coord;

   if (key->dim == GLSL_SAMPLER_DIM_BUF) {
      nir_def *x = nir_channel(b, src_coord_xy, 0);
      nir_def *y = nir_channel(b, src_coord_xy, 1);

      src_coord = nir_iadd(b, nir_imul(b, y, get_push(b, buffer_stride)), x);
      src_coord = nir_iadd(b, src_coord, get_push(b, buffer_offset));
   } else {
      src_coord = src_coord_xy;
   }

   u_foreach_bit(a, key->aspects) {
      VkImageAspectFlagBits aspect = (1 << a);

      enum glsl_base_type base_type;
      unsigned out_location, out_comps;
      const char *tex_name, *out_name;
      unsigned binding = 0;

      switch (aspect) {
      case VK_IMAGE_ASPECT_COLOR_BIT:
         tex_name = "color_tex";
         if (vk_format_is_int(key->dst_format))
            base_type = GLSL_TYPE_INT;
         else if (vk_format_is_uint(key->dst_format))
            base_type = GLSL_TYPE_UINT;
         else
            base_type = GLSL_TYPE_FLOAT;
         out_name = "gl_FragData[0]";
         out_location = FRAG_RESULT_DATA0;
         out_comps = 4;
         out_comps = vk_format_get_nr_components(key->dst_format);
         break;
      case VK_IMAGE_ASPECT_DEPTH_BIT:
         tex_name = "depth_tex";
         base_type = GLSL_TYPE_FLOAT;
         out_name = "gl_FragDepth";
         out_location = FRAG_RESULT_DEPTH;
         out_comps = 1;
         break;
      case VK_IMAGE_ASPECT_STENCIL_BIT:
         tex_name = "stencil_tex";
         base_type = GLSL_TYPE_UINT;
         out_name = "gl_FragStencilRef";
         out_location = FRAG_RESULT_STENCIL;
         out_comps = 1;
         binding = 1;
         break;
      default:
         unreachable("Unsupported aspect");
      }
      const bool is_array = false && key->dim != GLSL_SAMPLER_DIM_3D;
      const struct glsl_type *texture_type =
         glsl_sampler_type(key->dim, false, is_array, base_type);

      nir_variable *texture = nir_variable_create(b->shader, nir_var_uniform,
                                                  texture_type, tex_name);
      texture->data.descriptor_set = 0;
      texture->data.binding = binding;

      nir_def *texel =
         nir_txf_deref(b, nir_build_deref_var(b, texture), src_coord, NULL);

      /* We know the source format, so annotate the texture instruction with it
       * in case it lets backends optimize. This is load-bearing on AGX.
       */
      nir_instr_as_tex(texel->parent_instr)->format =
         vk_format_to_pipe_format(key->src_format);

      const struct glsl_type *out_type = glsl_vector_type(base_type, out_comps);
      nir_variable *out =
         nir_variable_create(b->shader, nir_var_shader_out, out_type, out_name);
      out->data.location = out_location;
      nir_def *out_tex = nir_trim_vector(b, texel, out_comps);
      nir_store_var(b, out, out_tex, BITFIELD_MASK(out_comps));
   }
   return b->shader;
}

static VkResult
get_copy_pipeline(struct vk_device *device, struct vk_meta_device *meta,
                  VkPipelineLayout layout, struct vk_meta_copy_key *key,
                  VkPipeline *out_pipeline)
{
   const VkPipelineShaderStageNirCreateInfoMESA fs_nir_info = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_NIR_CREATE_INFO_MESA,
      .nir = build_copy_shader(key),
   };
   const VkPipelineShaderStageCreateInfo fs_info = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
      .pNext = &fs_nir_info,
      .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
      .pName = "main",
   };
   VkPipelineDepthStencilStateCreateInfo ds_info = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
   };
   struct vk_meta_rendering_info render = {
      .samples = 1,
   };
   if (key->aspects & VK_IMAGE_ASPECT_COLOR_BIT) {
      render.color_attachment_count = 1;
      render.color_attachment_formats[0] = key->dst_format;
   }
   if (key->aspects & VK_IMAGE_ASPECT_DEPTH_BIT) {
      ds_info.depthTestEnable = VK_TRUE;
      ds_info.depthWriteEnable = VK_TRUE;
      ds_info.depthCompareOp = VK_COMPARE_OP_ALWAYS;
      render.depth_attachment_format = key->dst_format;
   }
   if (key->aspects & VK_IMAGE_ASPECT_STENCIL_BIT) {
      ds_info.stencilTestEnable = VK_TRUE;
      ds_info.front.compareOp = VK_COMPARE_OP_ALWAYS;
      ds_info.front.passOp = VK_STENCIL_OP_REPLACE;
      ds_info.front.compareMask = ~0u;
      ds_info.front.writeMask = ~0u;
      ds_info.back = ds_info.front;
      render.stencil_attachment_format = key->dst_format;
   }

   const VkGraphicsPipelineCreateInfo info = {
      .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
      .stageCount = 1,
      .pStages = &fs_info,
      .pDepthStencilState = &ds_info,
      .layout = layout,
   };

   return vk_meta_create_graphics_pipeline(device, meta, &info, &render, key,
                                           sizeof(*key), out_pipeline);
}

static void
do_copy(struct vk_command_buffer *cmd, struct vk_meta_device *meta,
        VkFormat src_format, struct vk_image *dst_image, VkFormat dst_format,
        VkImageLayout dst_image_layout, const VkImageCopy2 *region,
        uint32_t desc_count, VkWriteDescriptorSet *desc_writes,
        unsigned buffer_stride, unsigned buffer_offset, bool from_image)
{
   struct vk_device *device = cmd->base.device;
   const struct vk_device_dispatch_table *disp = &device->dispatch_table;
   VkResult result;

   VkDescriptorSetLayout set_layout;
   result = get_copy_descriptor_set_layout(device, &set_layout, from_image);
   if (result != VK_SUCCESS) {
      vk_command_buffer_set_error(cmd, result);
      return;
   }

   VkPipelineLayout pipeline_layout;
   result = get_copy_pipeline_layout(device, set_layout, &pipeline_layout);
   if (result != VK_SUCCESS) {
      vk_command_buffer_set_error(cmd, result);
      return;
   }

   VkImageAspectFlags aspects = region->dstSubresource.aspectMask;

   struct vk_meta_copy_push_data push = {
      .x_off = region->srcOffset.x - region->dstOffset.x,
      .y_off = region->srcOffset.y - region->dstOffset.y,
      .buffer_stride = buffer_stride,
      .buffer_offset = buffer_offset,
   };

   struct vk_meta_copy_key key = {
      .key_type = VK_META_OBJECT_KEY_COPY_IMAGE_PIPELINE,
      .dim = from_image ? GLSL_SAMPLER_DIM_2D : GLSL_SAMPLER_DIM_BUF,
      .dst_format = dst_format,
      .src_format = src_format,
      .aspects = aspects,
   };

   uint32_t dst_base_layer, dst_layer_count;
   if (dst_image->image_type == VK_IMAGE_TYPE_3D) {
      assert(!"TODO");
   } else {
      dst_base_layer = region->dstSubresource.baseArrayLayer;
      dst_layer_count = region->dstSubresource.layerCount;
      // push.arr_delta = region->dstSubresource.baseArrayLayer -
      //                  region->srcSubresource.baseArrayLayer;
   }

   VkImageView dst_view;
   const VkImageViewUsageCreateInfo dst_view_usage = {
      .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO,
      .usage = (aspects & VK_IMAGE_ASPECT_COLOR_BIT)
                  ? VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT
                  : VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
   };
   const VkImageViewCreateInfo dst_view_info = {
      .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
      .pNext = &dst_view_usage,
      .image = vk_image_to_handle(dst_image),
      .viewType = vk_image_render_view_type(dst_image, dst_layer_count),
      .format = dst_format,
      .subresourceRange =
         {
            .aspectMask = region->dstSubresource.aspectMask,
            .baseMipLevel = region->dstSubresource.mipLevel,
            .levelCount = 1,
            .baseArrayLayer = dst_base_layer,
            .layerCount = dst_layer_count,
         },
   };

   result = disp->CreateImageView(vk_device_to_handle(device), &dst_view_info,
                                  NULL, &dst_view);
   if (result != VK_SUCCESS) {
      vk_command_buffer_set_error(cmd, result);
      return;
   }

   struct vk_meta_rect dst_rect = {
      .x0 = region->dstOffset.x,
      .y0 = region->dstOffset.y,
      .x1 = region->dstOffset.x + region->extent.width,
      .y1 = region->dstOffset.y + region->extent.height,
   };

   const VkRenderingAttachmentInfo vk_att = {
      .sType = VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO,
      .imageView = dst_view,
      .imageLayout = dst_image_layout,
      /* TODO: Optimize */
      .loadOp = VK_ATTACHMENT_LOAD_OP_LOAD,
      .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
   };
   VkRenderingInfo vk_render = {
      .sType = VK_STRUCTURE_TYPE_RENDERING_INFO,
      .renderArea =
         {
            .offset = {region->dstOffset.x, region->dstOffset.y},
            .extent = {dst_image->extent.width, dst_image->extent.height},
         },
      .layerCount = dst_layer_count,
   };

   if (aspects & VK_IMAGE_ASPECT_COLOR_BIT) {
      vk_render.colorAttachmentCount = 1;
      vk_render.pColorAttachments = &vk_att;
   }
   if (aspects & VK_IMAGE_ASPECT_DEPTH_BIT)
      vk_render.pDepthAttachment = &vk_att;
   if (aspects & VK_IMAGE_ASPECT_STENCIL_BIT)
      vk_render.pStencilAttachment = &vk_att;

   disp->CmdBeginRendering(vk_command_buffer_to_handle(cmd), &vk_render);

   VkPipeline pipeline;
   result = get_copy_pipeline(device, meta, pipeline_layout, &key, &pipeline);
   if (result != VK_SUCCESS) {
      vk_command_buffer_set_error(cmd, result);
      return;
   }

   disp->CmdBindPipeline(vk_command_buffer_to_handle(cmd),
                         VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

   disp->CmdPushDescriptorSetKHR(vk_command_buffer_to_handle(cmd),
                                 VK_PIPELINE_BIND_POINT_GRAPHICS,
                                 pipeline_layout, 0, desc_count, desc_writes);

   disp->CmdPushConstants(vk_command_buffer_to_handle(cmd), pipeline_layout,
                          VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(push), &push);

   /*
    * Scissor to the render area to satisfy Vulkan spec text:
    *
    *    The application must ensure (using scissor if necessary) that all
    *    rendering is contained within the render area.
    */
   disp->CmdSetScissor(vk_command_buffer_to_handle(cmd), 0, 1,
                       &vk_render.renderArea);

   meta->cmd_draw_volume(cmd, meta, &dst_rect, dst_layer_count);

   disp->CmdEndRendering(vk_command_buffer_to_handle(cmd));
}

void
vk_meta_copy_image(struct vk_command_buffer *cmd, struct vk_meta_device *meta,
                   struct vk_image *src_image, VkFormat src_format,
                   VkImageLayout src_image_layout, struct vk_image *dst_image,
                   VkFormat dst_format, VkImageLayout dst_image_layout,
                   uint32_t region_count, const VkImageCopy2 *regions)
{
   unreachable("TODO: this path is broken");
#if 0
   for (unsigned r = 0; r < region_count; ++r) {
      const VkImageCopy2 *region = &regions[r];

      uint32_t desc_count = 0;
      VkDescriptorImageInfo image_infos[2];
      VkWriteDescriptorSet desc_writes[2];

      u_foreach_bit(a, region->srcSubresource.aspectMask) {
         VkImageAspectFlagBits aspect = (1 << a);

         VkImageView src_view;
         const VkImageViewUsageCreateInfo src_view_usage = {
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO,
            .usage = VK_IMAGE_USAGE_SAMPLED_BIT,
         };
         const VkImageViewCreateInfo src_view_info = {
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .pNext = &src_view_usage,
            .image = vk_image_to_handle(src_image),
            .viewType = vk_image_sampled_view_type(src_image),
            // Must have been created with MUTABLE_FORMAT to do this.
            // instead we must pack in the shader
            .format = dst_format,
            // .format = src_format,
            .subresourceRange =
               {
                  .aspectMask = aspect,
                  .baseMipLevel = region->srcSubresource.mipLevel,
                  .levelCount = 1,
                  .baseArrayLayer = region->srcSubresource.baseArrayLayer,
                  .layerCount = region->srcSubresource.layerCount,
               },
         };
         VkResult result =
            vk_meta_create_image_view(cmd, meta, &src_view_info, &src_view);
         if (unlikely(result != VK_SUCCESS)) {
            vk_command_buffer_set_error(cmd, result);
            return;
         }

         image_infos[desc_count] = (VkDescriptorImageInfo){
            .imageView = src_view,
         };
         desc_writes[desc_count] = (VkWriteDescriptorSet){
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
            .descriptorCount = 1,
            .pImageInfo = &image_infos[desc_count],
         };
         desc_count++;
      }

      do_copy(cmd, meta, src_format, dst_image, dst_format, dst_image_layout,
              &regions[r], desc_count, desc_writes, 0, 0, true);
   }
#endif
}

void
vk_meta_copy_buffer_to_image2(struct vk_command_buffer *cmd,
                              struct vk_meta_device *meta,
                              const struct VkCopyBufferToImageInfo2 *info)
{
   VK_FROM_HANDLE(vk_image, image, info->dstImage);

   /* We use a common format for both sides of the copy, eliminating shader
    * conversions for this path.
    */
   VkFormat format = image->format;

   for (unsigned r = 0; r < info->regionCount; ++r) {
      const VkBufferImageCopy2 *region = &info->pRegions[r];

      uint32_t desc_count = 0;
      VkWriteDescriptorSet desc_writes[2];

      unsigned src_width = region->bufferRowLength ?: region->imageExtent.width;
      unsigned src_height =
         region->bufferImageHeight ?: region->imageExtent.height;

      /* Clamp the height to what's actually necessary. CTS tests this. */
      src_height = MIN2(src_height, region->imageExtent.height);

      uint32_t blocksize_B =
         util_format_get_blocksize(vk_format_to_pipe_format(format));

      assert(region->imageExtent.depth == 1 && "TODO");

      /* Create a view into the source buffer as a texel buffer */
      const VkBufferViewCreateInfo src_view_info = {
         .sType = VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO,
         .buffer = info->srcBuffer,
         .format = format,

         /* Ideally, this would be region->bufferOffset, but that might not be
          * aligned to minTexelBufferOffsetAlignment. Instead, we use a 0 offset
          * (which is definitely aligned) and add the offset ourselves in the
          * shader.
          */
         .offset = 0,

         /* Include the offset in the range since we add the offset in shader.
          * For the size of the copy itself, the last row is smaller than the
          * stride. The CTS tests this.
          */
         .range = region->bufferOffset +
                  blocksize_B * ((src_width * (src_height - 1)) +
                                 region->imageExtent.width),
      };

      /* Vulkan guarantees this for non-planar, non-Z/S resources.
       *
       * XXX: Handle planar and Z/S.
       */
      assert((region->bufferOffset % blocksize_B) == 0 && "must be aligned");
      uint32_t offset_el = region->bufferOffset / blocksize_B;

      VkBufferView src_view;
      VkResult result =
         vk_meta_create_buffer_view(cmd, meta, &src_view_info, &src_view);
      if (unlikely(result != VK_SUCCESS)) {
         vk_command_buffer_set_error(cmd, result);
         return;
      }

      desc_writes[desc_count] = (VkWriteDescriptorSet){
         .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
         .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER,
         .descriptorCount = 1,
         .pTexelBufferView = &src_view,
      };
      desc_count++;

      /* Construct a VkImageCopy2 descriptor for the copy */
      VkImageCopy2 image_region = {
         .sType = VK_STRUCTURE_TYPE_IMAGE_COPY_2,
         .dstSubresource = region->imageSubresource,
         .dstOffset = region->imageOffset,
         .extent = region->imageExtent,

         /* Buffer */
         .srcSubresource =
            {
               .baseArrayLayer = 0,
               .layerCount = 1,
               .mipLevel = 0,
            },
         .srcOffset = {0, 0, 0},
      };

      if (region->imageSubresource.layerCount > 1)
         unreachable("todo");

      do_copy(cmd, meta, format, image, format, info->dstImageLayout,
              &image_region, desc_count, desc_writes, src_width, offset_el,
              false);
   }
}
