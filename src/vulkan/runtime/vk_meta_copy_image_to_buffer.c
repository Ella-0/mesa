/*
 * Copyright 2023 Valve Corporation
 * Copyright 2022 Collabora Ltd
 * SPDX-License-Identifier: MIT
 */

#include "compiler/glsl_types.h"
#include "compiler/nir_types.h"
#include "compiler/shader_enums.h"
#include "mesa/program/prog_parameter.h"
#include "util/bitscan.h"
#include "util/u_math.h"
#include "vulkan/vulkan_core.h"
#include "vk_buffer.h"
#include "vk_command_pool.h"
#include "vk_meta_private.h"

#include "vk_command_buffer.h"
#include "vk_device.h"
#include "vk_format.h"
#include "vk_pipeline.h"

#include "nir.h"
#include "nir_builder.h"

#define BINDING_OUTPUT 0
#define BINDING_INPUT  1

struct vk_meta_image_to_buffer_push_data {
   uint32_t dest_offset_el;
   uint32_t dest_width;
   uint32_t dest_height;

   int32_t src_offset_el[3];
} PACKED;

#define get_push(b, name)                                                      \
   nir_load_push_constant(                                                     \
      b, 1, sizeof(((struct vk_meta_image_to_buffer_push_data *)0)->name) * 8, \
      nir_imm_int(b,                                                           \
                  offsetof(struct vk_meta_image_to_buffer_push_data, name)))

struct vk_meta_buffer_copy_key {
   enum vk_meta_object_key_type key_type;
   bool is_array;
};

static nir_shader *
build_buffer_copy_shader(const struct vk_meta_buffer_copy_key *key)
{
   nir_builder build = nir_builder_init_simple_shader(MESA_SHADER_COMPUTE, NULL,
                                                      "vk-meta-copy-to-buffer");

   nir_builder *b = &build;

   /* Using uint types here is technically wrong but AGX ignores it. If other
    * backends care this needs to be added to the shader key :-(
    */
   const struct glsl_type *texture_type = glsl_sampler_type(
      GLSL_SAMPLER_DIM_2D, false, key->is_array, GLSL_TYPE_UINT);
   const struct glsl_type *image_type =
      glsl_image_type(GLSL_SAMPLER_DIM_BUF, false, GLSL_TYPE_UINT);

   nir_variable *texture =
      nir_variable_create(b->shader, nir_var_uniform, texture_type, "source");
   nir_variable *image =
      nir_variable_create(b->shader, nir_var_image, image_type, "dest");

   image->data.descriptor_set = 0;
   image->data.binding = BINDING_OUTPUT;

   texture->data.descriptor_set = 0;
   texture->data.binding = BINDING_INPUT;

   /* Grab the source image offset vector */
   nir_def *src_offset_el = nir_load_push_constant(
      b, 3, 32,
      nir_imm_int(
         b, offsetof(struct vk_meta_image_to_buffer_push_data, src_offset_el)));

   /* We're done setting up variables, do the copy */
   nir_def *coord = nir_load_global_invocation_id(b, 32);
   nir_def *src_coord = nir_iadd(b, coord, src_offset_el);

   /* TODO: Non-2D */
   if (!key->is_array)
      src_coord = nir_trim_vector(b, src_coord, 2);

   nir_def *value =
      nir_txf_deref(b, nir_build_deref_var(b, texture), src_coord, NULL);

   /* Index into the destination buffer */
   nir_def *dest_width = get_push(b, dest_width);
   nir_def *dest_height = get_push(b, dest_height);
   nir_def *dest_layer_stride = nir_imul(b, dest_width, dest_height);
   nir_def *x = nir_channel(b, coord, 0);
   nir_def *y = nir_channel(b, coord, 1);
   nir_def *z = nir_channel(b, coord, 2);

   nir_def *index = get_push(b, dest_offset_el);
   index = nir_iadd(b, index, x);
   index = nir_iadd(b, index, nir_imul(b, y, dest_width));
   index = nir_iadd(b, index, nir_imul(b, z, dest_layer_stride));

   /* Write formatted texel to storage texel buffer */
   nir_image_deref_store(b, &nir_build_deref_var(b, image)->dest.ssa,
                         nir_pad_vec4(b, index), nir_imm_int(b, 0), value,
                         nir_imm_int(b, 0), .image_dim = GLSL_SAMPLER_DIM_BUF,
                         .image_array = false);

   return b->shader;
}

static VkResult
get_buffer_copy_descriptor_set_layout(struct vk_device *device,
                                      struct vk_meta_device *meta,
                                      VkDescriptorSetLayout *layout_out)
{
   const char key[] = "vk-meta-copy-image-to-buffer-descriptor-set-layout";

   VkDescriptorSetLayout from_cache =
      vk_meta_lookup_descriptor_set_layout(meta, key, sizeof(key));
   if (from_cache != VK_NULL_HANDLE) {
      *layout_out = from_cache;
      return VK_SUCCESS;
   }

   const VkDescriptorSetLayoutBinding bindings[] = {
      {
         .binding = BINDING_OUTPUT,
         .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER,
         .descriptorCount = 1,
         .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
      },
      {
         .binding = BINDING_INPUT,
         .descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
         .descriptorCount = 1,
         .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
      },
   };

   const VkDescriptorSetLayoutCreateInfo info = {
      .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
      .bindingCount = ARRAY_SIZE(bindings),
      .pBindings = bindings,
   };

   return vk_meta_create_descriptor_set_layout(device, meta, &info, key,
                                               sizeof(key), layout_out);
}

static VkResult
get_buffer_copy_pipeline_layout(struct vk_device *device,
                                struct vk_meta_device *meta,
                                struct vk_meta_buffer_copy_key *key,
                                VkDescriptorSetLayout set_layout,
                                VkPipelineLayout *layout_out)
{
   const char pipeline_key[] = "vk-meta-buffer-to-image-pipeline-layout";

   VkPipelineLayout from_cache =
      vk_meta_lookup_pipeline_layout(meta, pipeline_key, sizeof(pipeline_key));
   if (from_cache != VK_NULL_HANDLE) {
      *layout_out = from_cache;
      return VK_SUCCESS;
   }

   VkPipelineLayoutCreateInfo info = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
      .setLayoutCount = 1,
      .pSetLayouts = &set_layout,
   };

   const VkPushConstantRange push_range = {
      .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
      .offset = 0,
      .size = sizeof(struct vk_meta_image_to_buffer_push_data),
   };

   info.pushConstantRangeCount = 1;
   info.pPushConstantRanges = &push_range;

   return vk_meta_create_pipeline_layout(device, meta, &info, key, sizeof(*key),
                                         layout_out);
}

static VkResult
get_buffer_copy_pipeline(struct vk_device *device, struct vk_meta_device *meta,
                         const struct vk_meta_buffer_copy_key *key,
                         VkPipelineLayout layout, VkPipeline *pipeline_out)
{
   VkPipeline from_cache = vk_meta_lookup_pipeline(meta, key, sizeof(*key));
   if (from_cache != VK_NULL_HANDLE) {
      *pipeline_out = from_cache;
      return VK_SUCCESS;
   }

   const VkPipelineShaderStageNirCreateInfoMESA nir_info = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_NIR_CREATE_INFO_MESA,
      .nir = build_buffer_copy_shader(key),
   };
   const VkPipelineShaderStageCreateInfo cs_info = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
      .pNext = &nir_info,
      .stage = VK_SHADER_STAGE_COMPUTE_BIT,
      .pName = "main",
   };

   const VkComputePipelineCreateInfo info = {
      .sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
      .stage = cs_info,
      .layout = layout,
   };

   VkResult result = vk_meta_create_compute_pipeline(
      device, meta, &info, key, sizeof(*key), pipeline_out);
   ralloc_free(nir_info.nir);

   return result;
}

void
vk_meta_copy_image_to_buffer2(struct vk_command_buffer *cmd,
                              struct vk_meta_device *meta,
                              const VkCopyImageToBufferInfo2 *pCopyBufferInfo)
{
   VK_FROM_HANDLE(vk_buffer, buffer, pCopyBufferInfo->dstBuffer);
   VK_FROM_HANDLE(vk_image, image, pCopyBufferInfo->srcImage);

   struct vk_device *device = cmd->base.device;
   const struct vk_device_dispatch_table *disp = &device->dispatch_table;

   VkResult result;

   /* We use a common format for both sides of the copy, eliminating shader
    * conversions for this path.
    */
   VkFormat format = image->format;

   VkDescriptorSetLayout set_layout;
   result = get_buffer_copy_descriptor_set_layout(device, meta, &set_layout);
   if (unlikely(result != VK_SUCCESS)) {
      vk_command_buffer_set_error(cmd, result);
      return;
   }

   for (unsigned i = 0; i < pCopyBufferInfo->regionCount; ++i) {
      const VkBufferImageCopy2 *region = &pCopyBufferInfo->pRegions[i];

      unsigned dest_width =
         region->bufferRowLength ?: region->imageExtent.width;
      unsigned dest_height =
         region->bufferImageHeight ?: region->imageExtent.height;

      /* Clamp the height to what's actually necessary. CTS tests this. */
      dest_height = MIN2(dest_height, region->imageExtent.height);

      uint32_t blocksize_B =
         util_format_get_blocksize(vk_format_to_pipe_format(format));

      unsigned size_B =
         blocksize_B *
         ((dest_width * (dest_height - 1)) + region->imageExtent.width) *
         region->imageSubresource.layerCount;

      bool is_array = region->imageSubresource.layerCount > 1;

      struct vk_meta_buffer_copy_key key = {
         .key_type = VK_META_OBJECT_KEY_COPY_IMAGE_TO_BUFFER_PIPELINE,
         .is_array = is_array,
      };

      VkPipelineLayout pipeline_layout;
      result = get_buffer_copy_pipeline_layout(device, meta, &key, set_layout,
                                               &pipeline_layout);
      if (unlikely(result != VK_SUCCESS)) {
         vk_command_buffer_set_error(cmd, result);
         return;
      }

      VkImageView src_view;
      const VkImageViewUsageCreateInfo src_view_usage = {
         .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO,
         .usage = VK_IMAGE_USAGE_SAMPLED_BIT,
      };
      const VkImageViewCreateInfo src_view_info = {
         .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
         .pNext = &src_view_usage,
         .image = pCopyBufferInfo->srcImage,
         .viewType = is_array ? VK_IMAGE_VIEW_TYPE_2D_ARRAY
                              : VK_IMAGE_VIEW_TYPE_2D /* TODO */,
         .format = format,
         .subresourceRange =
            {
               .aspectMask = region->imageSubresource.aspectMask,
               .baseMipLevel = region->imageSubresource.mipLevel,
               .baseArrayLayer = region->imageSubresource.baseArrayLayer,
               .layerCount = region->imageSubresource.layerCount,
               .levelCount = 1,
            },
      };

      result = vk_meta_create_image_view(cmd, meta, &src_view_info, &src_view);
      if (unlikely(result != VK_SUCCESS)) {
         vk_command_buffer_set_error(cmd, result);
         return;
      }

      VkDescriptorImageInfo src_info = {
         .imageLayout = pCopyBufferInfo->srcImageLayout,
         .imageView = src_view,
      };

      VkDescriptorBufferInfo buffer_infos[2];
      VkWriteDescriptorSet desc_writes[2];

      const VkBufferViewCreateInfo dst_view_info = {
         .sType = VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO,
         .buffer = pCopyBufferInfo->dstBuffer,
         .format = format,

         /* Ideally, this would be region->bufferOffset, but that might not
          * be aligned to minTexelBufferOffsetAlignment. Instead, we use a 0
          * offset (which is definitely aligned) and add the offset ourselves
          * in the shader.
          */
         .offset = 0,

         /* Include the offset in the range since we add the offset in
          * shader. For the size of the copy itself, the last row is smaller
          * than the stride. The CTS tests this.
          */
         .range = region->bufferOffset + size_B,
      };

      VkBufferView dst_view;
      VkResult result =
         vk_meta_create_buffer_view(cmd, meta, &dst_view_info, &dst_view);
      if (unlikely(result != VK_SUCCESS)) {
         vk_command_buffer_set_error(cmd, result);
         return;
         }

         desc_writes[0] = (VkWriteDescriptorSet){
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = 0,
            .dstBinding = BINDING_OUTPUT,
            .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER,
            .descriptorCount = 1,
            .pTexelBufferView = &dst_view,
         };

         desc_writes[1] = (VkWriteDescriptorSet){
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = 0,
            .dstBinding = BINDING_INPUT,
            .descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
            .descriptorCount = 1,
            .pImageInfo = &src_info,
         };

         disp->CmdPushDescriptorSetKHR(
            vk_command_buffer_to_handle(cmd), VK_PIPELINE_BIND_POINT_COMPUTE,
            pipeline_layout, 0, ARRAY_SIZE(desc_writes), desc_writes);

         VkPipeline pipeline;
         result = get_buffer_copy_pipeline(device, meta, &key, pipeline_layout,
                                           &pipeline);
         if (unlikely(result != VK_SUCCESS)) {
            vk_command_buffer_set_error(cmd, result);
            return;
         }

         disp->CmdBindPipeline(vk_command_buffer_to_handle(cmd),
                               VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);

         /* Vulkan guarantees this for non-planar, non-Z/S resources.
          *
          * XXX: Handle planar and Z/S.
          */
         assert((region->bufferOffset % blocksize_B) == 0 && "must be aligned");
         uint32_t offset_el = region->bufferOffset / blocksize_B;

         if (region->imageSubresource.layerCount > 1)
            unreachable("todo");

         for (unsigned l = 0; l < region->imageSubresource.layerCount; ++l) {
            struct vk_meta_image_to_buffer_push_data push = {
               .dest_offset_el = offset_el,
               .dest_width = dest_width,
               .dest_height = dest_height,

               .src_offset_el[0] = region->imageOffset.x,
               .src_offset_el[1] = region->imageOffset.y,
               .src_offset_el[2] = l + region->imageOffset.z,
            };

            disp->CmdPushConstants(vk_command_buffer_to_handle(cmd),
                                   pipeline_layout, VK_SHADER_STAGE_COMPUTE_BIT,
                                   0, sizeof(push), &push);

            /* Dispatch 1 thread per texel */
            disp->CmdDispatch(
               vk_command_buffer_to_handle(cmd), region->imageExtent.width,
               region->imageExtent.height, region->imageExtent.depth);
         }
   }
}
