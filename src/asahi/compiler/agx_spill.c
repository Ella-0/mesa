/*
 * Copyright 2023 Alyssa Rosenzweig
 * Copyright 2023 Valve Corporation
 * Copyright 2022 Collabora Ltd.
 * SPDX-License-Identifier: MIT
 */

/*
 * An implementation of "Register Spilling and Live-Range Splitting for SSA-Form
 * Programs" by Braun and Hack.
 *
 * SSA construction follows algorithm of "Simple and Efficient
 * Construction of Static Single Assignment Form", also by Braun et al.
 * https://link.springer.com/content/pdf/10.1007/978-3-642-37051-9_6.pdf
 */

#include "util/hash_table.h"
#include "util/u_math.h"
#include "agx_builder.h"
#include "agx_compiler.h"
#include "agx_opcodes.h"

/* Sentinel value representing an infinite distance */
#define DIST_INFINITY (UINT32_MAX)

struct spill_block {
   /* For a loop header, whether phis operands have been added */
   bool sealed;

   /* Set of values available in the register file at the end */
   unsigned W_exit[AGX_NUM_REGS];
   unsigned nW_exit;

   unsigned W_entry[AGX_NUM_REGS];
   unsigned nW_entry;

   /* Spilled subset of W_exit */
   unsigned S_exit[AGX_NUM_REGS];
   unsigned nS_exit;

   /* Estimate */
   uint32_t cycles;

   /* Next-use maps at the start/end of the block */
   uint32_t *next_use_in;
   uint32_t *next_use_out;

   /* Definition of a variable at the end of the block, indexed by variable
    * name.
    */
   agx_index *defs;
};

struct spill_ctx {
   agx_context *shader;
   agx_block *block;

   /* Set of values currently available in the register file */
   BITSET_WORD *W;

   /* |W| = Current register pressure */
   unsigned nW;

   /* Subset of W that has already been spilled */
   BITSET_WORD *S;

   /* Widths of vectors */
   uint8_t *ncomps;
   enum agx_size *size;

   /* Mapping of rematerializable values to their definitions, or NULL for nodes
    * that are not materializable.
    */
   agx_instr **remat;

   /* Maximum register pressure allowed */
   unsigned k;

   /* Number of variables */
   unsigned n;

   /* Information on blocks indexed in source order */
   struct spill_block *blocks;

   /* Map of spilled variables to spill locations */
   unsigned *spill_locations;

   /* Whether a variable has a spill location assigned yet */
   BITSET_WORD *assigned_spill_location;

   /* Mapping of removed trivial phi destinations to their replacement node.
    * This needs to be consulted after reading defs[], which can keep dangling
    * references to removed trivial phis.
    *
    * This is hash table because it is indexed by keys of inserted phi
    * destinations, which would otherwise read out-of-bounds if we allocated an
    * array of size ctx->alloc. It's also pretty sparse.
    */
   struct hash_table_u64 *trivial_phi_map;
};

static inline struct spill_block *
spill_block(struct spill_ctx *ctx, agx_block *block)
{
   return &ctx->blocks[block->index];
}

/*
 * Map a control flow edge to a block. Assumes no critical edges.
 */
static agx_block *
agx_edge_to_block(agx_block *pred, agx_block *succ)
{
   /* End of predecessor is unique if there's a single successor */
   if (agx_num_successors(pred) == 1)
      return pred;

   /* The predecessor has multiple successors, meaning this is not the only
    * edge leaving the predecessor. Therefore, it is the only edge entering
    * the successor (otherwise the edge would be critical), so the start of
    * the successor is unique.
    */
   assert(agx_num_predecessors(succ) == 1 && "critical edge detected");
   return succ;
}

/*
 * Get a cursor to insert along a control flow edge: either at the start of the
 * successor or the end of the predecessor. This relies on the control flow
 * graph having no critical edges.
 */
static agx_cursor
agx_along_edge(agx_block *pred, agx_block *succ)
{
   agx_block *to = agx_edge_to_block(pred, succ);

   if (to == pred)
      return agx_after_block_logical(pred);
   else
      return agx_before_block(succ);
}

/*
 * Record write to a variable, for use reconstructing SSA. writeVariable in
 * Braun et al.
 */
static void
record_write(struct spill_ctx *ctx, agx_block *block, unsigned node,
             agx_index val)
{
   assert(node < ctx->n);
   spill_block(ctx, block)->defs[node] = val;
}

static void add_phi_operands(struct spill_ctx *ctx, agx_block *block,
                             agx_instr *phi, unsigned node);

/*
 * Resolve read from a variable while reconstructing SSA. readVariable in Braun
 * et al.
 */
static agx_index
resolve_read(struct spill_ctx *ctx, agx_block *block, unsigned node)
{
   struct spill_block *sb = spill_block(ctx, block);

   /* Local value numbering */
   assert(node < ctx->n);
   agx_index local = sb->defs[node];

   if (!agx_is_null(local)) {
      /* Resolve trivial phis, don't return dangling pointers. Off-by-one since
       * 0 is treated as the null sentinel value.
       */
      if (local.type == AGX_INDEX_NORMAL) {
         void *repl =
            _mesa_hash_table_u64_search(ctx->trivial_phi_map, local.value);
         if (repl)
            local.value = ((uintptr_t)repl) - 1;
      }

      return local;
   }

   /* Global value numbering. readValueRecursive in the paper */
   unsigned nr_preds = agx_num_predecessors(block);
   agx_index val;

   assert(nr_preds > 0);

   /* Loop headers are not in the "sealedBlock" set in the paper. To
    * handle, we insert an incomplete phi to be filled in after the rest of
    * the loop is processed.
    */
   if (ctx->block->loop_header && !sb->sealed) {
      val = agx_temp(ctx->shader, ctx->size[node]);
      agx_builder b = agx_init_builder(ctx->shader, agx_before_block(block));
      agx_instr *phi = agx_phi_to(&b, val, nr_preds);

      /* XXX: this is a hack */
      phi->imm = node + 1;
   } else if (nr_preds == 1) {
      /* No phi needed */
      agx_block *pred =
         *util_dynarray_element(&block->predecessors, agx_block *, 0);
      val = resolve_read(ctx, pred, node);
   } else {
      /* Insert phi first to break cycles */
      val = agx_temp(ctx->shader, ctx->size[node]);
      agx_builder b = agx_init_builder(ctx->shader, agx_before_block(block));
      agx_instr *phi = agx_phi_to(&b, val, nr_preds);
      record_write(ctx, block, node, val);
      add_phi_operands(ctx, block, phi, node);

      /* Remove trivial phis */
      agx_index same = agx_null();
      bool all_same = true;

      agx_foreach_src(phi, s) {
         /* Same value or self-reference */
         if (agx_is_equiv(phi->src[s], same) ||
             agx_is_equiv(phi->src[s], phi->dest[0]))
            continue;

         if (!agx_is_null(same)) {
            all_same = false;
            break;
         }

         same = phi->src[s];
      }

      if (all_same) {
         val = phi->src[0];
         agx_remove_instruction(phi);

         /* We just inserted the phi, nothing else should have touched it */
         assert(!_mesa_hash_table_u64_search(ctx->trivial_phi_map,
                                             phi->dest[0].value));

         /* Tell future lookups to replace phi->dest[0].value with val.index.
          * Off-by-one since 0 is treated as the null sentinel value.
          */
         _mesa_hash_table_u64_insert(ctx->trivial_phi_map, phi->dest[0].value,
                                     (void *)(uintptr_t)(val.value + 1));
      }

      /* The paper suggests recursing, but this is nontrivial without use/def
       * chains and doesn't seem too important for our application.
       */
   }

   record_write(ctx, block, node, val);
   return val;
}

static void
add_phi_operands(struct spill_ctx *ctx, agx_block *block, agx_instr *phi,
                 unsigned node)
{
   /* Add phi operands */
   agx_foreach_predecessor(block, pred) {
      unsigned s = agx_predecessor_index(block, *pred);
      phi->src[s] = resolve_read(ctx, *pred, node);
   }
}

static void
seal_block(struct spill_ctx *ctx, agx_block *block)
{
   agx_foreach_phi_in_block(block, phi) {
      /* We use phi->imm as a sideband to pass the variable name.
       * XXX: bit of a hack.
       */
      if (phi->imm) {
         add_phi_operands(ctx, block, phi, phi->imm - 1);
         phi->imm = 0;
      }
   }

   spill_block(ctx, block)->sealed = true;
}

static void
seal_loop_headers(struct spill_ctx *ctx)
{
   agx_foreach_successor(ctx->block, succ) {
      /* Only loop headers need to be sealed late */
      if (!succ->loop_header)
         continue;

      /* Check if all predecessors have been processed */
      bool any_unprocessed = false;

      agx_foreach_predecessor(succ, P) {
         if ((*P)->index > ctx->block->index) {
            any_unprocessed = true;
            break;
         }
      }

      /* Seal once all predecessors are processed */
      if (!any_unprocessed)
         seal_block(ctx, succ);
   }
}

/*
 * Insert a spill for a variable. A given variable is always spilled to the same
 * place.
 */
static void
insert_spill(agx_builder *b, struct spill_ctx *ctx, unsigned node)
{
   /* If we'll rematerialize, there's nothing to do */
   if (ctx->remat[node])
      return;

   /* Calculate number of bytes to spill */
   unsigned regs = ctx->ncomps[node];
   unsigned size_16 = agx_size_align_16(ctx->size[node]);
   unsigned bytes = regs * 2;

   /* Reconstruct source */
   agx_index source = agx_get_index(node, ctx->size[node]);

   agx_context *shader = b->shader;

   /* Assign spill location if needed */
   if (!BITSET_TEST(ctx->assigned_spill_location, node)) {
      shader->out->scratch_size = ALIGN_POT(shader->out->scratch_size, bytes);
      ctx->spill_locations[node] = shader->out->scratch_size;
      shader->out->scratch_size += bytes;

      BITSET_SET(ctx->assigned_spill_location, node);
   }

   enum agx_format format = size_16 == 1 ? AGX_FORMAT_I16 : AGX_FORMAT_I32;
   unsigned components = size_16 == 1 ? regs : (regs >> 1);

   /* Insert spill */
   agx_stack_store(b, source, agx_immediate(ctx->spill_locations[node]), format,
                   BITFIELD_MASK(components), 0);

   ctx->shader->spills++;
}

/*
 * Insert a reload for a variable. The temporary created becomes the new
 * local definition of the variable, preserving SSA.
 */
static void
insert_reload(struct spill_ctx *ctx, agx_block *block, agx_cursor cursor,
              unsigned node)
{
   agx_builder b = agx_init_builder(ctx->shader, cursor);
   unsigned regs = ctx->ncomps[node];
   unsigned size_16 = agx_size_align_16(ctx->size[node]);

   agx_index tmp = agx_temp(ctx->shader, ctx->size[node]);

   /* If we're rematerializing a constant, that's a special path */
   if (ctx->remat[node]) {
      assert(ctx->remat[node]->op == AGX_OPCODE_MOV_IMM);
      agx_mov_imm_to(&b, tmp, ctx->remat[node]->imm);
   } else {
      enum agx_format format = size_16 == 1 ? AGX_FORMAT_I16 : AGX_FORMAT_I32;
      unsigned components = size_16 == 1 ? regs : (regs >> 1);

      agx_stack_load_to(&b, tmp, agx_immediate(ctx->spill_locations[node]),
                        format, BITFIELD_MASK(components), 0);

      ctx->shader->fills++;
   }

   record_write(ctx, block, node, tmp);
}

/* Insert into the register file */
static void
insert_W(struct spill_ctx *ctx, unsigned v)
{
   assert(v < ctx->n);
   assert(!BITSET_TEST(ctx->W, v));

   BITSET_SET(ctx->W, v);
   ctx->nW += ctx->ncomps[v];
}

/* Remove from the register file */
static void
remove_W(struct spill_ctx *ctx, unsigned v)
{
   assert(v < ctx->n);
   assert(BITSET_TEST(ctx->W, v));

   BITSET_CLEAR(ctx->W, v);
   BITSET_CLEAR(ctx->S, v);
   ctx->nW -= ctx->ncomps[v];
}

struct candidate {
   unsigned node;
   uint32_t dist;
};

/* Sum two next-use distances, with saturation at infinity. */
static uint32_t
dist_sum(uint32_t A, uint32_t B)
{
   uint32_t sum = A + B;
   bool overflow = (sum < A);

   return overflow ? DIST_INFINITY : sum;
}

static int
cmp_dist(const void *left_, const void *right_)
{
   const struct candidate *left = left_;
   const struct candidate *right = right_;

   if (left->dist > right->dist)
      return 1;
   else if (left->dist == right->dist)
      return 0;
   else
      return -1;
}

/*
 * Limit the register file W to its maximum size k by evicting registers. limit
 * in the paper.
 */
static void
limit(struct spill_ctx *ctx, agx_instr *I, bool skip_first, unsigned m)
{
   /* Nothing to do if we're already below the limit */
   if (ctx->nW <= m)
      return;

   /* Calculate next-use distances. First look within the block, else use
    * the precomputed distance from the end of the block.
    */
   struct candidate *candidates = calloc(sizeof(struct candidate), ctx->nW);
   unsigned j = 0;

   int i;
   BITSET_FOREACH_SET(i, ctx->W, ctx->n) {
      /* TODO: Optimize me! */
      unsigned dist = 0;
      bool live = false;
      bool should_skip = skip_first;
      agx_foreach_instr_in_block_from(ctx->block, use, I) {
         if (should_skip) {
            should_skip = false;
            continue;
         }

         agx_foreach_ssa_src(use, s) {
            if (use->src[s].value == i) {
               live = true;
               break;
            }
         }

         if (live)
            break;

         dist++;
      }

      /* Although unused in the block, it might be used later. Add the
       * number of instructions skipped in this block.
       */
      if (!live) {
         struct spill_block *sb = spill_block(ctx, ctx->block);
         dist = dist_sum(dist, sb->next_use_out[i]);
      }

      candidates[j++] = (struct candidate){.node = i, .dist = dist};
   }

   /* Sort by next-use distance */
   qsort(candidates, j, sizeof(struct candidate), cmp_dist);

   /* Evict what doesn't fit */
   unsigned new_weight = 0;

   for (i = 0; i < ctx->nW; ++i) {
      unsigned comps = ctx->ncomps[candidates[i].node];

      /* Stop when we would overflow, TODO: try to pack a scalar in */
      if ((new_weight + comps) > m)
         break;

      new_weight += comps;
   }

   /* i is now the zero-based index that overflowed == the number of
    * candidates we kept. Evict from there.
    */
   for (; i < j; ++i) {
      unsigned v = candidates[i].node;

      /* Insert a spill if we haven't spilled before and there is
       * another use
       */
      if (!BITSET_TEST(ctx->S, v) && candidates[i].dist < DIST_INFINITY) {
         agx_builder b = agx_init_builder(ctx->shader, agx_before_instr(I));
         insert_spill(&b, ctx, v);
      }

      remove_W(ctx, v);
   }

   free(candidates);
}

static void
insert_coupling_code(struct spill_ctx *ctx, agx_block *pred, agx_block *succ,
                     struct spill_block *blocks)
{
   struct spill_block *sb = spill_block(ctx, succ);
   struct spill_block *sp = spill_block(ctx, pred);

   /* Reload W_entry_B \ W_exit_P on edge */
   for (unsigned i = 0; i < sb->nW_entry; ++i) {
      unsigned node = sb->W_entry[i];
      bool in_exit = false;

      for (unsigned j = 0; j < sp->nW_exit; ++j) {
         if (sp->W_exit[j] == node) {
            in_exit = true;
            break;
         }
      }

      /* TODO: Phis? */
      agx_foreach_phi_in_block(succ, phi) {
         if (phi->dest[0].value == node) {
            in_exit = true;
            break;
         }
      }

      if (in_exit)
         continue;

      agx_block *reload_block = agx_edge_to_block(pred, succ);
      insert_reload(ctx, reload_block, agx_along_edge(pred, succ), node);

      /* Inserting a reload into the predecessor has the effect of adding the
       * node to W_exit_P implicitly, which makes it a candidate for S_entry_B.
       * Indeed, the node must be in S_entry_B since it was already spilled.
       */
      BITSET_SET(ctx->S, node);
   }

   /* Spill (S_entry_B \ S_exit_P) & W_exit_P on the edge. */
   for (unsigned i = 0; i < sp->nW_exit; ++i) {
      unsigned v = sp->W_exit[i];

      /* Must be in S_entry_B. ctx->S has been appropriately initialized.
       *
       * TODO: What about loops?
       */
      if (!BITSET_TEST(ctx->S, v))
         continue;

      /* Must not be in S_exit_P */
      bool v_in_S_exit = false;
      for (unsigned j = 0; j < sp->nS_exit; ++j) {
         if (sp->S_exit[j] == v) {
            v_in_S_exit = true;
            break;
         }
      }

      if (v_in_S_exit)
         continue;

      agx_builder b = agx_init_builder(ctx->shader, agx_along_edge(pred, succ));
      insert_spill(&b, ctx, v);
   }
}

/*
 * Before processing a block, insert coupling code for predecessors that are
 * already processed. This is the normal case, covering all but loop headers.
 */
static void
insert_coupling_code_early(struct spill_ctx *ctx, agx_block *block,
                           struct spill_block *blocks)
{
   agx_foreach_predecessor(block, pred) {
      if ((*pred)->index < block->index)
         insert_coupling_code(ctx, *pred, block, blocks);
      else
         assert(block->loop_header);
   }
}

/*
 * After processing a loop exit, insert coupling code for the loop header, now
 * that all of the header's predecessors have been processed. For other kinds of
 * blocks, do nothing so the caller doesn't need to check for loop exits.
 */
static void
insert_coupling_code_late(struct spill_ctx *ctx, agx_block *block,
                          struct spill_block *blocks)
{
   agx_foreach_successor(block, succ) {
      if (succ->loop_header) {
         insert_coupling_code(ctx, block, succ, blocks);
         return;
      }
   }
}

/*
 * Insert spills/fills for a single basic block, following Belady's algorithm.
 * Corresponds to minAlgorithm from the paper.
 */
static void
min_algorithm(struct spill_ctx *ctx)
{
   /* Set of values that need to be reloaded */
   BITSET_WORD *R = calloc(BITSET_WORDS(ctx->n), sizeof(BITSET_WORD));

   /* Iterate each instruction in forward order */
   agx_foreach_instr_in_block(ctx->block, I) {
      assert(ctx->nW <= ctx->k && "invariant");

      /* Record writes (even for phis) for SSA reconstruction. Skip
       * writes from coupling reloads that were already inserted.
       */
      agx_foreach_ssa_dest(I, d) {
         if (I->dest[d].value < ctx->n)
            record_write(ctx, ctx->block, I->dest[d].value, I->dest[d]);
      }

      /* Phis handled separately */
      /* TODO: Propagate reloads into phis? */
      if (I->op == AGX_OPCODE_PHI)
         continue;

      /* Iterate uses in R: uses \ W */
      agx_foreach_ssa_src(I, s) {
         agx_index src = I->src[s];
         if (BITSET_TEST(ctx->W, src.value))
            continue;

         /* We don't reprocess so should not be a reload */
         assert(src.value < ctx->n);

         /* A reload will be inserted for this value, so it
          * becomes available in the register file.
          */
         insert_W(ctx, src.value);

         /* Because the value was reloaded, it must have been
          * previously spilled because the program is in SSA
          * form.
          */
         BITSET_SET(ctx->S, src.value);

         /* Mark this variable as needing a reload. */
         BITSET_SET(R, src.value);
      }

      /* Limit W to make space for the sources we just added */
      limit(ctx, I, false, ctx->k);

      /* Count how many registers we need for destinations. Because of
       * SSA form, destinations are unique.
       */
      unsigned dest_size = 0;
      agx_foreach_ssa_dest(I, d) {
         dest_size += ctx->ncomps[I->dest[d].value];
      }

      /* Limit W to make space for the destinations.
       *
       * TODO: I_next isn't well defined for the last instruction in a
       * block.
       */
      limit(ctx, I, true, ctx->k - dest_size);

      /* Destinations are now in the register file */
      agx_foreach_ssa_dest(I, d) {
         insert_W(ctx, I->dest[d].value);
      }

      /* Add reloads for the sources in front of the instruction */
      agx_foreach_ssa_src(I, s) {
         if (!BITSET_TEST(R, I->src[s].value))
            continue;

         insert_reload(ctx, ctx->block, agx_before_instr(I), I->src[s].value);

         BITSET_CLEAR(R, I->src[s].value);
      }

      /* Repair SSA for the instruction */
      agx_foreach_ssa_src(I, s) {
         agx_replace_src(I, s, resolve_read(ctx, ctx->block, I->src[s].value));
      }
   }

   /* Record state of register file at the end */
   struct spill_block *sblock = spill_block(ctx, ctx->block);

   int i;
   BITSET_FOREACH_SET(i, ctx->W, ctx->n)
      sblock->W_exit[sblock->nW_exit++] = i;

   BITSET_FOREACH_SET(i, ctx->S, ctx->n)
      sblock->S_exit[sblock->nS_exit++] = i;

   free(R);
}

static void
compute_w_entry_loop_header(struct spill_ctx *ctx)
{
   agx_block *block = ctx->block;
   struct spill_block *sb = spill_block(ctx, block);

   /* TODO: Implement section 4.2 of the paper.
    *
    * For now, we implement the simpler heuristic in Hack's thesis: sort
    * the live-in set (+ destinations of phis) by next-use distance and
    * fill from there.
    */

   /* Count the size of the live set */
   unsigned nP = 0;
   unsigned i = 0;

   BITSET_FOREACH_SET(i, block->live_in, ctx->n) {
      nP++;
   }

   /* TODO: Optimize out the sort */
   struct candidate *candidates = calloc(sizeof(struct candidate), nP);
   unsigned j = 0;

   BITSET_FOREACH_SET(i, block->live_in, ctx->n) {
      candidates[j++] = (struct candidate){
         .node = i,
         .dist = sb->next_use_in[i],
      };
   }

   agx_foreach_phi_in_block(block, I) {
      assert(BITSET_TEST(block->live_in, I->dest[0].value));
   }

   assert(j == nP);

   /* Sort by next-use distance */
   qsort(candidates, j, sizeof(struct candidate), cmp_dist);

   /* Take as much as we can */
   unsigned new_weight = 0;

   for (unsigned i = 0; i < j; ++i) {
      unsigned node = candidates[i].node;
      unsigned comps = ctx->ncomps[node];

      /* Stop when we would overflow, TODO: try to pack a scalar in */
      if ((new_weight + comps) > ctx->k)
         break;

      new_weight += comps;
      insert_W(ctx, node);

      sb->W_entry[sb->nW_entry++] = node;
   }

   assert(new_weight <= ctx->k);
   free(candidates);
}

/*
 * Compute W_entry for a block. Section 4.2 in the paper.
 */
static void
compute_w_entry(struct spill_ctx *ctx)
{
   agx_block *block = ctx->block;

   /* Nothing to do for start blocks */
   if (agx_num_predecessors(block) == 0)
      return;

   /* Loop headers have a different heuristic */
   if (block->loop_header) {
      compute_w_entry_loop_header(ctx);
      return;
   }

   /* Usual blocks follow */
   unsigned *freq = calloc(ctx->n, sizeof(unsigned));

   /* Record what's written at the end of each predecessor */
   agx_foreach_predecessor(ctx->block, P) {
      struct spill_block *sp = spill_block(ctx, *P);

      for (unsigned i = 0; i < sp->nW_exit; ++i) {
         unsigned v = sp->W_exit[i];
         freq[v]++;
      }
   }

   /* ...and include what's written by phis. Here we model phis as being
    * written in each predecessor.
    */
   agx_foreach_phi_in_block(ctx->block, I) {
      freq[I->dest[0].value] += agx_num_predecessors(ctx->block);
   }

   /* Variables that are in all predecessors are assumed in W_entry */
   for (unsigned i = 0; i < ctx->n; ++i) {
      if (freq[i] == agx_num_predecessors(ctx->block)) {
         insert_W(ctx, i);
         freq[i] = 0;
      }
   }

   struct spill_block *sb = spill_block(ctx, block);

   /* TODO: Use next-use distance to pack in what's live-in and in some */
   /* TODO: Use next-use distance here */
   int i;
   for (i = 0; i < ctx->n && ctx->nW < ctx->k; ++i) {
      if (freq[i] && (ctx->nW + ctx->ncomps[i] <= ctx->k) &&
          sb->next_use_in[i] < DIST_INFINITY) {
         insert_W(ctx, i);
         freq[i] = 0;
      }
   }

   assert(ctx->nW <= ctx->k && "invariant");

   BITSET_FOREACH_SET(i, ctx->W, ctx->n) {
      sb->W_entry[sb->nW_entry++] = i;
   }

   free(freq);
}

/*
 * Section 4.3 defines
 *
 *    S_entry_B = W_entry_B & (union{P in pred(B)} (S_exit_P))
 *
 * Initialize S accordingly. Assumes that W_entry_B has been calculated.
 */
static void
compute_s_entry(struct spill_ctx *ctx)
{
   /* Set to W_entry_B */
   memcpy(ctx->S, ctx->W, BITSET_WORDS(ctx->n) * sizeof(BITSET_WORD));

   /* TODO: Should be linear time, since S_exit_P are all sorted */
   int v;
   BITSET_FOREACH_SET(v, ctx->S, ctx->n) {
      bool in_some_S_exit_P = false;

      agx_foreach_predecessor(ctx->block, pred) {
         struct spill_block *sP = spill_block(ctx, *pred);
         for (unsigned j = 0; j < sP->nS_exit; ++j) {
            if (sP->S_exit[j] == v) {
               in_some_S_exit_P = true;
               break;
            }
         }

         if (in_some_S_exit_P)
            break;
      }

      if (!in_some_S_exit_P)
         BITSET_CLEAR(ctx->S, v);
   }
}

static uint32_t
instr_cycles(const agx_instr *I)
{
   return 1;
}

static void
global_next_use_distances(agx_context *ctx, struct spill_block *blocks)
{
   u_worklist worklist;
   u_worklist_init(&worklist, ctx->num_blocks, NULL);

   agx_foreach_block(ctx, block) {
      struct spill_block *sb = &blocks[block->index];

      sb->next_use_in = malloc(sizeof(uint32_t) * ctx->alloc);
      sb->next_use_out = malloc(sizeof(uint32_t) * ctx->alloc);

      /* Initialize to infinity */
      for (unsigned i = 0; i < ctx->alloc; ++i) {
         sb->next_use_in[i] = DIST_INFINITY;
         sb->next_use_out[i] = DIST_INFINITY;
      }

      agx_foreach_instr_in_block(block, I) {
         sb->cycles += instr_cycles(I);
      }

      agx_worklist_push_head(&worklist, block);
   }

   /* Iterate the work list in reverse order since liveness is backwards */
   while (!u_worklist_is_empty(&worklist)) {
      agx_block *blk = agx_worklist_pop_head(&worklist);
      struct spill_block *sb = &blocks[blk->index];

      /* Definitions that have been seen */
      BITSET_WORD *defined =
         calloc(sizeof(BITSET_WORD), BITSET_WORDS(ctx->alloc));

      /* Distance to first use before def in block. */
      uint32_t *nu = malloc(sizeof(uint32_t) * ctx->alloc);
      for (unsigned i = 0; i < ctx->alloc; ++i)
         nu[i] = DIST_INFINITY;

      uint32_t cycle = 0;

      /* Calculate nu. Phis are handled separately. */
      agx_foreach_instr_in_block(blk, I) {
         if (I->op == AGX_OPCODE_PHI) {
            cycle++;
            continue;
         }

         /* Record first use before def. Phi sources are handled
          * above, because they logically happen in the
          * predecessor.
          */
         agx_foreach_ssa_src(I, s) {
            if (BITSET_TEST(defined, I->src[s].value))
               continue;
            if (nu[I->src[s].value] < DIST_INFINITY)
               continue;

            assert(I->src[s].value < ctx->alloc);
            nu[I->src[s].value] = cycle;
         }

         /* Record defs */
         agx_foreach_ssa_dest(I, d) {
            assert(I->dest[d].value < ctx->alloc);
            BITSET_SET(defined, I->dest[d].value);
         }

         cycle += instr_cycles(I);
      }

      /* Apply transfer function to get our entry state. */
      for (unsigned i = 0; i < ctx->alloc; ++i) {
         /* TODO: Detect loop backedges */
         unsigned edge_length = 0;

         unsigned dist = edge_length;

         if (BITSET_TEST(defined, i))
            dist = DIST_INFINITY;
         else if (nu[i] < DIST_INFINITY)
            dist += nu[i];
         else
            dist = dist_sum(sb->cycles + dist, sb->next_use_out[i]);

         sb->next_use_in[i] = dist;
      }

      /* Propagate the live in of the successor (blk) to the live out
       * of predecessors.
       *
       * Phi nodes are logically on the control flow edge and act in
       * parallel. To handle when propagating, we kill writes from
       * phis and make live the corresponding sources.
       */
      agx_foreach_predecessor(blk, pred) {
         struct spill_block *sp = &blocks[(*pred)->index];
         uint32_t *tmp = malloc(ctx->alloc * sizeof(uint32_t));
         memcpy(tmp, sb->next_use_in, sizeof(uint32_t) * ctx->alloc);

         /* Kill write */
         agx_foreach_phi_in_block(blk, I) {
            assert(I->dest[0].type == AGX_INDEX_NORMAL);
            tmp[I->dest[0].value] = DIST_INFINITY;
         }

         /* Make live the corresponding source */
         agx_foreach_phi_in_block(blk, I) {
            agx_index operand = I->src[agx_predecessor_index(blk, *pred)];
            if (operand.type == AGX_INDEX_NORMAL)
               tmp[operand.value] = 0;
         }

         bool progress = false;

         /* Join by taking minimum */
         for (unsigned i = 0; i < ctx->alloc; ++i) {
            if (tmp[i] < sp->next_use_out[i]) {
               sp->next_use_out[i] = tmp[i];
               progress = true;
            }
         }

         if (progress)
            agx_worklist_push_tail(&worklist, *pred);

         free(tmp);
      }

      free(nu);
      free(defined);
   }

   u_worklist_fini(&worklist);
}

static void
assert_implies_liveness(uint32_t *distance, BITSET_WORD *live, unsigned count)
{
   for (unsigned i = 0; i < count; ++i) {
      /* A value is live iff it has a finite next-use distance */
      bool implied_live = (distance[i] < DIST_INFINITY);

      assert(BITSET_TEST(live, i) == implied_live);
   }
}

static void
validate_next_use_info(agx_context *ctx, struct spill_block *blocks)
{
   agx_compute_liveness(ctx);

   agx_foreach_block(ctx, blk) {
      /* Next-use analysis implies liveness */
      assert_implies_liveness(blocks[blk->index].next_use_in, blk->live_in,
                              ctx->alloc);

      assert_implies_liveness(blocks[blk->index].next_use_out, blk->live_out,
                              ctx->alloc);
   }
}

/*
 * Lower to conventional SSA naively, inserting moves for every phi source. This
 * could be made smarter but so far I don't see a huge point in doing so.
 */
static void
lower_to_cssa(agx_context *ctx)
{
   agx_foreach_block(ctx, block) {
      agx_foreach_predecessor(block, pred) {
         agx_builder b = agx_init_builder(ctx, agx_after_block_logical(*pred));
         unsigned s = agx_predecessor_index(block, *pred);

         agx_foreach_phi_in_block(block, phi) {
            phi->src[s] = agx_mov(&b, phi->src[s]);
         }
      }
   }
}

void
agx_spill(agx_context *ctx, unsigned k)
{
   /* Lower to conventional SSA. This is required for spiller correctness. */
   lower_to_cssa(ctx);

   /* If control flow is used, we force the nesting counter (r0l) live
    * throughout the shader. Just subtract that from our limit so we can forget
    * about it while spilling.
    */
   if (ctx->any_cf)
      k--;

   /* If we've called agx_spill, we assume we will (in fact) spill something.
    * Eligible shaders will spill to local memory instead of the stack, which is
    * much faster but requires 2 extra 16-bit temporary registers.
    */
   bool promote =
      agx_can_promote_scratch_to_local(ctx, 2 /* minimum allocation */, NULL);

   if (promote)
      k -= 2;

   uint8_t *ncomps = agx_collect_ncomps(ctx);
   enum agx_size *sizes = calloc(sizeof(enum agx_size), ctx->alloc);
   unsigned *spill_locations = calloc(sizeof(unsigned), ctx->alloc);
   BITSET_WORD *assigned_spill_location =
      calloc(sizeof(BITSET_WORD), BITSET_WORDS(ctx->alloc));
   struct hash_table_u64 *trivial_phi_map = _mesa_hash_table_u64_create(NULL);
   agx_instr **remat = calloc(sizeof(agx_instr *), ctx->alloc);

   agx_foreach_instr_global(ctx, I) {
      if (I->op == AGX_OPCODE_MOV_IMM)
         remat[I->dest[0].value] = I;

      /* Measure vectors */
      agx_foreach_ssa_dest(I, d) {
         assert(sizes[I->dest[d].value] == 0 && "broken SSA");
         sizes[I->dest[d].value] = I->dest[d].size;
      }
   }

   struct spill_block *blocks =
      calloc(sizeof(struct spill_block), ctx->num_blocks);
   signed block_index = -1;

   agx_foreach_block(ctx, block) {
      struct spill_block *sb = &blocks[block->index];
      sb->defs = calloc(sizeof(sb->defs[0]), ctx->alloc);

      assert(block_index < (signed)block->index && "must be monotonic");
      block_index = block->index;
   }

   /* Step 1. Compute global next-use distances */
   global_next_use_distances(ctx, blocks);

   /* Validate the next-use distances */
   validate_next_use_info(ctx, blocks);

   unsigned n = ctx->alloc;

   BITSET_WORD *W = malloc(BITSET_WORDS(n) * sizeof(BITSET_WORD));
   BITSET_WORD *S = malloc(BITSET_WORDS(n) * sizeof(BITSET_WORD));

   agx_foreach_block(ctx, block) {
      memset(W, 0, BITSET_WORDS(n) * sizeof(BITSET_WORD));
      memset(S, 0, BITSET_WORDS(n) * sizeof(BITSET_WORD));

      struct spill_ctx sctx = {
         .shader = ctx,
         .n = n,
         .ncomps = ncomps,
         .size = sizes,
         .remat = remat,
         .block = block,
         .blocks = blocks,
         .k = k,
         .W = W,
         .S = S,
         .trivial_phi_map = trivial_phi_map,
         .spill_locations = spill_locations,
         .assigned_spill_location = assigned_spill_location,
      };

      compute_w_entry(&sctx);
      compute_s_entry(&sctx);
      insert_coupling_code_early(&sctx, block, blocks);
      min_algorithm(&sctx);
      seal_loop_headers(&sctx);
      insert_coupling_code_late(&sctx, block, blocks);
   }

   agx_foreach_block(ctx, block) {
      struct spill_block *sb = &blocks[block->index];
      free(sb->defs);
      free(sb->next_use_in);
      free(sb->next_use_out);
   }

   _mesa_hash_table_u64_destroy(trivial_phi_map);

   free(W);
   free(S);
   free(remat);
   free(spill_locations);
   free(assigned_spill_location);
   free(sizes);
   free(ncomps);

   /* Spills and fills come as pairs */
   assert((ctx->spills > 0) == (ctx->fills > 0));

   /* Remat can introduce dead code */
   agx_dce(ctx, false);

   /* Promote scratch to local if applicable */
   if (promote && ctx->spills)
      agx_promote_scratch_to_local(ctx);
}
