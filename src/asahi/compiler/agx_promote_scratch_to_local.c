/*
 * Copyright 2023 Valve Corporation
 * SPDX-License-Identifier: MIT
 */

#include "compiler/shader_enums.h"
#include "agx_builder.h"
#include "agx_compiler.h"
#include "agx_opcodes.h"

static agx_index
agx_constant_u16(agx_builder *b, uint16_t x)
{
   if (x < 0x100) {
      return agx_immediate(x);
   } else {
      agx_index tmp = agx_temp(b->shader, AGX_SIZE_16);
      agx_mov_imm_to(b, tmp, x);
      return tmp;
   }
}

static unsigned
round_size(unsigned size)
{
   /* The spill code might do 32-bit access, so we need to make sure we have
    * 32-bit alignment for all threads.
    */
   return ALIGN_POT(size, 4);
}

bool
agx_can_promote_scratch_to_local(agx_context *ctx, unsigned scratch_size,
                                 unsigned *out_local_size)
{
   /* We can only optimize compute-like shaders, other stages do not support
    * local memory access.
    */
   if (!gl_shader_stage_is_compute(ctx->stage))
      return false;

   /* We can only this in the main part of a shader where we control shared */
   if (ctx->is_preamble)
      return false;

   /* We could optimize the OpenCL case but so far we don't */
   if (ctx->nir->info.cs.has_variable_shared_mem)
      return false;

   /* We need to know the workgroup size statically for the allocations */
   if (ctx->nir->info.workgroup_size_variable)
      return false;

   /* If we did promote everything to scratch, work out the size it would be */
   unsigned threads_per_workgroup = ctx->nir->info.workgroup_size[0] *
                                    ctx->nir->info.workgroup_size[1] *
                                    ctx->nir->info.workgroup_size[2];

   unsigned scratch_stride = round_size(scratch_size);
   unsigned scratch_base = round_size(ctx->out->local_size);

   unsigned total_shared =
      scratch_base + (scratch_stride * threads_per_workgroup);

   /* So far we do not promote partially. TODO: Generalize. */
   if (total_shared > 32768)
      return false;

   /* Otherwise we can promote */
   if (out_local_size)
      *out_local_size = total_shared;

   return true;
}

void
agx_promote_scratch_to_local(agx_context *ctx)
{
   /* No point in promoting nothing */
   if (ctx->out->scratch_size == 0)
      return;

   unsigned scratch_stride = round_size(ctx->out->scratch_size);
   unsigned scratch_base = round_size(ctx->out->local_size);

   /* Try to promote */
   if (!agx_can_promote_scratch_to_local(ctx, scratch_stride,
                                         &ctx->out->local_size))
      return;

   /* Success! */
   ctx->out->scratch_size = 0;

   /* Calculate the base for promoted scratch for the thread */
   agx_builder b =
      agx_init_builder(ctx, agx_before_block(agx_start_block(ctx)));

   agx_index thread = agx_get_sr(&b, 16, AGX_SR_THREAD_INDEX_IN_THREADGROUP);

   agx_index thread_scratch_base =
      agx_imad(&b, thread, agx_constant_u16(&b, scratch_stride),
               agx_constant_u16(&b, scratch_base), 0);

   agx_index tmp = agx_temp(ctx, AGX_SIZE_16);
   agx_mov_to(&b, tmp, thread_scratch_base);
   thread_scratch_base = tmp;

   /* Promote */
   agx_foreach_instr_global_safe(ctx, I) {
      if (I->op != AGX_OPCODE_STACK_LOAD && I->op != AGX_OPCODE_STACK_STORE)
         continue;

      b.cursor = agx_before_instr(I);

      agx_index scratch_offset = I->src[I->op == AGX_OPCODE_STACK_LOAD ? 0 : 1];

      /* Memory immediates can be bigger than ALU, so insert a move if needed */
      if (scratch_offset.type == AGX_INDEX_IMMEDIATE)
         scratch_offset = agx_constant_u16(&b, scratch_offset.value);

      agx_index base = thread_scratch_base;
      agx_index index = agx_zero();

      /* TODO: Use index. It doesn't work properly on G13 due to a hardware bug
       * (looks like overzealous bounds checking?). Need to replicate the
       * workaround the blob uses.
       */
      if (!(scratch_offset.type == AGX_INDEX_IMMEDIATE &&
            scratch_offset.value == 0)) {

         base = agx_iadd(&b, thread_scratch_base, scratch_offset, 0);
      }

      if (I->op == AGX_OPCODE_STACK_LOAD) {
         agx_local_load_to(&b, I->dest[0], base, index, I->format, I->mask);
      } else {
         agx_local_store(&b, I->src[0], base, index, I->format, I->mask);
      }

      agx_remove_instruction(I);
   }
}
