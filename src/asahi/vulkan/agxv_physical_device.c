/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_physical_device.h"

#include "agxv_entrypoints.h"
#include "agxv_instance.h"
#include "agxv_limits.h"
#include "agxv_wsi.h"
#include "vk_physical_device_properties.h"

#include "vulkan/runtime/vk_device.h"
#include "vulkan/runtime/vk_drm_syncobj.h"
#include "vulkan/runtime/vk_sync_dummy.h"
#include "vulkan/vulkan_core.h"
#include "vulkan/wsi/wsi_common.h"

static const struct debug_named_value agx_debug_options[] = {
   {"trace", AGX_DBG_TRACE, "Trace the command stream"},
   {"deqp", AGX_DBG_DEQP, "Hacks for dEQP"},
   {"no16", AGX_DBG_NO16, "Disable 16-bit support"},
   {"perf", AGX_DBG_PERF, "Print performance warnings"},
#ifndef NDEBUG
   {"dirty", AGX_DBG_DIRTY, "Disable dirty tracking"},
#endif
   {"precompile", AGX_DBG_PRECOMPILE, "Precompile shaders for shader-db"},
   {"nocompress", AGX_DBG_NOCOMPRESS, "Disable lossless compression"},
   {"nocluster", AGX_DBG_NOCLUSTER, "Disable vertex clustering"},
   {"sync", AGX_DBG_SYNC, "Synchronously wait for all submissions"},
   {"stats", AGX_DBG_STATS, "Show command execution statistics"},
   DEBUG_NAMED_VALUE_END};

PUBLIC VKAPI_ATTR PFN_vkVoidFunction VKAPI_CALL
vk_icdGetPhysicalDeviceProcAddr(VkInstance _instance, const char *pName)
{
   VK_FROM_HANDLE(agxv_instance, instance, _instance);
   return vk_instance_get_physical_device_proc_addr(&instance->vk, pName);
}

static void
agxv_get_device_extensions(const struct agxv_physical_device *device,
                           struct vk_device_extension_table *ext)
{
   *ext = (struct vk_device_extension_table){
      .KHR_get_memory_requirements2 = true,
      .KHR_copy_commands2 = true,
      .KHR_variable_pointers = true,
      .KHR_swapchain = true,
      .EXT_index_type_uint8 = true,
      .EXT_scalar_block_layout = true,
      .KHR_8bit_storage = true,
      .KHR_16bit_storage = true,
      .KHR_shader_float16_int8 = true,
      .KHR_storage_buffer_storage_class = true,
      .KHR_dynamic_rendering = true,
      /* TODO: what is it? */
      .KHR_push_descriptor = true,
   };
}

static void
agxv_get_features(const struct agxv_physical_device *physical_device,
                  struct vk_features *features)
{
   *features = (struct vk_features){
      /* Vulkan 1.0 */
      .robustBufferAccess = false,
      .fullDrawIndexUint32 = true,
      .imageCubeArray = false,
      .independentBlend = true,
      .geometryShader = false,
      .tessellationShader = false,
      .sampleRateShading = false,
      .dualSrcBlend = true,
      .logicOp = true,
      .multiDrawIndirect = false,
      .drawIndirectFirstInstance = false,
      .depthClamp = false,
      .depthBiasClamp = false,
      .fillModeNonSolid = false,
      .depthBounds = false,
      .wideLines = true,
      .largePoints = true,
      .alphaToOne = false,
      .multiViewport = false,
      .samplerAnisotropy = true,
      .textureCompressionETC2 = false,
      .textureCompressionASTC_LDR = false,
      .textureCompressionBC = false,
      .occlusionQueryPrecise = false,
      .pipelineStatisticsQuery = false,
      .vertexPipelineStoresAndAtomics = false,
      .fragmentStoresAndAtomics = true,
      .shaderTessellationAndGeometryPointSize = false,
      .shaderImageGatherExtended = true,
      .shaderStorageImageExtendedFormats = false,
      .shaderStorageImageMultisample = false,
      .shaderStorageImageReadWithoutFormat = true,
      .shaderStorageImageWriteWithoutFormat = true,
      .shaderUniformBufferArrayDynamicIndexing = true,
      .shaderSampledImageArrayDynamicIndexing = true,
      .shaderStorageBufferArrayDynamicIndexing = true,
      .shaderStorageImageArrayDynamicIndexing = true,
      .shaderClipDistance = false,
      .shaderCullDistance = false,
      .shaderFloat64 = false,
      .shaderInt64 = true,
      .shaderInt16 = true,
      .sparseBinding = false,
      .variableMultisampleRate = false,
      .inheritedQueries = false,

      /* Vulkan 1.1 */
      .storageBuffer16BitAccess = true,
      .uniformAndStorageBuffer16BitAccess = true,
      .storagePushConstant16 = false,
      .storageInputOutput16 = false,
      .multiview = false,
      .multiviewGeometryShader = false,
      .multiviewTessellationShader = false,
      .variablePointersStorageBuffer = false,
      .variablePointers = false,
      .protectedMemory = false,
      .samplerYcbcrConversion = false,
      .shaderDrawParameters = false,

      /* Vulkan 1.2 */
      .samplerMirrorClampToEdge = true,
      .drawIndirectCount = false,
      .storageBuffer8BitAccess = true,
      .uniformAndStorageBuffer8BitAccess = true,
      .storagePushConstant8 = false,
      .shaderBufferInt64Atomics = false,
      .shaderSharedInt64Atomics = false,
      .shaderFloat16 = false,
      .shaderInt8 = false,

      .descriptorIndexing = false,
      .shaderInputAttachmentArrayDynamicIndexing = false,
      .shaderUniformTexelBufferArrayDynamicIndexing = false,
      .shaderStorageTexelBufferArrayDynamicIndexing = false,
      .shaderUniformBufferArrayNonUniformIndexing = false,
      .shaderSampledImageArrayNonUniformIndexing = false,
      .shaderStorageBufferArrayNonUniformIndexing = false,
      .shaderStorageImageArrayNonUniformIndexing = false,
      .shaderInputAttachmentArrayNonUniformIndexing = false,
      .shaderUniformTexelBufferArrayNonUniformIndexing = false,
      .shaderStorageTexelBufferArrayNonUniformIndexing = false,
      .descriptorBindingUniformBufferUpdateAfterBind = false,
      .descriptorBindingSampledImageUpdateAfterBind = false,
      .descriptorBindingStorageImageUpdateAfterBind = false,
      .descriptorBindingStorageBufferUpdateAfterBind = false,
      .descriptorBindingUniformTexelBufferUpdateAfterBind = false,
      .descriptorBindingStorageTexelBufferUpdateAfterBind = false,
      .descriptorBindingUpdateUnusedWhilePending = false,
      .descriptorBindingPartiallyBound = false,
      .descriptorBindingVariableDescriptorCount = false,
      .runtimeDescriptorArray = false,

      .samplerFilterMinmax = false,
      .scalarBlockLayout = true,
      .imagelessFramebuffer = false,
      .uniformBufferStandardLayout = false,
      .shaderSubgroupExtendedTypes = false,
      .separateDepthStencilLayouts = false,
      .hostQueryReset = false,
      .timelineSemaphore = false,
      .bufferDeviceAddress = false,
      .bufferDeviceAddressCaptureReplay = false,
      .bufferDeviceAddressMultiDevice = false,
      .vulkanMemoryModel = false,
      .vulkanMemoryModelDeviceScope = false,
      .vulkanMemoryModelAvailabilityVisibilityChains = false,
      .shaderOutputViewportIndex = false,
      .shaderOutputLayer = false,
      .subgroupBroadcastDynamicId = false,

      /* Vulkan 1.3 */
      .robustImageAccess = false,
      .inlineUniformBlock = false,
      .descriptorBindingInlineUniformBlockUpdateAfterBind = false,
      .pipelineCreationCacheControl = false,
      .privateData = false,
      .shaderDemoteToHelperInvocation = false,
      .shaderTerminateInvocation = false,
      .subgroupSizeControl = false,
      .computeFullSubgroups = false,
      .synchronization2 = false,
      .textureCompressionASTC_HDR = false,
      .shaderZeroInitializeWorkgroupMemory = false,
      .dynamicRendering = true,
      .shaderIntegerDotProduct = false,
      .maintenance4 = false,

      /* VK_EXT_index_type_uint8 */
      .indexTypeUint8 = true,

      /* KHR_shader_float16_int8 */
      .shaderInt8 = true,
      .shaderFloat16 = true,

      /* KHR_8bit_storage */
      .uniformAndStorageBuffer8BitAccess = true,
      .storageBuffer8BitAccess = true,
      .storagePushConstant8 = true,

      /* KHR_16bit_storage */
      .uniformAndStorageBuffer16BitAccess = true,
      .storageInputOutput16 = false, /* TODO: 16-bit flatshading */
      .storagePushConstant16 = true,
      .storageBuffer16BitAccess = true,
   };
}

static void
agxv_get_properties(const struct agxv_physical_device *physical_device,
                    struct vk_properties *properties)
{
   unsigned max_fb_size = 16384;

   VkSampleCountFlags sample_counts =
      VK_SAMPLE_COUNT_1_BIT | VK_SAMPLE_COUNT_4_BIT;

   *properties = (struct vk_properties){
      /* Core properties */
      .apiVersion = VK_MAKE_VERSION(1, 0, VK_HEADER_VERSION),
      .driverVersion = vk_get_driver_version(),
      .deviceType = VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU,
      .deviceID = physical_device->dev.params.chip_id,
      .vendorID = VK_VENDOR_ID_MESA,

      /* Core limits */
      .maxImageDimension1D = 16384,
      .maxImageDimension2D = 16384,
      .maxImageDimension3D = 8192,
      .maxImageDimensionCube = 8192,
      .maxImageArrayLayers = 2048,
      .maxTexelBufferElements = (1ul << 24) /* TODO */,
      .maxUniformBufferRange = 65536,
      .maxStorageBufferRange = 1 << 27,
      .maxPushConstantsSize = AGXV_MAX_PUSH,
      .maxMemoryAllocationCount = 4096,
      .maxSamplerAllocationCount = 4000,
      .bufferImageGranularity = 131072,
      .sparseAddressSpaceSize = 0,
      .maxBoundDescriptorSets = AGXV_MAX_SETS,
      .maxPerStageDescriptorSamplers = AGXV_MAX_SAMPLERS,
      .maxPerStageDescriptorUniformBuffers = AGXV_MAX_UNIFORM_BUFFERS,
      .maxPerStageDescriptorStorageBuffers = AGXV_MAX_STORAGE_BUFFERS,
      .maxPerStageDescriptorSampledImages = AGXV_MAX_SAMPLED_IMAGES,
      .maxPerStageDescriptorStorageImages = AGXV_MAX_STORAGE_IMAGES,
      .maxPerStageDescriptorInputAttachments = 4,
      .maxPerStageResources = 128,

      /* Some of these limits are multiplied by 6 because they need to
       * include all possible shader stages (even if not supported). See
       * 'Required Limits' table in the Vulkan spec.
       */
      .maxDescriptorSetSamplers = 6 * AGXV_MAX_SAMPLERS,
      .maxDescriptorSetUniformBuffers = 6 * AGXV_MAX_UNIFORM_BUFFERS,
      .maxDescriptorSetUniformBuffersDynamic = AGXV_MAX_DYNAMIC_UNIFORM_BUFFERS,
      .maxDescriptorSetStorageBuffers = 6 * AGXV_MAX_STORAGE_BUFFERS,
      .maxDescriptorSetStorageBuffersDynamic = AGXV_MAX_DYNAMIC_STORAGE_BUFFERS,
      .maxDescriptorSetSampledImages = 6 * AGXV_MAX_SAMPLED_IMAGES,
      .maxDescriptorSetStorageImages = 6 * AGXV_MAX_STORAGE_IMAGES,
      .maxDescriptorSetInputAttachments = 4,

      /* Vertex limits */
      .maxVertexInputAttributes = AGXV_MAX_VERTEX_ATTRIBS,
      .maxVertexInputBindings = AGXV_MAX_VBS,
      .maxVertexInputAttributeOffset = 0xffffffff,
      .maxVertexInputBindingStride = 0xffffffff,
      .maxVertexOutputComponents = 128,

      /* Fragment limits */
      .maxFragmentInputComponents = 128,
      .maxFragmentOutputAttachments = AGXV_MAX_RENDER_TARGETS,
      .maxFragmentDualSrcAttachments = 0,
      .maxFragmentCombinedOutputResources = AGXV_MAX_RENDER_TARGETS +
                                            AGXV_MAX_STORAGE_BUFFERS +
                                            AGXV_MAX_STORAGE_IMAGES,

      /* Compute limits */
      .maxComputeSharedMemorySize = 16384,
      .maxComputeWorkGroupCount = {65535, 65535, 65535},
      .maxComputeWorkGroupInvocations = 256,
      .maxComputeWorkGroupSize = {256, 256, 256},

      .subPixelPrecisionBits = 4,
      .subTexelPrecisionBits = 8,
      .mipmapPrecisionBits = 8,
      .maxDrawIndexedIndexValue = 0x00ffffff,
      .maxDrawIndirectCount = 0x7fffffff,
      .maxSamplerLodBias = 14.0f,
      .maxSamplerAnisotropy = 16.0f,
      .maxViewports = 1,
      .maxViewportDimensions = {max_fb_size, max_fb_size},
      .viewportBoundsRange = {-2.0 * max_fb_size, 2.0 * max_fb_size - 1},
      .viewportSubPixelBits = 0,
      .minMemoryMapAlignment = 16384 /* 1 page */,
      .minTexelBufferOffsetAlignment = 16,
      .minUniformBufferOffsetAlignment = 16,
      .minStorageBufferOffsetAlignment = 16,
      .minTexelOffset = -8,
      .maxTexelOffset = 7,
      .minTexelGatherOffset = -8,
      .maxTexelGatherOffset = 7,
      .minInterpolationOffset = -0.5,
      .maxInterpolationOffset = 0.5,
      .subPixelInterpolationOffsetBits = 4,
      .maxFramebufferWidth = 16384,
      .maxFramebufferHeight = 16384,
      .maxFramebufferLayers = 2048,
      .framebufferColorSampleCounts = sample_counts,
      .framebufferDepthSampleCounts = sample_counts,
      .framebufferStencilSampleCounts = sample_counts,
      .framebufferNoAttachmentsSampleCounts = sample_counts,
      .maxColorAttachments = AGXV_MAX_RENDER_TARGETS,
      .sampledImageColorSampleCounts = sample_counts,
      .sampledImageIntegerSampleCounts = sample_counts,
      .sampledImageDepthSampleCounts = sample_counts,
      .sampledImageStencilSampleCounts = sample_counts,
      .storageImageSampleCounts = VK_SAMPLE_COUNT_1_BIT,
      .maxSampleMaskWords = 1,
      .timestampComputeAndGraphics = false,
      .timestampPeriod = 0,
      .maxClipDistances = 0,
      .maxCullDistances = 0,
      .maxCombinedClipAndCullDistances = 0,
      .discreteQueuePriorities = 2,
      .pointSizeRange = {1.0, 511.95f},
      .lineWidthRange = {1.0f, 16.0},
      .pointSizeGranularity = 0.1,
      .lineWidthGranularity = 0.1,
      .strictLines = false,
      .standardSampleLocations = true,
      .optimalBufferCopyOffsetAlignment = 16384,
      .optimalBufferCopyRowPitchAlignment = 32,
      .nonCoherentAtomSize = 256,

      /* KHR_push_descriptor */
      .maxPushDescriptors = AGXV_MAX_PUSH_DESCRIPTORS,

      /* Point clipping */
      /* TODO: what is it? */
      .pointClippingBehavior = VK_POINT_CLIPPING_BEHAVIOR_ALL_CLIP_PLANES,

   };

   strncpy(properties->deviceName, physical_device->dev.name,
           sizeof(properties->deviceName));
}

VkResult
agxv_physical_device_try_create(struct vk_instance *vk_instance,
                                struct _drmDevice *drm_device,
                                struct vk_physical_device **out)
{
   struct agxv_instance *instance =
      container_of(vk_instance, struct agxv_instance, vk);
   // const char *primary_path = drm_device->nodes[DRM_NODE_PRIMARY];
   const char *path = drm_device->nodes[DRM_NODE_RENDER];
   VkResult result;
   int fd;

   if (!(drm_device->available_nodes & (1 << DRM_NODE_RENDER)) ||
       drm_device->bustype != DRM_BUS_PLATFORM) {
      return VK_ERROR_INCOMPATIBLE_DRIVER;
   }

   fd = open(path, O_RDWR | O_CLOEXEC);
   if (fd < 0) {
      if (errno == ENOMEM) {
         return vk_errorf(instance, VK_ERROR_OUT_OF_HOST_MEMORY,
                          "Unable to open device %s: out of memory", path);
      }
      return vk_errorf(instance, VK_ERROR_INCOMPATIBLE_DRIVER,
                       "Unable to open device %s: %m", path);
   }

   vk_warn_non_conformant_implementation("agxv");

   struct agxv_physical_device *device =
      vk_zalloc(&instance->vk.alloc, sizeof(*device), 8,
                VK_SYSTEM_ALLOCATION_SCOPE_INSTANCE);

   if (device == NULL) {
      result = vk_error(instance, VK_ERROR_OUT_OF_HOST_MEMORY);
      goto fail_fd;
   }

   device->dev.debug |=
      debug_get_flags_option("ASAHI_MESA_DEBUG", agx_debug_options, 0);
   device->dev.fd = fd;

   if (!agx_open_device(NULL, &device->dev)) {
      result = VK_ERROR_INITIALIZATION_FAILED;
      vk_error(instance, result);
      goto fail_alloc;
   }

   struct vk_physical_device_dispatch_table dispatch_table;
   vk_physical_device_dispatch_table_from_entrypoints(
      &dispatch_table, &agxv_physical_device_entrypoints, true);
   vk_physical_device_dispatch_table_from_entrypoints(
      &dispatch_table, &wsi_physical_device_entrypoints, false);

   struct vk_device_extension_table supported_extensions;
   agxv_get_device_extensions(device, &supported_extensions);

   struct vk_features supported_features;
   agxv_get_features(device, &supported_features);

   struct vk_properties supported_properties;
   agxv_get_properties(device, &supported_properties);

   result = vk_physical_device_init(&device->vk, &instance->vk,
                                    &supported_extensions, &supported_features,
                                    &supported_properties, &dispatch_table);

   if (result != VK_SUCCESS) {
      vk_error(instance, result);
      goto fail_alloc;
   }

   device->instance = instance;

   device->drm_syncobj_type = vk_drm_syncobj_get_type(fd);
   device->drm_syncobj_type.features &= ~VK_SYNC_FEATURE_TIMELINE;
   device->sync_timeline_type =
      vk_sync_timeline_get_type(&device->drm_syncobj_type);

   device->sync_types[0] = &device->drm_syncobj_type;
   device->sync_types[1] = &device->sync_timeline_type.sync;
   device->sync_types[1] = NULL;
   device->vk.supported_sync_types = device->sync_types;

   result = agxv_init_wsi(device);
   // TODO: handle error

   *out = &device->vk;

   return VK_SUCCESS;

fail_alloc:
   vk_free(&instance->vk.alloc, device);

fail_fd:
   close(fd);
   return result;
}

void
agxv_physical_device_destroy(struct vk_physical_device *vk_device)
{
   struct agxv_physical_device *device =
      container_of(vk_device, struct agxv_physical_device, vk);

   agxv_finish_wsi(device);
   vk_free(&device->instance->vk.alloc, device);
}

VKAPI_ATTR void VKAPI_CALL
agxv_GetPhysicalDeviceMemoryProperties2(
   VkPhysicalDevice physicalDevice,
   VkPhysicalDeviceMemoryProperties2 *pMemoryProperties)
{
   pMemoryProperties->memoryProperties = (VkPhysicalDeviceMemoryProperties){
      .memoryTypeCount = 1,
      .memoryTypes[0].heapIndex = 0,
      .memoryTypes[0].propertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT |
                                      VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                                      VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
      .memoryHeapCount = 1,
      /* FIXME: there is no portable way to get memory size. We could use
       * sysinfo(2) but that would be Linux specific. For now we just specify
       * 8GB as there are no Apple Silicon devices with less RAM than this.
       */
      .memoryHeaps[0].size = 8ull * 1024ull * 1024ull * 1024ull,
   };

   vk_foreach_struct(ext, pMemoryProperties->pNext) {
      switch (ext->sType) {
      default:
         agxv_debug_ignored_stype(ext->sType);
         break;
      }
   }
}

VKAPI_ATTR void VKAPI_CALL
agxv_GetPhysicalDeviceQueueFamilyProperties2(
   VkPhysicalDevice physicalDevice, uint32_t *pQueueFamilyPropertyCount,
   VkQueueFamilyProperties2 *pQueueFamilyProperties)
{
   // VK_FROM_HANDLE(agxv_physical_device, pdevice, physicalDevice);
   VK_OUTARRAY_MAKE_TYPED(VkQueueFamilyProperties2, out, pQueueFamilyProperties,
                          pQueueFamilyPropertyCount);

   vk_outarray_append_typed(VkQueueFamilyProperties2, &out, p)
   {
      p->queueFamilyProperties.queueFlags =
         VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT;
      p->queueFamilyProperties.queueCount = 1;
      p->queueFamilyProperties.timestampValidBits = 64;
      p->queueFamilyProperties.minImageTransferGranularity =
         (VkExtent3D){1, 1, 1};
   }
}
