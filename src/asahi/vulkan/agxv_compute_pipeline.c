/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_compute_pipeline.h"

#include "agxv_device.h"

static void
agxv_lower_compute(struct agxv_pipeline *pipeline, nir_shader *nir,
                   struct agx_shader_key *key, void *data)
{
}

static VkResult
agxv_compute_pipeline_create(struct agxv_device *device,
                             const VkComputePipelineCreateInfo *pCreateInfo,
                             const VkAllocationCallbacks *pAllocator,
                             VkPipeline *pPipeline)
{
   VK_FROM_HANDLE(agxv_pipeline_layout, pipeline_layout, pCreateInfo->layout);
   VkResult result = VK_SUCCESS;

   struct agxv_compute_pipeline *pipeline;

   pipeline = vk_object_zalloc(&device->vk, pAllocator, sizeof(*pipeline),
                               VK_OBJECT_TYPE_PIPELINE);
   if (pipeline == NULL)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   pipeline->base.type = AGXV_PIPELINE_COMPUTE;

   const VkPipelineShaderStageCreateInfo *stage = &pCreateInfo->stage;

   agxv_compile_pipeline_stage(device, &pipeline->base, pipeline_layout, stage,
                               agxv_lower_compute, NULL);

   *pPipeline = agxv_compute_pipeline_to_handle(pipeline);
   return result;
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateComputePipelines(VkDevice _device, VkPipelineCache cache,
                            uint32_t createInfoCount,
                            const VkComputePipelineCreateInfo *pCreateInfos,
                            const VkAllocationCallbacks *pAllocator,
                            VkPipeline *pPipelines)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VkResult result = VK_SUCCESS;

   unsigned i = 0;
   for (; i < createInfoCount; i++) {
      result = agxv_compute_pipeline_create(device, &pCreateInfos[i],
                                            pAllocator, &pPipelines[i]);

      if (result != VK_SUCCESS) {
         pPipelines[i] = VK_NULL_HANDLE;

         if (pCreateInfos[i].flags &
             VK_PIPELINE_CREATE_EARLY_RETURN_ON_FAILURE_BIT)
            break;
      }
   }

   for (; i < createInfoCount; i++)
      pPipelines[i] = VK_NULL_HANDLE;

   return result;
}
