/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#ifndef AGXV_BUFFER
#define AGXV_BUFFER 1

#include "agxv_private.h"

#include "agxv_device_memory.h"

#include "vulkan/runtime/vk_buffer.h"

struct agxv_buffer {
   struct vk_buffer vk;
   struct agxv_device_memory *mem;
   uint64_t mem_offset;
};

static struct agxv_buffer *
to_agxv_buffer(struct vk_buffer *vk)
{
   return (struct agxv_buffer *)vk;
}

static inline uint64_t
agxv_buffer_addr(struct agxv_buffer *buffer)
{
   if (buffer->mem->bo == NULL)
      return 0;

   return buffer->mem->bo->ptr.gpu + buffer->mem_offset;
}

static inline uint64_t
agxv_buffer_range(struct agxv_buffer *buffer, uint64_t offset, uint64_t range)
{
   if (buffer->mem->bo == NULL)
      return 0;

   return vk_buffer_range(&buffer->vk, offset, range);
}

VK_DEFINE_HANDLE_CASTS(agxv_buffer, vk.base, VkBuffer, VK_OBJECT_TYPE_BUFFER)

#endif
