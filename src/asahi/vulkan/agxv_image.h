/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#ifndef AGXV_IMAGE
#define AGXV_IMAGE 1

#include "agxv_private.h"

#include "agxv_device_memory.h"

#include "asahi/layout/layout.h"

#include "vulkan/runtime/vk_image.h"

struct agxv_image {
   struct vk_image vk;
   struct ail_layout layout;

   /* TODO */
   bool disjoint;

   struct {
      struct agx_bo *bo;
      VkDeviceSize offset;
   } bindings[3];
};

static inline uint64_t
agxv_image_addr(struct agxv_image *image, unsigned plane)
{
   return image->bindings[plane].bo->ptr.gpu + image->bindings[plane].offset;
}

VK_DEFINE_HANDLE_CASTS(agxv_image, vk.base, VkImage, VK_OBJECT_TYPE_IMAGE)

#endif
