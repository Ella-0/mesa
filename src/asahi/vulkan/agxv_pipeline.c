/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_pipeline.h"

#include "asahi/compiler/agx_compile.h"
#include "asahi/lib/agx_nir_lower_vbo.h"
#include "compiler/nir/nir_builder.h"
#include "compiler/nir_types.h"
#include "compiler/shader_enums.h"
#include "compiler/spirv/nir_spirv.h"
#include "agx_bo.h"
#include "agxv_compute_pipeline.h"
#include "agxv_descriptor_set_layout.h"
#include "agxv_graphics_pipeline.h"
#include "agxv_physical_device.h"
#include "agxv_pipeline_layout.h"
#include "nir.h"
#include "nir_intrinsics_indices.h"
#include "vk_pipeline.h"
#include "vk_shader_module.h"

VKAPI_ATTR void VKAPI_CALL
agxv_DestroyPipeline(VkDevice _device, VkPipeline _pipeline,
                     const VkAllocationCallbacks *pAllocator)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_pipeline, pipeline, _pipeline);

   if (!pipeline)
      return;

   if (pipeline->destroy)
      pipeline->destroy(pipeline);

   for (unsigned i = 0; i < ARRAY_SIZE(pipeline->stages); ++i) {
      if (pipeline->stages[i].bo)
         agx_bo_unreference(pipeline->stages[i].bo);
   }

   vk_object_free(&device->vk, pAllocator, pipeline);
}

/* Register layout is as follows:
 *  - push constants
 *  - sets
 *  - vertex buffers (if vertex input)
 *
 * Buffers must come last in order to maintain pipeline layout compatibility as
 * the number of buffers can change depending on pipeline. Push constant length
 * is constant across pipelines created with the same pipeline layout. The
 * number of sets can be larger for pipelines with a compatible layout. We only
 * need to bind the sets up to the vertex buffer base for the pipeline to
 * maintain compatibility.
 */
struct agxv_uniform_reg_layout {
   uint32_t push_base;
   uint32_t set_base;
   uint32_t buffer_base;
   // for each set, base of textures
   uint8_t sampler_bases[32];
   uint8_t texture_bases[32];
};

struct layouts {
   struct agxv_uniform_reg_layout *reg;
   struct agxv_pipeline_layout *pipeline;
};

/* emit a ubo load to get the address of the binding
 * TODO:
 * This isn't correct for inline ubo, sampler, image and combined
 * descriptors. For those cases the address should be returned.
 */
static nir_def *
lower_load_vulkan_descriptor(nir_builder *b, nir_intrinsic_instr *instr,
                             struct agxv_pipeline_layout *layout,
                             struct agxv_uniform_reg_layout *reg_layout)
{
   nir_intrinsic_instr *parent = nir_src_as_intrinsic(instr->src[0]);
   assert(parent->intrinsic == nir_intrinsic_vulkan_resource_index);
   unsigned set = nir_intrinsic_desc_set(parent);
   struct vk_descriptor_set_layout *vk_set_layout = layout->vk.set_layouts[set];
   struct agxv_descriptor_set_layout *set_layout =
      container_of(vk_set_layout, struct agxv_descriptor_set_layout, vk);
   struct agxv_descriptor_set_binding_layout *binding =
      &set_layout->binding[nir_intrinsic_binding(parent)];

   unsigned register_index = reg_layout->set_base + set * 4;

   nir_def *set_addr = nir_load_preamble(b, 1, 64, .base = register_index);

   nir_def *array_index = parent->src[0].ssa;
   nir_def *offset = nir_iadd_imm(
      b, nir_imul_imm(b, array_index, binding->stride), binding->offset);

   return nir_load_global_constant(
      b, nir_iadd(b, set_addr, nir_u2u64(b, offset)), 4,
      nir_dest_num_components(instr->dest), nir_dest_bit_size(instr->dest));
}

static nir_def *
lower_load_vbo_base_agx(nir_builder *b, nir_intrinsic_instr *instr,
                        struct agxv_uniform_reg_layout *layout)
{
   /* TODO: calculate properly with pipeline layout */
   uint32_t offset = layout->buffer_base + 4 * nir_src_as_uint(instr->src[0]);
   return nir_load_preamble(b, 1, 64, .base = offset);
}

static nir_def *
lower_load_push_constant(nir_builder *b, nir_intrinsic_instr *intr,
                         struct agxv_uniform_reg_layout *layout)
{
   nir_def *buf_addr = nir_load_preamble(b, 1, 64, .base = layout->push_base);
   nir_def *offset =
      nir_iadd_imm(b, intr->src[0].ssa, nir_intrinsic_base(intr));
   nir_def *addr = nir_iadd(b, buf_addr, nir_u2u64(b, offset));

   return nir_load_global_constant(b, addr, nir_intrinsic_align(intr),
                                   nir_dest_num_components(intr->dest),
                                   nir_dest_bit_size(intr->dest));
}

static bool
image_intr_uses_tex_descriptor(nir_intrinsic_instr *intr)
{
   return intr->intrinsic == nir_intrinsic_image_deref_load ||
          intr->intrinsic == nir_intrinsic_image_deref_size;
}

static nir_def *
lower_image(nir_builder *b, nir_intrinsic_instr *intr,
            struct agxv_pipeline_layout *layout,
            struct agxv_uniform_reg_layout *reg_layout)
{
   nir_deref_instr *deref = nir_src_as_deref(intr->src[0]);
   nir_variable *var = nir_deref_instr_get_variable(deref);

   unsigned set = var->data.descriptor_set;
   unsigned binding = var->data.binding;

   struct vk_descriptor_set_layout *vk_set_layout = layout->vk.set_layouts[set];
   struct agxv_descriptor_set_layout *set_layout =
      container_of(vk_set_layout, struct agxv_descriptor_set_layout, vk);

   unsigned reg_base = reg_layout->texture_bases[set];
   unsigned reg_offset = set_layout->binding[binding].texture_index;

   /* Image descriptors come in pairs. First texture, then PBE. */
   if (!image_intr_uses_tex_descriptor(intr))
      reg_offset++;

   nir_def *handle = nir_imm_intN_t(b, reg_base + reg_offset, 16);
   nir_rewrite_image_intrinsic(intr, handle, false);
   return NIR_LOWER_INSTR_PROGRESS;
}

static nir_def *
lower_intrinsic(nir_builder *b, nir_intrinsic_instr *instr,
                struct agxv_pipeline_layout *layout,
                struct agxv_uniform_reg_layout *reg_layout)
{
   switch (instr->intrinsic) {
   case nir_intrinsic_load_vulkan_descriptor:
      return lower_load_vulkan_descriptor(b, instr, layout, reg_layout);

   case nir_intrinsic_image_deref_store:
   case nir_intrinsic_image_deref_load:
   case nir_intrinsic_image_deref_atomic:
   case nir_intrinsic_image_deref_atomic_swap:
   case nir_intrinsic_image_deref_size:
   case nir_intrinsic_image_deref_samples:
      return lower_image(b, instr, layout, reg_layout);

   case nir_intrinsic_load_vbo_base_agx:
      return lower_load_vbo_base_agx(b, instr, reg_layout);

   case nir_intrinsic_load_push_constant:
      return lower_load_push_constant(b, instr, reg_layout);

   default:
      return NULL;
   }
}

static bool
lower_tex_deref(nir_builder *b, nir_tex_instr *tex,
                nir_tex_src_type deref_src_type,
                struct agxv_pipeline_layout *layout,
                struct agxv_uniform_reg_layout *reg_layout)
{
   int deref_src_idx = nir_tex_instr_src_index(tex, deref_src_type);
   if (deref_src_idx < 0)
      return false;

   nir_deref_instr *deref = nir_src_as_deref(tex->src[deref_src_idx].src);
   nir_variable *var = nir_deref_instr_get_variable(deref);

   unsigned set = var->data.descriptor_set;
   unsigned binding = var->data.binding;

   struct vk_descriptor_set_layout *vk_set_layout = layout->vk.set_layouts[set];
   struct agxv_descriptor_set_layout *set_layout =
      container_of(vk_set_layout, struct agxv_descriptor_set_layout, vk);

   unsigned reg_base;
   unsigned reg_offset;
   if (deref_src_type == nir_tex_src_texture_deref) {
      reg_base = reg_layout->texture_bases[set];
      reg_offset = set_layout->binding[binding].texture_index;
   } else {
      assert(deref_src_type == nir_tex_src_sampler_deref);
      reg_base = reg_layout->sampler_bases[set];
      reg_offset = set_layout->binding[binding].sampler_index;
   }

   unsigned reg = reg_base + reg_offset;
   nir_def *handle = nir_imm_int(b, reg);

   unsigned register_index = reg_layout->set_base + set * 4;
   nir_src_rewrite(&tex->src[deref_src_idx].src,
                   handle); // TODO: + set_addr

   if (deref_src_type == nir_tex_src_texture_deref) {
      tex->src[deref_src_idx].src_type = nir_tex_src_texture_offset;
   } else {
      assert(deref_src_type == nir_tex_src_sampler_deref);
      tex->src[deref_src_idx].src_type = nir_tex_src_sampler_offset;
   }
   return true;
}

static bool
lower_tex(nir_builder *b, nir_tex_instr *tex,
          struct agxv_pipeline_layout *layout,
          struct agxv_uniform_reg_layout *reg_layout)
{
   lower_tex_deref(b, tex, nir_tex_src_texture_deref, layout, reg_layout);
   lower_tex_deref(b, tex, nir_tex_src_sampler_deref, layout, reg_layout);
   return true;
}

static bool
lower_pipeline_layout(nir_builder *b, nir_instr *instr, void *data)
{
   struct layouts *layout = data;
   b->cursor = nir_before_instr(instr);

   switch (instr->type) {
   case nir_instr_type_intrinsic: {
      nir_intrinsic_instr *intr = nir_instr_as_intrinsic(instr);
      nir_def *repl = lower_intrinsic(b, intr, layout->pipeline, layout->reg);
      if (!repl)
         return false;

      if (repl == NIR_LOWER_INSTR_PROGRESS)
         return true;

      nir_def_rewrite_uses(&intr->dest.ssa, repl);
      nir_instr_remove(instr);
      return true;
   }

   case nir_instr_type_tex:
      return lower_tex(b, nir_instr_as_tex(instr), layout->pipeline,
                       layout->reg);
   default:
      return false;
   }
}

static nir_address_format
address_format_for_buffers(struct agxv_device *dev)
{
   if (dev->vk.enabled_features.robustBufferAccess)
      return nir_address_format_64bit_bounded_global;
   else
      return nir_address_format_64bit_global_32bit_offset;
}

/* Default options. UBO/SSBO address formats set dynamically. */
static const struct spirv_to_nir_options default_spirv_options = {
   .caps =
      {
         .storage_8bit = true,
         .storage_16bit = true,
         .float16 = true,
         .int8 = true,
         .int16 = true,
         .int64 = true,
         .image_read_without_format = true,
         .image_write_without_format = true,
      },
   .push_const_addr_format = nir_address_format_32bit_offset,
};

static void
shared_type_info(const struct glsl_type *type, unsigned *size, unsigned *align)
{
   assert(glsl_type_is_vector_or_scalar(type));

   uint32_t comp_size =
      glsl_type_is_boolean(type) ? 1 : glsl_get_bit_size(type) / 8;

   *size = comp_size * glsl_get_vector_elements(type);
   *align = comp_size;
}

static bool
lower_global_offset(nir_builder *b, nir_instr *instr, void *data)
{
   b->cursor = nir_before_instr(instr);
   if (instr->type != nir_instr_type_intrinsic)
      return false;

   nir_intrinsic_instr *intr = nir_instr_as_intrinsic(instr);
   nir_def *repl = NULL;

   switch (intr->intrinsic) {
   case nir_intrinsic_load_global_constant_offset:
      repl = nir_load_global_constant(
         b, nir_iadd(b, intr->src[0].ssa, nir_u2u64(b, intr->src[1].ssa)),
         nir_intrinsic_align(intr), nir_dest_num_components(intr->dest),
         nir_dest_bit_size(intr->dest));
      break;
   default:
      return false;
   }

   nir_def_rewrite_uses(&intr->dest.ssa, repl);
   nir_instr_remove(&intr->instr);
   return true;
}

VkResult
agxv_compile_pipeline_stage(
   struct agxv_device *dev, struct agxv_pipeline *pipeline,
   struct agxv_pipeline_layout *layout,
   const VkPipelineShaderStageCreateInfo *pCreateInfo,
   void (*stage_specific_lowering)(struct agxv_pipeline *pipeline,
                                   nir_shader *nir, struct agx_shader_key *,
                                   void *),
   void *data)
{
   VkResult result = VK_SUCCESS;
   gl_shader_stage stage = vk_to_mesa_shader_stage(pCreateInfo->stage);

   struct agx_shader_info *info = &pipeline->stages[stage].info;

   struct spirv_to_nir_options spirv_options = default_spirv_options;
   spirv_options.ubo_addr_format = address_format_for_buffers(dev);
   spirv_options.ssbo_addr_format = address_format_for_buffers(dev);

   nir_shader *nir;
   result = vk_pipeline_shader_stage_to_nir(
      &dev->vk, pCreateInfo, &spirv_options, &agx_nir_options, NULL, &nir);
   if (result != VK_SUCCESS)
      return result;

   struct util_dynarray bin;
   util_dynarray_init(&bin, NULL);

   unsigned set_base = layout->push_size ? 4 : 0;
   struct agxv_uniform_reg_layout reg_layout = {
      .push_base = 0,
      .set_base = set_base,
      .buffer_base = set_base + 4 * layout->vk.set_count,
   };

   nir_assign_io_var_locations(nir, nir_var_shader_in, &nir->num_inputs, stage);
   nir_assign_io_var_locations(nir, nir_var_shader_out, &nir->num_outputs,
                               stage);

   NIR_PASS_V(nir, nir_lower_io_to_temporaries, nir_shader_get_entrypoint(nir),
              true, false);

   /* Point coords are varyings. Frag coords and front facing are sysvals. */
   struct nir_lower_sysvals_to_varyings_options options = {
      .point_coord = true,
   };
   NIR_PASS_V(nir, nir_lower_sysvals_to_varyings, &options);

   NIR_PASS_V(nir, nir_lower_system_values);

   /* Lower all indirects away */
   NIR_PASS_V(nir, nir_lower_indirect_derefs, nir_var_all, ~0);

   /* Lower pipeline layout early so we get real image/bindless_image
    * instructions for agx_preprocess_nir to lower, rather than derefs.
    */
   NIR_PASS_V(nir, nir_shader_instructions_pass, lower_pipeline_layout,
              nir_metadata_block_index | nir_metadata_dominance,
              &(struct layouts){.reg = &reg_layout, .pipeline = layout});

   /* Lower explicit I/O derefs before agx_preprocess_nir, since that expects
    * lowered I/O for shared memory.
    */
   if (!nir->info.shared_memory_explicit_layout) {
      NIR_PASS_V(nir, nir_lower_vars_to_explicit_types, nir_var_mem_shared,
                 shared_type_info);
   }

   NIR_PASS_V(nir, nir_lower_explicit_io, nir_var_mem_ubo | nir_var_mem_ssbo,
              spirv_options.ubo_addr_format);
   NIR_PASS_V(nir, nir_lower_explicit_io,
              nir_var_mem_push_const | nir_var_mem_shared,
              nir_address_format_32bit_offset);

   NIR_PASS_V(nir, nir_shader_instructions_pass, lower_global_offset,
              nir_metadata_block_index | nir_metadata_dominance, NULL);

   /* Now preprocess. */
   struct agx_uncompiled_shader_info uncompiled_info;
   agx_preprocess_nir(nir, false, true, &uncompiled_info);

   /* Stash the information about the varyings for proper linkage */
   if (stage == MESA_SHADER_FRAGMENT) {
      struct agxv_graphics_pipeline *gfx = (void *)pipeline;

      gfx->varyings_flat_shaded = uncompiled_info.inputs_flat_shaded;
      gfx->varyings_linear_shaded = uncompiled_info.inputs_linear_shaded;
   }

   /* backend compiler expects us to do this for performance */
   if (stage == MESA_SHADER_FRAGMENT && nir->info.fs.uses_discard)
      NIR_PASS_V(nir, nir_opt_conditional_discard);

   struct agx_shader_key key = {
      .reserved_preamble = reg_layout.buffer_base,
   };
   pipeline->buffer_base = reg_layout.buffer_base;

   stage_specific_lowering(pipeline, nir, &key, data);

   /* XXX: Don't duplicate, needed for late vbo base lowering */
   NIR_PASS_V(nir, nir_shader_instructions_pass, lower_pipeline_layout,
              nir_metadata_block_index | nir_metadata_dominance,
              &(struct layouts){.reg = &reg_layout, .pipeline = layout});

   agx_compile_shader_nir(nir, &key, NULL, &bin, info);

   if (result != VK_SUCCESS)
      return result;
   pipeline->stages[stage].bo = agx_bo_create(
      &dev->pdev->dev, bin.size, AGX_BO_LOW_VA | AGX_BO_EXEC, "Shader");
   memcpy(pipeline->stages[stage].bo->ptr.cpu, bin.data, bin.size);

   if (stage == MESA_SHADER_COMPUTE) {
      struct agxv_compute_pipeline *compute =
         agxv_as_compute_pipeline(pipeline);

      assert(!nir->info.workgroup_size_variable && "not supported in Vulkan");
      for (unsigned i = 0; i < 3; ++i)
         compute->local_size[i] = nir->info.workgroup_size[i];
   }

   ralloc_free(nir);
   return result;
}
