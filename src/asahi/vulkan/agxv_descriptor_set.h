/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "vulkan/vulkan_core.h"
#include "vk_descriptor_update_template.h"
#ifndef AGXV_DESCRIPTOR_SET
#define AGXV_DESCRIPTOR_SET 1

#include "agxv_private.h"

#include "agxv_descriptor_pool.h"
#include "agxv_descriptor_set_layout.h"
#include "agxv_device.h"
#include "agxv_limits.h"

#include "asahi/lib/agx_bo.h"

struct agxv_push_descriptor_set {
   uint8_t data[AGXV_PUSH_DESCRIPTOR_SET_SIZE];

   struct agx_texture_packed textures[AGXV_MAX_TEXTURES];
   struct agx_sampler_packed samplers[AGXV_MAX_SAMPLERS];
};

struct agxv_descriptor_set {
   struct vk_object_base base;
   struct agxv_descriptor_set_layout *layout;

   uint32_t bo_offset;
   uint32_t bo_size;
   struct agx_bo *bo;
   void *mapped_ptr;

   struct agx_texture_packed textures[AGXV_MAX_TEXTURES];
   struct agx_sampler_packed samplers[AGXV_MAX_SAMPLERS];
};

static inline uint64_t
agxv_descriptor_set_addr(struct agxv_descriptor_set *set)
{
   return set->bo->ptr.gpu + set->bo_offset;
}

static inline void *
agxv_descriptor_set_addr_cpu(struct agxv_descriptor_set *set)
{
   return set->bo->ptr.cpu + set->bo_offset;
}

/* This has to match nir_adddress_format_64bit_bounded_global */
struct agxv_buffer_descriptor {
   uint64_t ptr;
   uint32_t size;
   uint32_t zero;
};

static inline uint32_t
agxv_binding_stride(VkDescriptorType type)
{
   switch (type) {
   case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
   case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER:
      return sizeof(struct agxv_buffer_descriptor);

   case VK_DESCRIPTOR_TYPE_SAMPLER:
      return AGX_SAMPLER_LENGTH + AGX_BORDER_LENGTH;

   case VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER:
      return AGX_SAMPLER_LENGTH + AGX_BORDER_LENGTH + AGX_TEXTURE_LENGTH;

   case VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE:
   case VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER:
      return AGX_TEXTURE_LENGTH;

   case VK_DESCRIPTOR_TYPE_STORAGE_IMAGE:
   case VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER:
      return AGX_TEXTURE_LENGTH + AGX_PBE_LENGTH;

   default:
      unreachable("Unsupported binding type");
   }
}

void agxv_push_descriptor_set_update(struct agxv_push_descriptor_set *push_set,
                                     struct agxv_descriptor_set_layout *layout,
                                     uint32_t write_count,
                                     const VkWriteDescriptorSet *writes);

void agxv_push_descriptor_set_update_template(
   struct agxv_push_descriptor_set *push_set,
   struct agxv_descriptor_set_layout *layout,
   const struct vk_descriptor_update_template *templat, const void *data);

VK_DEFINE_HANDLE_CASTS(agxv_descriptor_set, base, VkDescriptorSet,
                       VK_OBJECT_TYPE_DESCRIPTOR_SET)

#endif
