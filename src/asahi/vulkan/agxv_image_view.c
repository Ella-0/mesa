/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_image_view.h"
#include "asahi/layout/layout.h"
#include "asahi/lib/agx_helpers.h"
#include "util/format/u_format.h"
#include "vulkan/vulkan_core.h"

#include "agx_bo.h"
#include "agx_pack.h"
#include "agxv_image.h"

#include "agxv_buffer.h"
#include "agxv_device.h"
#include "agxv_physical_device.h"

#include "vk_buffer_view.h"
#include "vk_format.h"

static enum agx_texture_dimension
translate_view_type(VkImageViewType type, bool multisampled)
{
   switch (type) {
   case VK_IMAGE_VIEW_TYPE_1D:
   case VK_IMAGE_VIEW_TYPE_2D:
      return multisampled ? AGX_TEXTURE_DIMENSION_2D_MULTISAMPLED
                          : AGX_TEXTURE_DIMENSION_2D;

   case VK_IMAGE_VIEW_TYPE_1D_ARRAY:
   case VK_IMAGE_VIEW_TYPE_2D_ARRAY:
      return multisampled ? AGX_TEXTURE_DIMENSION_2D_ARRAY_MULTISAMPLED
                          : AGX_TEXTURE_DIMENSION_2D_ARRAY;

   case VK_IMAGE_VIEW_TYPE_3D:
      assert(!multisampled);
      return AGX_TEXTURE_DIMENSION_3D;

   case VK_IMAGE_VIEW_TYPE_CUBE:
      assert(!multisampled);
      return AGX_TEXTURE_DIMENSION_CUBE;

   case VK_IMAGE_VIEW_TYPE_CUBE_ARRAY:
      assert(!multisampled);
      return AGX_TEXTURE_DIMENSION_CUBE_ARRAY;

   default:
      unreachable("Invalid image type");
   }
}

static void
agxv_pack_tex(struct agxv_device *device, struct agxv_image_view *view)
{
   struct agxv_image *image = (struct agxv_image *)view->vk.image;

   enum pipe_format p_format = vk_format_to_pipe_format(view->vk.format);
   struct agx_pixel_format_entry format = agx_pixel_format[p_format];
   const struct util_format_description *desc =
      util_format_description(p_format);

   uint8_t format_swizzle[] = {
      desc->swizzle[0],
      desc->swizzle[1],
      desc->swizzle[2],
      desc->swizzle[3],
   };
   uint8_t view_swizzle[4];
   vk_component_mapping_to_pipe_swizzle(view->vk.swizzle, view_swizzle);

   uint8_t out_swizzle[4];
   util_format_compose_swizzles(format_swizzle, view_swizzle, out_swizzle);

   bool msaa = view->vk.image->samples > 1;

   agx_pack(&view->texture_desc, TEXTURE, cfg) {
      cfg.dimension = translate_view_type(view->vk.view_type, msaa);
      cfg.layout = agx_translate_layout(image->layout.tiling);

      cfg.type = format.type;
      cfg.channels = format.channels;

      cfg.swizzle_r = agx_channel_from_pipe(out_swizzle[0]);
      cfg.swizzle_g = agx_channel_from_pipe(out_swizzle[1]);
      cfg.swizzle_b = agx_channel_from_pipe(out_swizzle[2]);
      cfg.swizzle_a = agx_channel_from_pipe(out_swizzle[3]);

      cfg.width = view->image->layout.width_px;
      cfg.height = view->image->layout.height_px;

      cfg.first_level = view->vk.base_mip_level;
      cfg.last_level = view->vk.base_mip_level + view->vk.level_count - 1;

      cfg.srgb = vk_format_is_srgb(view->vk.format);
      cfg.unk_mipmapped = view->image->vk.mip_levels > 1;
      cfg.srgb_2_channel =
         cfg.srgb && (vk_format_get_nr_components(view->vk.format) == 2);

      if (ail_is_compressed(&view->image->layout)) {
         cfg.compressed_1 = true;
         cfg.extended = true;
      }

      // TODO: layers
      assert(view->vk.base_array_layer < view->image->layout.depth_px);

      cfg.address =
         agxv_image_addr(view->image, 0) +
         ail_get_layer_offset_B(&image->layout, view->vk.base_array_layer);
      assert(view->image->bindings[0].offset + image->layout.size_B <=
                view->image->bindings[0].bo->size &&
             "no overflow");

      // TODO: compressed
      cfg.depth = view->vk.layer_count;

      if (view->vk.image->samples == VK_SAMPLE_COUNT_4_BIT)
         cfg.samples = AGX_SAMPLE_COUNT_4;
      else
         cfg.samples = AGX_SAMPLE_COUNT_2;

      if (view->vk.image->tiling == VK_IMAGE_TILING_LINEAR) {
         cfg.stride = ail_get_linear_stride_B(&view->image->layout, 0) - 16;
      } else {
         cfg.page_aligned_layers = view->image->layout.page_aligned_layers;
      }
   }
}

static void
agxv_pack_tex_buffer(struct agxv_device *device, struct agxv_buffer_view *view)
{
   enum pipe_format p_format = vk_format_to_pipe_format(view->vk.format);
   struct agx_pixel_format_entry format = agx_pixel_format[p_format];
   const struct util_format_description *desc =
      util_format_description(p_format);

   agx_pack(&view->texture_desc, TEXTURE, cfg) {
      cfg.dimension = AGX_TEXTURE_DIMENSION_2D;
      cfg.layout = AGX_LAYOUT_LINEAR;
      cfg.first_level = 0;
      cfg.last_level = 0;
      cfg.depth = 1;

      cfg.type = format.type;
      cfg.channels = format.channels;
      cfg.swizzle_r = agx_channel_from_pipe(desc->swizzle[0]);
      cfg.swizzle_g = agx_channel_from_pipe(desc->swizzle[1]);
      cfg.swizzle_b = agx_channel_from_pipe(desc->swizzle[2]);
      cfg.swizzle_a = agx_channel_from_pipe(desc->swizzle[3]);

      /* Use a 2D texture to increase the maximum size */
      cfg.width = 1024; /* XXX: common #define */
      cfg.height = DIV_ROUND_UP(view->vk.elements, cfg.width);
      cfg.stride = (cfg.width * util_format_get_blocksize(p_format)) - 16;

      cfg.srgb = vk_format_is_srgb(view->vk.format);
      cfg.srgb_2_channel =
         cfg.srgb && (vk_format_get_nr_components(view->vk.format) == 2);

      cfg.address =
         agxv_buffer_addr(to_agxv_buffer(view->vk.buffer)) + view->vk.offset;

      /* Stash the actual size in an unused part of the texture descriptor,
       * which we'll read later to implement txs.
       */
      cfg.acceleration_buffer = (view->vk.elements << 4);
   }
}

static void
agxv_pack_pbe_buffer(struct agxv_device *device, struct agxv_buffer_view *view)
{
   enum pipe_format p_format = vk_format_to_pipe_format(view->vk.format);
   struct agx_pixel_format_entry format = agx_pixel_format[p_format];
   const struct util_format_description *desc =
      util_format_description(p_format);

   uint64_t base =
      agxv_buffer_addr(to_agxv_buffer(view->vk.buffer)) + view->vk.offset;

   struct agx_pbe_buffer_software_packed software;
   agx_pack(&software, PBE_BUFFER_SOFTWARE, cfg) {
      cfg.base = base;
   }

   agx_pack(&view->pbe_desc, PBE, cfg) {
      cfg.dimension = AGX_TEXTURE_DIMENSION_2D;
      cfg.layout = AGX_LAYOUT_LINEAR;
      cfg.level = 0;
      cfg.layers = 1;
      cfg.levels = 1;

      cfg.type = format.type;
      cfg.channels = format.channels;
      cfg.swizzle_r = agx_channel_from_pipe(desc->swizzle[0]) & 3;
      cfg.swizzle_g = agx_channel_from_pipe(desc->swizzle[1]) & 3;
      cfg.swizzle_b = agx_channel_from_pipe(desc->swizzle[2]) & 3;
      cfg.swizzle_a = agx_channel_from_pipe(desc->swizzle[3]) & 3;
      cfg.srgb = vk_format_is_srgb(view->vk.format);

      /* Use a 2D texture to increase the maximum size */
      cfg.width = 1024; /* XXX: common #define */
      cfg.height = DIV_ROUND_UP(view->vk.elements, cfg.width);
      cfg.stride = (cfg.width * util_format_get_blocksize(p_format)) - 16;

      cfg.buffer = base;

      /* Software-defined PBE buffer descriptor is compact, copy it right in */
      memcpy(&cfg.software_defined, &software, sizeof(cfg.software_defined));
   }
}

static void
agxv_pack_pbe(struct agxv_device *device, struct agxv_image_view *view)
{
   struct agxv_image *image = (struct agxv_image *)view->vk.image;

   enum pipe_format p_format = vk_format_to_pipe_format(view->vk.format);
   struct agx_pixel_format_entry format = agx_pixel_format[p_format];
   const struct util_format_description *desc =
      util_format_description(p_format);

   bool msaa = view->vk.image->samples > 1;

   agx_pack(&view->pbe_desc, PBE, cfg) {
      cfg.dimension = translate_view_type(view->vk.view_type, msaa);
      cfg.layout = agx_translate_layout(image->layout.tiling);
      cfg.type = format.type;
      cfg.channels = format.channels;

      cfg.swizzle_r = agx_channel_from_pipe(desc->swizzle[0]) & 3;
      cfg.swizzle_g = agx_channel_from_pipe(desc->swizzle[1]) & 3;
      cfg.swizzle_b = agx_channel_from_pipe(desc->swizzle[2]) & 3;
      cfg.swizzle_a = agx_channel_from_pipe(desc->swizzle[3]) & 3;
      cfg.srgb = vk_format_is_srgb(view->vk.format);

      cfg.width = view->image->layout.width_px;
      cfg.height = view->image->layout.height_px;
      cfg.level = view->vk.base_mip_level;
      cfg.buffer =
         agxv_image_addr(view->image, 0) +
         ail_get_layer_offset_B(&image->layout, view->vk.base_array_layer);
      cfg.unk_mipmapped = view->image->vk.mip_levels > 1;

      if (view->vk.image->samples == VK_SAMPLE_COUNT_4_BIT)
         cfg.samples = AGX_SAMPLE_COUNT_4;
      else
         cfg.samples = AGX_SAMPLE_COUNT_2;

      if (view->vk.image->tiling == VK_IMAGE_TILING_LINEAR) {
         cfg.levels = 1;
         cfg.stride = ail_get_linear_stride_B(&view->image->layout, 0) - 0x10;
      } else {
         cfg.levels = view->vk.base_mip_level + view->vk.level_count;
         cfg.page_aligned_layers = view->image->layout.page_aligned_layers;
      }
   }
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateBufferView(VkDevice _device,
                      const VkBufferViewCreateInfo *pCreateInfo,
                      const VkAllocationCallbacks *pAllocator,
                      VkBufferView *pView)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   struct agxv_buffer_view *view;

   view = vk_buffer_view_create(&device->vk, pCreateInfo, pAllocator,
                                sizeof(*view));
   if (!view)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   agxv_pack_tex_buffer(device, view);
   agxv_pack_pbe_buffer(device, view);

   *pView = agxv_buffer_view_to_handle(view);

   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_DestroyBufferView(VkDevice _device, VkBufferView bufferView,
                       const VkAllocationCallbacks *pAllocator)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_buffer_view, view, bufferView);

   if (!view)
      return;

   vk_buffer_view_destroy(&device->vk, pAllocator, &view->vk);
   /* XXX: leak? */
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateImageView(VkDevice _device, const VkImageViewCreateInfo *pCreateInfo,
                     const VkAllocationCallbacks *pAllocator,
                     VkImageView *pView)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_image, image, pCreateInfo->image);
   struct agxv_image_view *view;

   view = vk_image_view_create(&device->vk, true /* XXX: HUGE HACK */,
                               pCreateInfo, pAllocator, sizeof(*view));

   if (!view)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   view->image = image;

   agxv_pack_tex(device, view);
   agxv_pack_pbe(device, view);

   *pView = vk_image_view_to_handle(&view->vk);

   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_DestroyImageView(VkDevice _device, VkImageView imageView,
                      const VkAllocationCallbacks *pAllocator)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_image_view, view, imageView);

   if (!view)
      return;

   vk_free2(&device->vk.alloc, pAllocator, view);
}
