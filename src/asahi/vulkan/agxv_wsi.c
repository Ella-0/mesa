/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_wsi.h"
#include "agxv_instance.h"
#include "wsi_common.h"

static VKAPI_ATTR PFN_vkVoidFunction VKAPI_CALL
agxv_wsi_proc_addr(VkPhysicalDevice physicalDevice, const char *pName)
{
   VK_FROM_HANDLE(agxv_physical_device, pdev, physicalDevice);
   return vk_instance_get_proc_addr_unchecked(&pdev->instance->vk, pName);
}

VkResult
agxv_init_wsi(struct agxv_physical_device *pdev)
{
   VkResult result;

   result =
      wsi_device_init(&pdev->wsi_device, agxv_physical_device_to_handle(pdev),
                      agxv_wsi_proc_addr, &pdev->instance->vk.alloc, -1, NULL,
                      &(struct wsi_device_options){.sw_device = false});
   if (result != VK_SUCCESS)
      return result;

   /* enable when we support modifiers */
   pdev->wsi_device.supports_scanout = false;

   pdev->vk.wsi_device = &pdev->wsi_device;

   return result;
}

void
agxv_finish_wsi(struct agxv_physical_device *pdev)
{
   pdev->vk.wsi_device = NULL;
   wsi_device_finish(&pdev->wsi_device, &pdev->instance->vk.alloc);
}
