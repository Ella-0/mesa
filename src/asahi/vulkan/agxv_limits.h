/*
 * Copyright 2023 Valve Corporation
 * SPDX-License-Identifier: MIT
 */

#ifndef AGXV_LIMITS
#define AGXV_LIMITS

/* Common alias */
#define MAX_RTS 8

#define AGXV_MAX_RENDER_TARGETS MAX_RTS
#define AGXV_MAX_VERTEX_ATTRIBS 32
#define AGXV_MAX_VBS            32
#define AGXV_MAX_PUSH           256
#define AGXV_MAX_SETS           32
#define AGXV_MAX_SAMPLERS       8
#define AGXV_MAX_TEXTURES       8

#define AGXV_MAX_DYNAMIC_UNIFORM_BUFFERS 12
#define AGXV_MAX_DYNAMIC_STORAGE_BUFFERS 4
#define AGXV_MAX_UNIFORM_BUFFERS         12
#define AGXV_MAX_STORAGE_BUFFERS         4
#define AGXV_MAX_SAMPLED_IMAGES          16
#define AGXV_MAX_STORAGE_IMAGES          4

#define AGXV_MAX_DESCRIPTOR_SIZE     64
#define AGXV_MAX_PUSH_DESCRIPTORS    32
#define AGXV_MAX_DESCRIPTOR_SET_SIZE (1u << 30)
#define AGXV_PUSH_DESCRIPTOR_SET_SIZE                                          \
   (AGXV_MAX_PUSH_DESCRIPTORS * AGXV_MAX_DESCRIPTOR_SIZE)

#endif
