/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#ifndef AGXV_GRAPHICS_PIPELINE
#define AGXV_GRAPHICS_PIPELINE 1

#include "agxv_pipeline.h"
#include "agxv_private.h"

#include "vk_graphics_state.h"

struct agxv_graphics_pipeline {
   struct agxv_pipeline base;

   struct vk_dynamic_graphics_state dynamic;

   /* Mask of locations that are flat-shaded or linear (nonperpsective) correct
    * respectively. Set when compiling the fragment shader, read when compiling
    * the vertex shader.
    */
   uint64_t varyings_flat_shaded;
   uint64_t varyings_linear_shaded;

   /* TODO: do we need multiple of these if something stages are different
    * from vertex + fragment?
    */
   struct agx_bo *cf_bindings;
};

VK_DEFINE_NONDISP_HANDLE_CASTS(agxv_graphics_pipeline, base.base, VkPipeline,
                               VK_OBJECT_TYPE_PIPELINE);

#endif
