/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#ifndef AGXV_INSTANCE
#define AGXV_INSTANCE 1

#include "agxv_private.h"

#include "vulkan/runtime/vk_instance.h"

struct agxv_instance {
   struct vk_instance vk;
};

VK_DEFINE_HANDLE_CASTS(agxv_instance, vk.base, VkInstance,
                       VK_OBJECT_TYPE_INSTANCE)

#endif
