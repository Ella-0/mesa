/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#ifndef AGXV_COMPUTE_PIPELINE
#define AGXV_COMPUTE_PIPELINE 1

#include "agxv_pipeline.h"

struct agxv_compute_pipeline {
   struct agxv_pipeline base;
   uint32_t local_size[3];
};

static inline struct agxv_compute_pipeline *
agxv_as_compute_pipeline(struct agxv_pipeline *pipeline)
{
   assert(pipeline->type == AGXV_PIPELINE_COMPUTE);
   return (struct agxv_compute_pipeline *)pipeline;
}

VK_DEFINE_NONDISP_HANDLE_CASTS(agxv_compute_pipeline, base.base, VkPipeline,
                               VK_OBJECT_TYPE_PIPELINE);

#endif
