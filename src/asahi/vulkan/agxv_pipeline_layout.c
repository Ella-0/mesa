/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_pipeline_layout.h"

#include "agxv_descriptor_set_layout.h"
#include "agxv_device.h"

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreatePipelineLayout(VkDevice _device,
                          const VkPipelineLayoutCreateInfo *pCreateInfo,
                          UNUSED const VkAllocationCallbacks *pAllocator,
                          VkPipelineLayout *pPipelineLayout)
{
   VK_FROM_HANDLE(agxv_device, device, _device);

   struct agxv_pipeline_layout *layout = vk_pipeline_layout_zalloc(
      &device->vk, sizeof(struct agxv_pipeline_layout), pCreateInfo);
   if (layout == NULL)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   for (uint32_t i = 0; i < pCreateInfo->pushConstantRangeCount; i++) {
      const VkPushConstantRange *range = &pCreateInfo->pPushConstantRanges[i];
      layout->push_size = MAX2(layout->push_size, range->offset + range->size);
   }

   for (uint32_t i = 0; i < layout->vk.set_count; i++) {
      struct vk_descriptor_set_layout *vk_set_layout =
         layout->vk.set_layouts[i];
      struct agxv_descriptor_set_layout *set_layout =
         container_of(vk_set_layout, struct agxv_descriptor_set_layout, vk);

      layout->texture_map[i] = layout->texture_count;
      layout->sampler_map[i] = layout->sampler_count;

      layout->texture_count += set_layout->texture_count;
      layout->sampler_count += set_layout->sampler_count;
   }

   *pPipelineLayout = agxv_pipeline_layout_to_handle(layout);

   return VK_SUCCESS;
}
