/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_private.h"

#include "asahi/layout/layout.h"
#include "asahi/lib/agx_formats.h"
#include "asahi/lib/agx_nir_lower_vbo.h"
#include "asahi/lib/agx_pack.h"

#include "vulkan/util/vk_enum_defines.h"
#include "vulkan/util/vk_format.h"
#include "vulkan/vulkan_core.h"

static bool
agxv_is_valid_pixel_format(enum pipe_format format)
{
   switch (format) {
   /* TODO: These formats are emulated for texture read only and don't support
    * write */
   case PIPE_FORMAT_R32G32B32_FLOAT:
   case PIPE_FORMAT_R32G32B32_UINT:
   case PIPE_FORMAT_R32G32B32_SINT:
      return false;
   default:
      return agx_is_valid_pixel_format(format);
   }
}

static VkFormatFeatureFlags2
agxv_format_feature_flags(enum pipe_format format, bool linear)
{
   VkFormatFeatureFlags2 flags = 0;

   if (!agxv_is_valid_pixel_format(format))
      goto done;

   flags |= VK_FORMAT_FEATURE_2_SAMPLED_IMAGE_BIT;
   flags |= VK_FORMAT_FEATURE_2_TRANSFER_SRC_BIT;
   flags |= VK_FORMAT_FEATURE_2_TRANSFER_DST_BIT;
   flags |= VK_FORMAT_FEATURE_2_BLIT_SRC_BIT;
   flags |= VK_FORMAT_FEATURE_2_BLIT_DST_BIT;

   if (agx_pixel_format[format].renderable) {
      flags |= VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BIT |
               VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BLEND_BIT;
   }

   bool is_ds = util_format_is_depth_or_stencil(format);

   /* Not all hardware supports linear ds */
   if (is_ds && !linear)
      flags |= VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT;

done:
   return flags;
}

static VkImageUsageFlags
agxv_image_usage_flags(enum pipe_format format, bool linear)
{
   VkImageUsageFlags flags = 0;

   if (!agxv_is_valid_pixel_format(format))
      goto done;

   flags |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
   flags |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
   flags |= VK_IMAGE_USAGE_SAMPLED_BIT;

   bool is_ds = util_format_is_depth_or_stencil(format);

   /* Not all hardware supports linear ds */
   if (is_ds && !linear) {
      flags |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
   } else if (!is_ds) {
      flags |= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
   }
done:
   return flags;
}

VKAPI_ATTR void VKAPI_CALL
agxv_GetPhysicalDeviceFormatProperties2(VkPhysicalDevice physicalDevice,
                                        VkFormat vk_format,
                                        VkFormatProperties2 *pFormatProperties)
{
   VkFormatProperties props = {};

   enum pipe_format format = vk_format_to_pipe_format(vk_format);

   props.linearTilingFeatures |=
      vk_format_features2_to_features(agxv_format_feature_flags(format, true));

   props.optimalTilingFeatures |=
      vk_format_features2_to_features(agxv_format_feature_flags(format, false));

   if (agx_vbo_supports_format(format)) {
      props.bufferFeatures |= VK_FORMAT_FEATURE_VERTEX_BUFFER_BIT;
   }

   vk_foreach_struct(ext, pFormatProperties->pNext) {
      /* Use unsigned since some cases are not in the VkStructureType enum. */
      switch ((unsigned)ext->sType) {
      default:
         agxv_debug_ignored_stype(ext->sType);
         break;
      }
   }

   pFormatProperties->formatProperties = props;
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_GetPhysicalDeviceImageFormatProperties2(
   VkPhysicalDevice physicalDevice,
   const VkPhysicalDeviceImageFormatInfo2 *base_info,
   VkImageFormatProperties2 *base_props)
{
   bool linear = base_info->tiling == VK_IMAGE_TILING_LINEAR;
   enum pipe_format format = vk_format_to_pipe_format(base_info->format);
   VkImageUsageFlags usage = agxv_image_usage_flags(format, linear);

   if ((usage & base_info->usage) != base_info->usage)
      return VK_ERROR_FORMAT_NOT_SUPPORTED;

   base_props->imageFormatProperties.maxExtent.width = 4096;  // TODO
   base_props->imageFormatProperties.maxExtent.height = 4096; // TODO
   base_props->imageFormatProperties.maxMipLevels = AIL_MAX_MIP_LEVELS;
   base_props->imageFormatProperties.maxArrayLayers = 1; // TODO
   base_props->imageFormatProperties.sampleCounts =
      VK_SAMPLE_COUNT_1_BIT;                                            // TODO
   base_props->imageFormatProperties.maxResourceSize = 4096 * 4096 * 8; // TODO

   vk_foreach_struct(ext, base_props->pNext) {
      /* Use unsigned since some cases are not in the VkStructureType enum. */
      switch ((unsigned)ext->sType) {
      default:
         agxv_debug_ignored_stype(ext->sType);
         break;
      }
   }

   return VK_SUCCESS;
}
