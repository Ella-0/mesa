/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#ifndef AGXV_DESCRIPTOR_POOL
#define AGXV_DESCRIPTOR_POOL 1

#include "agxv_private.h"

struct agxv_descriptor_pool_entry {
   uint64_t offset;
   uint64_t size;
   struct agxv_descriptor_set *set;
};

struct agxv_descriptor_pool {
   struct vk_object_base base;
   struct agx_bo *bo;
   uint64_t size;
   uint64_t current_offset;

   uint32_t entry_count;
   uint32_t max_entry_count;
   struct agxv_descriptor_pool_entry entries[];
};

VK_DEFINE_NONDISP_HANDLE_CASTS(agxv_descriptor_pool, base, VkDescriptorPool,
                               VK_OBJECT_TYPE_DESCRIPTOR_POOL)

#endif
