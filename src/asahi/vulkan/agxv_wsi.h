/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#ifndef AGXV_WSI
#define AGXV_WSI 1

#include "agxv_physical_device.h"

VkResult agxv_init_wsi(struct agxv_physical_device *pdev);
void agxv_finish_wsi(struct agxv_physical_device *pdev);

#endif
