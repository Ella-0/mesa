/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_cmd_buffer.h"

#include "agxv_buffer.h"
#include "agxv_compute_pipeline.h"
#include "agxv_descriptor_set.h"
#include "agxv_device.h"
#include "agxv_graphics_pipeline.h"
#include "agxv_image.h"
#include "agxv_image_view.h"
#include "agxv_physical_device.h"

#include "asahi/compiler/agx_compile.h"
#include "asahi/lib/agx_meta.h"
#include "asahi/lib/agx_ppp.h"
#include "asahi/lib/agx_usc.h"
#include "asahi/lib/decode.h"
#include "compiler/shader_enums.h"

#include "pool.h"
#include "vk_command_pool.h"
#include "vk_format.h"
#include "vk_meta.h"
#include "vk_pipeline_layout.h"

extern const struct vk_command_buffer_ops agxv_cmd_buffer_ops;

static VkResult
agxv_cmd_buffer_create(struct vk_command_pool *pool,
                       struct vk_command_buffer **vk_cmd_buffer)
{
   VkResult result;

   struct agxv_cmd_buffer *cmd_buffer;
   cmd_buffer = vk_zalloc(&pool->alloc, sizeof(*cmd_buffer), 8,
                          VK_SYSTEM_ALLOCATION_SCOPE_OBJECT);

   if (!cmd_buffer)
      return vk_error(pool->base.device, VK_ERROR_OUT_OF_HOST_MEMORY);

   result =
      vk_command_buffer_init(pool, &cmd_buffer->vk, &agxv_cmd_buffer_ops, 0);

   if (result != VK_SUCCESS) {
      vk_free(&pool->alloc, cmd_buffer);
      return result;
   }

   list_inithead(&cmd_buffer->jobs);

   cmd_buffer->device = container_of(pool->base.device, struct agxv_device, vk);

   struct vk_dynamic_graphics_state *state =
      &cmd_buffer->vk.dynamic_graphics_state;
   vk_dynamic_graphics_state_dirty_all(state);
   *vk_cmd_buffer = &cmd_buffer->vk;

   return VK_SUCCESS;
}

static void
agxv_cmd_buffer_reset(struct vk_command_buffer *vk_cmd_buffer,
                      VkCommandBufferResetFlags flags)
{
   struct agxv_cmd_buffer *cmd_buffer =
      container_of(vk_cmd_buffer, struct agxv_cmd_buffer, vk);

   agx_pool_cleanup(&cmd_buffer->cmd_pool);
   agx_pool_cleanup(&cmd_buffer->pipeline_pool);
   agx_pool_cleanup(&cmd_buffer->shader_pool);

   list_inithead(&cmd_buffer->jobs);

   vk_command_buffer_reset(vk_cmd_buffer);
}

static void
agxv_cmd_buffer_destroy(struct vk_command_buffer *vk_cmd_buffer)
{
   agxv_cmd_buffer_reset(vk_cmd_buffer, 0);
   /* TODO: agxv_cmd_buffer::pool::alloc */
   vk_command_buffer_finish(vk_cmd_buffer);
   vk_free(&vk_cmd_buffer->pool->alloc, vk_cmd_buffer);
}

const struct vk_command_buffer_ops agxv_cmd_buffer_ops = {
   .create = agxv_cmd_buffer_create,
   .reset = agxv_cmd_buffer_reset,
   .destroy = agxv_cmd_buffer_destroy,
};

struct agxv_job *
agxv_job_create(struct agxv_cmd_buffer *cmd_buffer, enum agxv_job_type type,
                uint32_t attachment_count)
{
   struct agxv_job *job;
   struct drm_asahi_attachment *attachments;
   VK_MULTIALLOC(ma);
   vk_multialloc_add(&ma, &job, struct agxv_job, 1);
   vk_multialloc_add(&ma, &attachments, struct drm_asahi_attachment,
                     attachment_count);
   if (!vk_multialloc_zalloc(&ma, &cmd_buffer->device->vk.alloc,
                             VK_SYSTEM_ALLOCATION_SCOPE_COMMAND))
      return NULL;
   if (attachment_count)
      assert(attachments == job->attachments);

   job->type = type;

   list_addtail(&job->link, &cmd_buffer->jobs);

   job->barrier = cmd_buffer->state.barrier;
   cmd_buffer->state.barrier.compute = false;
   cmd_buffer->state.barrier.render = false;

   /* Allocate the command buffer a 64k at a time; When we run out of
    * space, we insert a stream link.
    */
   cmd_buffer->state.encoder =
      agx_pool_alloc_aligned(&cmd_buffer->cmd_pool, 0x10000, 0x4000);
   cmd_buffer->state.encoder_current = cmd_buffer->state.encoder.cpu;

   if (type == AGXV_JOB_GRAPHICS) {
      job->render_buf.flags = 0;
      job->render_buf.flags |= ASAHI_RENDER_PROCESS_EMPTY_TILES;
      // job->render_buf.flags |= ASAHI_RENDER_NO_CLEAR_PIPELINE_TEXTURES;
      // job->render_buf.flags |= ASAHI_RENDER_SET_WHEN_RELOADING_Z_OR_S;
      /* FIXME: do we need to do this on LOAD_OP_NONE or LOAD_OP_DONT_CARE? */
      job->render_buf.encoder_ptr = cmd_buffer->state.encoder.gpu;
      job->render_buf.encoder_id =
         agx_get_global_id(&cmd_buffer->device->pdev->dev);
      job->render_buf.cmd_3d_id =
         agx_get_global_id(&cmd_buffer->device->pdev->dev);
      job->render_buf.cmd_ta_id =
         agx_get_global_id(&cmd_buffer->device->pdev->dev);

      job->render_buf.ppp_ctrl = 0x202;
      job->render_buf.ppp_multisamplectl = 0x88;
      agx_pack(&job->render_buf.zls_ctrl, ZLS_CONTROL, cfg) {
         cfg.z_load_enable = false;
         cfg.z_store_enable = false;
         cfg.s_load_enable = false;
         cfg.s_store_enable = false;
      };

      job->render_buf.samples = 1;
      job->render_buf.layers = 1;

      job->render_buf.fb_width = 0;
      job->render_buf.fb_height = 0;

      job->render_buf.fragment_attachment_count = attachment_count;
      job->render_buf.fragment_attachments = (uintptr_t)job->attachments;
   } else {
      assert(type == AGXV_JOB_COMPUTE);
      job->compute_buf.encoder_ptr = cmd_buffer->state.encoder.gpu;
      job->compute_buf.encoder_id =
         agx_get_global_id(&cmd_buffer->device->pdev->dev);
      job->compute_buf.iogpu_unk_40 = 0x1c;
      job->compute_buf.cmd_id =
         agx_get_global_id(&cmd_buffer->device->pdev->dev);
      job->compute_buf.iogpu_unk_44 = 0xffffffff;
   }

   return job;
}

static void
agxv_job_destroy(struct agxv_job *job)
{
   switch (job->type) {
   case AGXV_JOB_GRAPHICS:
      break;
   default:
      unreachable("unhandled job type");
   }
   vk_free(NULL, job);
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_BeginCommandBuffer(VkCommandBuffer commandBuffer,
                        const VkCommandBufferBeginInfo *pBeginInfo)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   agxv_cmd_buffer_reset(&cmd_buffer->vk, 0);

   agx_pool_init(&cmd_buffer->cmd_pool, &cmd_buffer->device->pdev->dev,
                 AGX_BO_WRITEBACK, false);

   agx_pool_init(&cmd_buffer->pipeline_pool, &cmd_buffer->device->pdev->dev,
                 AGX_BO_LOW_VA, false);

   agx_pool_init(&cmd_buffer->shader_pool, &cmd_buffer->device->pdev->dev,
                 AGX_BO_EXEC | AGX_BO_LOW_VA, false);

   return VK_SUCCESS;
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_EndCommandBuffer(VkCommandBuffer commandBuffer)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   return vk_command_buffer_get_record_result(&cmd_buffer->vk);
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdBindPipeline(VkCommandBuffer commandBuffer,
                     VkPipelineBindPoint pipelineBindPoint,
                     VkPipeline _pipeline)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   /* TODO: support compute pipelines */
   switch (pipelineBindPoint) {
   case VK_PIPELINE_BIND_POINT_GRAPHICS: {
      VK_FROM_HANDLE(agxv_graphics_pipeline, pipeline, _pipeline);
      vk_cmd_set_dynamic_graphics_state(&cmd_buffer->vk, &pipeline->dynamic);

      cmd_buffer->state.pipeline = &pipeline->base;
      cmd_buffer->state.dirty.vs = true;
      cmd_buffer->state.dirty.fs = true;
      cmd_buffer->state.dirty.pipeline = true;

      break;
   }
   case VK_PIPELINE_BIND_POINT_COMPUTE: {
      VK_FROM_HANDLE(agxv_compute_pipeline, pipeline, _pipeline);
      cmd_buffer->state.pipeline = &pipeline->base;
      cmd_buffer->state.dirty.cs = true;
      cmd_buffer->state.dirty.pipeline = true;
      break;
   }
   default:
      unreachable("Not implemented");
   }
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdBindDescriptorSets(VkCommandBuffer commandBuffer,
                           VkPipelineBindPoint pipelineBindPoint,
                           VkPipelineLayout _layout, uint32_t firstSet,
                           uint32_t descriptorSetCount,
                           const VkDescriptorSet *pDescriptorSets,
                           uint32_t dynamicOffsetCount,
                           const uint32_t *pDynamicOffsets)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   VK_FROM_HANDLE(agxv_pipeline_layout, layout, _layout);
   cmd_buffer->state.descriptors.sets_base = layout->push_size ? 4 : 0;

   if (descriptorSetCount) {
      cmd_buffer->state.dirty.sets = true;

      /* FIXME: check if shader is actually needed by each stage */
      cmd_buffer->state.dirty.vs = true;
      cmd_buffer->state.dirty.fs = true;
   }

   for (uint32_t i = 0; i < descriptorSetCount; i++) {
      VK_FROM_HANDLE(agxv_descriptor_set, set, pDescriptorSets[i]);
      cmd_buffer->state.descriptors.sets[firstSet + i] = set;
      cmd_buffer->state.descriptors.set_vas[firstSet + i] =
         set->bo->ptr.gpu + set->bo_offset;
   }

   cmd_buffer->state.descriptors.ts_count = layout->texture_count;
   cmd_buffer->state.descriptors.ss_count = layout->sampler_count;
}

static struct agxv_push_descriptor_set *
agxv_cmd_push_descriptors(struct agxv_cmd_buffer *cmd,
                          VkPipelineBindPoint bind_point, uint32_t set)
{
   struct agxv_descriptor_state *desc =
      agxv_get_descriptors_state(cmd, bind_point);

   assert(set < AGXV_MAX_SETS);
   if (unlikely(!desc->push[set])) {
      desc->push[set] =
         vk_zalloc(&cmd->vk.pool->alloc, sizeof(*desc->push[set]), 8,
                   VK_SYSTEM_ALLOCATION_SCOPE_OBJECT);
      if (unlikely(desc->push[set] == NULL)) {
         vk_command_buffer_set_error(&cmd->vk, VK_ERROR_OUT_OF_HOST_MEMORY);
         return NULL;
      }
   }

   /* Pushing descriptors replaces whatever sets are bound */
   desc->sets[set] = NULL;
   desc->push_dirty |= BITFIELD_BIT(set);

   return desc->push[set];
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdPushDescriptorSetKHR(VkCommandBuffer commandBuffer,
                             VkPipelineBindPoint pipelineBindPoint,
                             VkPipelineLayout layout_, uint32_t set,
                             uint32_t descriptorWriteCount,
                             const VkWriteDescriptorSet *pDescriptorWrites)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd, commandBuffer);
   VK_FROM_HANDLE(agxv_pipeline_layout, layout, layout_);

   struct agxv_push_descriptor_set *push_set =
      agxv_cmd_push_descriptors(cmd, pipelineBindPoint, set);
   if (unlikely(push_set == NULL))
      return;

   struct agxv_descriptor_set_layout *set_layout =
      vk_to_agxv_descriptor_set_layout(layout->vk.set_layouts[set]);

   agxv_push_descriptor_set_update(push_set, set_layout, descriptorWriteCount,
                                   pDescriptorWrites);

   cmd->state.descriptors.sets_base = layout->push_size ? 4 : 0;
   cmd->state.descriptors.ts_count = layout->texture_count;
   cmd->state.descriptors.ss_count = layout->sampler_count;

   cmd->state.descriptors.pushed_layouts[set] = set_layout;
   cmd->state.descriptors.pushed_textures[set] = agx_pool_upload_aligned(
      &cmd->cmd_pool, push_set->textures, sizeof(push_set->textures), 64);
   cmd->state.descriptors.pushed_samplers[set] = agx_pool_upload_aligned(
      &cmd->cmd_pool, push_set->samplers, sizeof(push_set->samplers), 64);

   cmd->state.descriptors.set_vas[set] = agx_pool_upload_aligned(
      &cmd->cmd_pool, push_set->data, sizeof(push_set->data), 64);

   /* XXX */
   cmd->state.dirty.sets = true;

   /* FIXME: check if shader is actually needed by each stage */
   cmd->state.dirty.vs = true;
   cmd->state.dirty.fs = true;
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdPushConstants(VkCommandBuffer commandBuffer, VkPipelineLayout _layout,
                      VkShaderStageFlags stageFlags, uint32_t offset,
                      uint32_t size, const void *pValues)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);

   if (!size)
      return;

   cmd_buffer->state.descriptors.sets_base = 4;

   assert(offset + size <= AGXV_MAX_PUSH);
   memcpy(&cmd_buffer->state.push[offset], pValues, size);
   cmd_buffer->state.dirty.push = true;
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdPipelineBarrier2(VkCommandBuffer commandBuffer,
                         const VkDependencyInfo *pDependencyInfo)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   cmd_buffer->state.barrier.compute = true;
   cmd_buffer->state.barrier.render = true;
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdCopyBuffer2(VkCommandBuffer commandBuffer,
                    const VkCopyBufferInfo2 *info)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);

   vk_meta_copy_buffer2(&cmd_buffer->vk, &cmd_buffer->device->meta, info);
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdCopyImage2(VkCommandBuffer commandBuffer,
                   const VkCopyImageInfo2 *pCopyImageInfo)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   VK_FROM_HANDLE(agxv_image, src, pCopyImageInfo->srcImage);
   VK_FROM_HANDLE(agxv_image, dst, pCopyImageInfo->dstImage);

   vk_meta_copy_image(&cmd_buffer->vk, &cmd_buffer->device->meta, &src->vk,
                      src->vk.format, pCopyImageInfo->srcImageLayout, &dst->vk,
                      dst->vk.format, pCopyImageInfo->dstImageLayout,
                      pCopyImageInfo->regionCount, pCopyImageInfo->pRegions);
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdCopyBufferToImage2(VkCommandBuffer commandBuffer,
                           const VkCopyBufferToImageInfo2 *pCopyBufferInfo)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);

   vk_meta_copy_buffer_to_image2(&cmd_buffer->vk, &cmd_buffer->device->meta,
                                 pCopyBufferInfo);
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdCopyImageToBuffer2(VkCommandBuffer commandBuffer,
                           const VkCopyImageToBufferInfo2 *pCopyBufferInfo)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd, commandBuffer);

   vk_meta_copy_image_to_buffer2(&cmd->vk, &cmd->device->meta, pCopyBufferInfo);
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdFillBuffer(VkCommandBuffer commandBuffer, VkBuffer dstBuffer,
                   VkDeviceSize dstOffset, VkDeviceSize dstRange, uint32_t data)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   VK_FROM_HANDLE(vk_buffer, buffer, dstBuffer);

   vk_meta_fill_buffer(&cmd_buffer->vk, &cmd_buffer->device->meta, buffer,
                       dstOffset, dstRange, data);
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdUpdateBuffer(VkCommandBuffer commandBuffer, VkBuffer dstBuffer,
                     VkDeviceSize dstOffset, VkDeviceSize dataSize,
                     const void *pData)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   VK_FROM_HANDLE(vk_buffer, buffer, dstBuffer);

   vk_meta_update_buffer(&cmd_buffer->vk, &cmd_buffer->device->meta, buffer,
                         dstOffset, dataSize, pData);
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdClearColorImage(VkCommandBuffer commandBuffer, VkImage _image,
                        VkImageLayout imageLayout,
                        const VkClearColorValue *pColor, uint32_t rangeCount,
                        const VkImageSubresourceRange *pRanges)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   VK_FROM_HANDLE(vk_image, image, _image);
   vk_meta_clear_color_image(&cmd_buffer->vk, &cmd_buffer->device->meta, image,
                             imageLayout, image->format, pColor, rangeCount,
                             pRanges);
}

static void
agxv_meta_init_render(struct agxv_cmd_buffer *cmd,
                      struct vk_meta_rendering_info *info)
{
   const struct agxv_rendering_state *render = &cmd->state.render;

   *info = (struct vk_meta_rendering_info){
      .view_mask = render->view_mask,
      .samples = 1 /* TODO: MSAA */,
      .color_attachment_count = render->color_att_count,
      .depth_attachment_format = render->depth_att.vk_format,
      .stencil_attachment_format = render->stencil_att.vk_format,
   };
   for (uint32_t a = 0; a < render->color_att_count; a++)
      info->color_attachment_formats[a] = render->color_att[a].vk_format;
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdClearAttachments(VkCommandBuffer commandBuffer,
                         uint32_t attachmentCount,
                         const VkClearAttachment *pAttachments,
                         uint32_t rectCount, const VkClearRect *pRects)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd, commandBuffer);

   struct vk_meta_rendering_info render;
   agxv_meta_init_render(cmd, &render);

   vk_meta_clear_attachments(&cmd->vk, &cmd->device->meta, &render,
                             attachmentCount, pAttachments, rectCount, pRects);
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdClearDepthStencilImage(VkCommandBuffer commandBuffer, VkImage image,
                               VkImageLayout imageLayout,
                               const VkClearDepthStencilValue *pDepthStencil,
                               uint32_t rangeCount,
                               const VkImageSubresourceRange *pRanges)
{
   unreachable("stub");
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdBeginQuery(VkCommandBuffer commandBuffer, VkQueryPool queryPool,
                   uint32_t query, VkQueryControlFlags flags)
{
   unreachable("stub");
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdEndQuery(VkCommandBuffer commandBuffer, VkQueryPool queryPool,
                 uint32_t query)
{
   unreachable("stub");
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdResetQueryPool(VkCommandBuffer commandBuffer, VkQueryPool queryPool,
                       uint32_t firstQuery, uint32_t queryCount)
{
   unreachable("stub");
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdCopyQueryPoolResults(VkCommandBuffer commandBuffer,
                             VkQueryPool queryPool, uint32_t firstQuery,
                             uint32_t queryCount, VkBuffer dstBuffer,
                             VkDeviceSize dstOffset, VkDeviceSize stride,
                             VkQueryResultFlags flags)
{
   unreachable("stub");
}
