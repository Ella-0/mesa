/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#ifndef AGXV_SAMPLER
#define AGXV_SAMPLER 1

#include "asahi/lib/agx_pack.h"
#include "agxv_private.h"

struct agxv_sampler {
   struct vk_object_base base;
   struct agx_sampler_packed desc;
};

VK_DEFINE_NONDISP_HANDLE_CASTS(agxv_sampler, base, VkSampler,
                               VK_OBJECT_TYPE_SAMPLER)

#endif
