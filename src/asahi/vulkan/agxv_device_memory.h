/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#ifndef AGXV_DEVICE_MEMORY
#define AGXV_DEVICE_MEMORY 1

#include "agxv_private.h"

#include "asahi/lib/agx_bo.h"

struct agxv_device_memory {
   struct vk_object_base base;
   struct agx_bo *bo;
};

VK_DEFINE_HANDLE_CASTS(agxv_device_memory, base, VkDeviceMemory,
                       VK_OBJECT_TYPE_DEVICE_MEMORY)

#endif
