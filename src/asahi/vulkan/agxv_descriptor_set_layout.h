/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#ifndef AGXV_DESCRIPTOR_SET_LAYOUT
#define AGXV_DESCRIPTOR_SET_LAYOUT 1

#include "agxv_private.h"

#include "asahi/lib/agx_pack.h"

#include "vulkan/runtime/vk_descriptor_set_layout.h"

struct agxv_descriptor_set_binding_layout {
   VkDescriptorType type;

   /* Number of array elements in this binding */
   uint32_t array_size;

   /* Offset into the descriptor buffer */
   uint32_t offset;

   /* Number of bytes between each array element */
   uint8_t stride;

   /* optional indices mapping binding to ts and ss registers */
   uint8_t texture_index;
   uint8_t sampler_index;

   /* TODO */
   bool immutable_samplers;
};

#define AGXV_MAX_SAMPLERS 8
#define AGXV_MAX_TEXTURES 8

struct agxv_descriptor_set_layout {
   struct vk_descriptor_set_layout vk;

   /* Total size for this layout */
   uint32_t size;

   uint8_t texture_count;
   uint8_t sampler_count;

   uint8_t texture_index;
   uint8_t sampler_index;

   uint32_t binding_count;
   struct agxv_descriptor_set_binding_layout binding[];
};

VK_DEFINE_HANDLE_CASTS(agxv_descriptor_set_layout, vk.base,
                       VkDescriptorSetLayout,
                       VK_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT);

static struct agxv_descriptor_set_layout *
vk_to_agxv_descriptor_set_layout(struct vk_descriptor_set_layout *layout)
{
   return (struct agxv_descriptor_set_layout *)layout;
}

#endif
