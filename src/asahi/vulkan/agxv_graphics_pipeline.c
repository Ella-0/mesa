/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_graphics_pipeline.h"

#include "agx_tilebuffer.h"
#include "agxv_device.h"
#include "agxv_physical_device.h"

#include "asahi/compiler/agx_compile.h"
#include "asahi/lib/agx_nir_lower_vbo.h"
#include "compiler/nir/nir_builder.h"
#include "compiler/shader_enums.h"
#include "compiler/spirv/nir_spirv.h"
#include "util/format/u_format.h"
#include "nir_lower_blend.h"
#include "vk_blend.h"
#include "vk_format.h"
#include "vk_graphics_state.h"
#include "vk_pipeline_layout.h"
#include "vk_shader_module.h"

static void
link_varyings(struct agxv_device *device,
              struct agxv_graphics_pipeline *pipeline)
{
   struct agx_varyings_vs *vs =
      &pipeline->base.stages[MESA_SHADER_VERTEX].info.varyings.vs;
   struct agx_varyings_fs *fs =
      &pipeline->base.stages[MESA_SHADER_FRAGMENT].info.varyings.fs;

   if (fs->nr_bindings == 0)
      return;

   size_t size =
      AGX_CF_BINDING_HEADER_LENGTH + (fs->nr_bindings * AGX_CF_BINDING_LENGTH);

   struct agx_bo *bo =
      agx_bo_create(&device->pdev->dev, size, AGX_BO_LOW_VA, "CF Bindings");
   void *buf = bo->ptr.cpu;

   /* Figure out how many slots are used up for position and point size */
   unsigned base_slot;
   if (vs->slots[VARYING_SLOT_PSIZ] < vs->nr_index)
      base_slot = vs->nr_index - 5;
   else
      base_slot = vs->nr_index - 4;

   unsigned num_slots = base_slot;

   /* W is prepended */
   num_slots++;

   /* Z may be prepended */
   if (fs->reads_z)
      num_slots++;

   agxv_push(buf, CF_BINDING_HEADER, cfg) {
      cfg.number_of_32_bit_slots = num_slots;
      cfg.number_of_coefficient_registers = fs->nr_cf;
   };

   for (unsigned i = 0; i < fs->nr_bindings; i++) {
      agxv_push(buf, CF_BINDING, cfg) {
         cfg.base_coefficient_register = fs->bindings[i].cf_base;
         cfg.components = fs->bindings[i].count;
         cfg.perspective = fs->bindings[i].perspective;
         /* TODO: VK_EXT_provoking_vertex */
         cfg.shade_model = fs->bindings[i].smooth
                              ? AGX_SHADE_MODEL_GOURAUD
                              : AGX_SHADE_MODEL_FLAT_VERTEX_0;

         if (fs->bindings[i].slot == VARYING_SLOT_PNTC) {
            assert(fs->bindings[i].offset == 0);
            cfg.point_sprite = true;
         } else {
            if (fs->bindings[i].slot == VARYING_SLOT_POS) {
               if (fs->bindings[i].offset == 3) {
                  cfg.base_slot = 0;
               } else if (fs->bindings[i].offset == 2) {
                  cfg.base_slot = 1;
               } else {
                  unreachable("invalid offset");
               }
            } else {
               unsigned vs_index = vs->slots[fs->bindings[i].slot];
               assert(vs_index >= 4 &&
                      "gl_Position should have been first 4 slots");

               if (vs_index >= vs->nr_index) {
                  /* Varyings not written by vertex shader are undefined. It
                   * doesn't matter what we read but we can't crash.
                   */
                  cfg.base_slot = 0;
               } else {
                  unsigned vs_u_idx = (vs_index + fs->bindings[i].offset) - 4;

                  if (fs->reads_z)
                     cfg.base_slot = vs_u_idx + 2;
                  else
                     cfg.base_slot = vs_u_idx + 1;
               }
            }
         }

         if (fs->bindings[i].slot == VARYING_SLOT_POS) {
            if (fs->bindings[i].offset == 2) {
               cfg.fragcoord_z = true;
            } else {
               assert(!cfg.perspective && "W must not be perspective divided");
            }
         }

         assert(cfg.base_coefficient_register + cfg.components <= fs->nr_cf &&
                "overflowed coefficent registers");
      }
   }
   pipeline->cf_bindings = bo;
   assert(bo->ptr.gpu < (1ull << 32) && "varyings must be in low memory");
}

static void
agxv_graphics_pipeline_destroy(struct agxv_pipeline *base)
{
   struct agxv_graphics_pipeline *pipeline =
      container_of(base, struct agxv_graphics_pipeline, base);
   if (pipeline->cf_bindings)
      agx_bo_unreference(pipeline->cf_bindings);
}

static void
agxv_lower_vertex(struct agxv_pipeline *pipeline, nir_shader *nir,
                  struct agx_shader_key *key, void *data)
{
   struct agxv_graphics_pipeline *gfx = (void *)pipeline;
   struct vk_graphics_pipeline_state *state = data;
   assert(state);

   pipeline->buffer_base = key->reserved_preamble;
   pipeline->buffer_len = 4 * util_last_bit(state->vi->bindings_valid);
   key->reserved_preamble += pipeline->buffer_len;

   struct agx_vbufs vbuf_key = {
      .count = util_bitcount(state->vi->attributes_valid),
   };

   if (state->vi) {
      u_foreach_bit(i, state->vi->bindings_valid) {
         vbuf_key.strides[i] = state->vi->bindings[i].stride;
      }

      u_foreach_bit(i, state->vi->attributes_valid) {
         uint32_t binding = state->vi->attributes[i].binding;
         uint16_t input_rate = state->vi->bindings[binding].input_rate;

         /* nir_assign_io_var_locations compacts the attributes, so we need to
          * match that compaction here for correct results.
          */
         unsigned k = util_bitcount64(nir->info.inputs_read &
                                      BITFIELD64_MASK(VERT_ATTRIB_GENERIC(i)));

         vbuf_key.attributes[k].divisor =
            input_rate == VK_VERTEX_INPUT_RATE_INSTANCE ? 1 : 0;
         vbuf_key.attributes[k].buf = binding;
         vbuf_key.attributes[k].format =
            vk_format_to_pipe_format(state->vi->attributes[i].format);
         vbuf_key.attributes[k].src_offset = state->vi->attributes[i].offset;
      }
      NIR_PASS_V(nir, agx_nir_lower_vbo, &vbuf_key);
   }

   /* Set the key according to the linked fragment shader */
   key->vs.outputs_flat_shaded = gfx->varyings_flat_shaded;
   key->vs.outputs_linear_shaded = gfx->varyings_linear_shaded;
}

static void
agxv_lower_blend(struct agxv_pipeline *pipeline, nir_shader *nir,
                 const struct vk_color_blend_state *state,
                 enum pipe_format *rt_formats)
{
   nir_lower_blend_options options = {
      .logicop_enable = state->logic_op_enable,
      .logicop_func = vk_logic_op_to_pipe(state->logic_op),
   };

   for (unsigned rt = 0; rt < state->attachment_count; rt++) {
      const struct vk_color_blend_attachment_state *att =
         &state->attachments[rt];

      /* For now, we always apply colour masking with nir_lower_blend */
      options.rt[rt].colormask = att->write_mask;
      options.format[rt] = rt_formats[rt];

      if (!att->blend_enable) {
         static const nir_lower_blend_channel replace = {
            .func = PIPE_BLEND_ADD,
            .src_factor = PIPE_BLENDFACTOR_ONE,
            .dst_factor = PIPE_BLENDFACTOR_ZERO,
         };

         options.rt[rt].rgb = replace;
         options.rt[rt].alpha = replace;
      } else {
         options.rt[rt].rgb.func = vk_blend_op_to_pipe(att->color_blend_op);
         options.rt[rt].rgb.src_factor =
            vk_blend_factor_to_pipe(att->src_color_blend_factor);
         options.rt[rt].rgb.dst_factor =
            vk_blend_factor_to_pipe(att->dst_color_blend_factor);

         options.rt[rt].alpha.func = vk_blend_op_to_pipe(att->color_blend_op);
         options.rt[rt].alpha.src_factor =
            vk_blend_factor_to_pipe(att->src_color_blend_factor);
         options.rt[rt].alpha.dst_factor =
            vk_blend_factor_to_pipe(att->dst_color_blend_factor);
      }
   }

   NIR_PASS_V(nir, nir_lower_blend, &options);
}

static void
agxv_lower_fragment(struct agxv_pipeline *pipeline, nir_shader *nir,
                    struct agx_shader_key *key, void *data)
{
   struct vk_graphics_pipeline_state *state = data;
   enum pipe_format fmts[MAX_RTS] = {};
   uint8_t colour_masks[MAX_RTS] = {};

   uint32_t cb_count = state->rp->color_attachment_count;
   assert(cb_count < ARRAY_SIZE(fmts));

   /* Collect attachment formats */
   for (uint32_t i = 0; i < cb_count; i++) {
      fmts[i] =
         vk_format_to_pipe_format(state->rp->color_attachment_formats[i]);

      /* TODO: Optimize write masks. Currently disabled in the GL driver due to
       * CTS failures with this mechanism.
       */
      colour_masks[i] = 0xff;
   }

   /* Discards must be lowered before MSAA */
   NIR_PASS_V(nir, agx_nir_lower_discard_zs_emit);

   /* Lower blending, including write masks and logic ops */
   if (state->cb) {
      agxv_lower_blend(pipeline, nir, state->cb, fmts);
   } else {
      assert(cb_count == 0 && "dynamic blending not yet supported");
   }

   /* Now that we've lowered blending, lower tilebuffer access. TODO: MSAA */
   struct agx_tilebuffer_layout tib =
      agx_build_tilebuffer_layout(fmts, cb_count, 1);

   ASSERTED bool force_translucent = false;
   agx_nir_lower_tilebuffer(nir, &tib, colour_masks, NULL, &force_translucent);
   assert(!force_translucent && "no tib writemasking used");

   NIR_PASS_V(nir, agx_nir_lower_sample_intrinsics);
   NIR_PASS_V(nir, agx_nir_lower_monolithic_msaa,
              &(struct agx_msaa_state){
                 .nr_samples = tib.nr_samples,
                 .api_sample_mask = false /* TODO */,
              });
   key->fs.nr_samples = tib.nr_samples;
}

static VkResult
agxv_graphics_pipeline_create(struct agxv_device *device,
                              const VkGraphicsPipelineCreateInfo *pCreateInfo,
                              const VkAllocationCallbacks *pAllocator,
                              VkPipeline *pPipeline)
{
   VK_FROM_HANDLE(agxv_pipeline_layout, pipeline_layout, pCreateInfo->layout);
   VkResult result = VK_SUCCESS;

   struct agxv_graphics_pipeline *pipeline;

   pipeline = vk_object_zalloc(&device->vk, pAllocator, sizeof(*pipeline),
                               VK_OBJECT_TYPE_PIPELINE);
   if (pipeline == NULL)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   pipeline->base.type = AGXV_PIPELINE_GRAPHICS;

   const VkPipelineShaderStageCreateInfo *stages = pCreateInfo->pStages;

   struct vk_graphics_pipeline_state state = {};
   struct vk_graphics_pipeline_all_state all_state;
   result = vk_graphics_pipeline_state_fill(&device->vk, &state, pCreateInfo,
                                            NULL, /* sp_info */
                                            &all_state, NULL, 0, NULL);

   vk_dynamic_graphics_state_fill(&pipeline->dynamic, &state);

   /* Compile fragment shader first to propagate the
    * flatshading/perspective-correction info for interpolated varyings back to
    * vertex shaders. This is a bit delicate but meh.
    */
   for (unsigned i = 0; i < pCreateInfo->stageCount; ++i) {
      if (stages[i].stage == VK_SHADER_STAGE_FRAGMENT_BIT) {
         agxv_compile_pipeline_stage(device, &pipeline->base, pipeline_layout,
                                     &stages[i], agxv_lower_fragment, &state);
      }
   }

   for (unsigned i = 0; i < pCreateInfo->stageCount; ++i) {
      if (stages[i].stage == VK_SHADER_STAGE_VERTEX_BIT) {
         agxv_compile_pipeline_stage(device, &pipeline->base, pipeline_layout,
                                     &stages[i], agxv_lower_vertex, &state);
      }
   }

   link_varyings(device, pipeline);

   pipeline->base.destroy = agxv_graphics_pipeline_destroy;

   *pPipeline = agxv_graphics_pipeline_to_handle(pipeline);
   return VK_SUCCESS;
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateGraphicsPipelines(VkDevice _device, VkPipelineCache cache,
                             uint32_t createInfoCount,
                             const VkGraphicsPipelineCreateInfo *pCreateInfos,
                             const VkAllocationCallbacks *pAllocator,
                             VkPipeline *pPipelines)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VkResult result = VK_SUCCESS;

   unsigned i = 0;
   for (; i < createInfoCount; i++) {
      result = agxv_graphics_pipeline_create(device, &pCreateInfos[i],
                                             pAllocator, &pPipelines[i]);

      if (result != VK_SUCCESS) {
         pPipelines[i] = VK_NULL_HANDLE;

         if (pCreateInfos[i].flags &
             VK_PIPELINE_CREATE_EARLY_RETURN_ON_FAILURE_BIT)
            break;
      }
   }

   for (; i < createInfoCount; i++)
      pPipelines[i] = VK_NULL_HANDLE;

   return result;
}
