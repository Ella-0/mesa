/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#ifndef AGXV_PIPELINE
#define AGXV_PIPELINE 1

#include "agxv_private.h"

#include "asahi/compiler/agx_compile.h"
#include "asahi/lib/agx_device.h"
#include "compiler/nir/nir_builder.h"
#include "agxv_device.h"
#include "agxv_pipeline_layout.h"

enum agxv_pipeline_type {
   AGXV_PIPELINE_GRAPHICS,
   AGXV_PIPELINE_COMPUTE,
};

struct agxv_pipeline {
   struct vk_object_base base;

   enum agxv_pipeline_type type;

   struct {
      struct agx_bo *bo;
      struct agx_shader_info info;
   } stages[MESA_SHADER_STAGES];

   /* Used to work out what range of buffers to bind on vertex state emission.
    * This is suboptimal if buffer binding locations are not contiguous.
    */
   uint32_t buffer_base;
   uint32_t buffer_len;

   void (*destroy)(struct agxv_pipeline *);
};

VkResult agxv_compile_pipeline_stage(
   struct agxv_device *device, struct agxv_pipeline *pipeline,
   struct agxv_pipeline_layout *layout,
   const VkPipelineShaderStageCreateInfo *pCreateInfo,
   void (*stage_specific_lowering)(struct agxv_pipeline *pipeline,
                                   nir_shader *nir, struct agx_shader_key *,
                                   void *),
   void *data);

VK_DEFINE_NONDISP_HANDLE_CASTS(agxv_pipeline, base, VkPipeline,
                               VK_OBJECT_TYPE_PIPELINE);

#endif
