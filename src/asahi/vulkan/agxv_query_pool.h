/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#ifndef AGXV_QUERY_POOL
#define AGXV_QUERY_POOL 1

#include "agxv_private.h"

struct agxv_query_pool {
   struct vk_object_base base;

   struct agx_bo *bo;
};

VK_DEFINE_NONDISP_HANDLE_CASTS(agxv_query_pool, base, VkQueryPool,
                               VK_OBJECT_TYPE_QUERY_POOL);

#endif
