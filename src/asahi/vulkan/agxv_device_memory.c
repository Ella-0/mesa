/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_device_memory.h"

#include <sys/mman.h>
#include "agxv_device.h"
#include "agxv_physical_device.h"

VKAPI_ATTR VkResult VKAPI_CALL
agxv_AllocateMemory(VkDevice _device, const VkMemoryAllocateInfo *pAllocateInfo,
                    const VkAllocationCallbacks *pAllocator,
                    VkDeviceMemory *pMemory)
{
   VK_FROM_HANDLE(agxv_device, device, _device);

   VkResult result = VK_SUCCESS;

   struct agxv_device_memory *mem;

   mem = vk_object_alloc(&device->vk, pAllocator, sizeof(*mem),
                         VK_OBJECT_TYPE_DEVICE_MEMORY);
   if (!mem) {
      result = VK_ERROR_OUT_OF_HOST_MEMORY;
      vk_error(device, result);
      goto fail;
   }

   int agx_bo_flags = 0;

   vk_foreach_struct_const(ext, pAllocateInfo->pNext) {
      switch (ext->sType) {
      case VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO: {
         VkExportMemoryAllocateInfo *pInfo = (void *)ext;
         if (pInfo->handleTypes)
            agx_bo_flags |= AGX_BO_SHAREABLE;

         break;
      }
      default:
         agxv_debug_ignored_stype(ext->sType);
      }
   }

   mem->bo = agx_bo_create(&device->pdev->dev, pAllocateInfo->allocationSize,
                           agx_bo_flags, "Device Memory");
   if (!mem->bo) {
      result = VK_ERROR_OUT_OF_DEVICE_MEMORY;
      vk_error(device, result);
      goto fail_alloc;
   }

   *pMemory = agxv_device_memory_to_handle(mem);

   return VK_SUCCESS;
fail_alloc:
   vk_object_free(&device->vk, pAllocator, mem);
fail:
   return result;
}

VKAPI_ATTR void VKAPI_CALL
agxv_FreeMemory(VkDevice _device, VkDeviceMemory _memory,
                const VkAllocationCallbacks *pAllocator)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_device_memory, mem, _memory);

   if (!mem)
      return;

   agx_bo_unreference(mem->bo);

   vk_object_free(&device->vk, pAllocator, mem);
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_MapMemory(VkDevice _device, VkDeviceMemory _memory, VkDeviceSize offset,
               VkDeviceSize size, VkMemoryMapFlags flags, void **ppData)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_device_memory, mem, _memory);

   if (!mem->bo->ptr.cpu) {
      agx_bo_mmap(mem->bo);
   }

   if (!mem->bo->ptr.cpu)
      return vk_error(device, VK_ERROR_MEMORY_MAP_FAILED);

   *ppData = mem->bo->ptr.cpu + offset;

   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_UnmapMemory(VkDevice _device, VkDeviceMemory _memory)
{
   VK_FROM_HANDLE(agxv_device_memory, mem, _memory);
   /* This causes issues with the agx_bo.c
    * the some places assume all bos are mapped bo create will hapily return
    * a recently free'd bo that's been unmaped without mapping it again.
   munmap(mem->bo->ptr.cpu, mem->bo->size);
   mem->bo->ptr.cpu = NULL;
    */
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_FlushMappedMemoryRanges(VkDevice _device, uint32_t memoryRangeCount,
                             const VkMappedMemoryRange *pMemoryRanges)
{
   /* We don't need to do anything here; memory is coherent */
   return VK_SUCCESS;
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_InvalidateMappedMemoryRanges(VkDevice _device, uint32_t memoryRangeCount,
                                  const VkMappedMemoryRange *pMemoryRanges)
{
   return VK_SUCCESS;
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_GetMemoryFdKHR(VkDevice _device, const VkMemoryGetFdInfoKHR *pGetFdInfo,
                    int *pFd)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_device_memory, memory, pGetFdInfo->memory);
   assert(pGetFdInfo->sType == VK_STRUCTURE_TYPE_MEMORY_GET_FD_INFO_KHR);

   assert(
      pGetFdInfo->handleType == VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT ||
      pGetFdInfo->handleType == VK_EXTERNAL_MEMORY_HANDLE_TYPE_DMA_BUF_BIT_EXT);

   int prime_fd = agx_bo_export(memory->bo);
   if (prime_fd < 0)
      return vk_error(device, VK_ERROR_OUT_OF_DEVICE_MEMORY);

   *pFd = prime_fd;
   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_GetDeviceMemoryCommitment(VkDevice _device, VkDeviceMemory _memory,
                               VkDeviceSize *pCommittedMemoryInBytes)
{
   *pCommittedMemoryInBytes = 0;
}
