/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "vk_buffer_view.h"
#ifndef AGXV_IMAGE_VIEW
#define AGXV_IMAGE_VIEW 1

#include "agxv_private.h"

#include "asahi/lib/agx_pack.h"

#include "vulkan/runtime/vk_image.h"

struct agxv_image_view {
   struct vk_image_view vk;
   struct agxv_image *image;

   struct agx_texture_packed texture_desc;
   struct agx_pbe_packed pbe_desc;
};

VK_DEFINE_HANDLE_CASTS(agxv_image_view, vk.base, VkImageView,
                       VK_OBJECT_TYPE_IMAGE_VIEW)

struct agxv_buffer_view {
   struct vk_buffer_view vk;
   struct agxv_buffer *buffer;

   struct agx_texture_packed texture_desc;
   struct agx_pbe_packed pbe_desc;
};
VK_DEFINE_HANDLE_CASTS(agxv_buffer_view, vk.base, VkBufferView,
                       VK_OBJECT_TYPE_BUFFER_VIEW)

#endif
