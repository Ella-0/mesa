/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agx_helpers.h"
#include "agx_pack.h"
#include "agx_tilebuffer.h"
#include "agxv_cmd_buffer.h"

#include "agxv_buffer.h"
#include "agxv_descriptor_pool.h"
#include "agxv_descriptor_set.h"
#include "agxv_device.h"
#include "agxv_graphics_pipeline.h"
#include "agxv_image.h"
#include "agxv_image_view.h"
#include "agxv_limits.h"
#include "agxv_physical_device.h"
#include "vk_graphics_state.h"
#include "vk_util.h"

#include "asahi/lib/agx_meta.h"
#include "asahi/lib/agx_ppp.h"
#include "asahi/lib/agx_usc.h"
#include "asahi/lib/decode.h"

#include "compiler/shader_enums.h"
#include "vulkan/util/vk_format.h"
#include "vulkan/vulkan_core.h"

static enum agx_meta_op
translate_load_op(enum VkAttachmentLoadOp op)
{
   switch (op) {
   case VK_ATTACHMENT_LOAD_OP_LOAD:
      return AGX_META_OP_LOAD;

   case VK_ATTACHMENT_LOAD_OP_CLEAR:
      return AGX_META_OP_CLEAR;

   case VK_ATTACHMENT_LOAD_OP_DONT_CARE:
   case VK_ATTACHMENT_LOAD_OP_NONE_EXT:
      return AGX_META_OP_NONE;

   default:
      unreachable("Unimplemented load op");
   }
}

static enum agx_meta_op
translate_store_op(enum VkAttachmentStoreOp op)
{
   switch (op) {
   case VK_ATTACHMENT_STORE_OP_STORE:
      return AGX_META_OP_STORE;

   case VK_ATTACHMENT_STORE_OP_DONT_CARE:
   case VK_ATTACHMENT_STORE_OP_NONE:
      return AGX_META_OP_NONE;

   default:
      unreachable("Unimplemented store op");
   }
}

static uint32_t
agxv_build_meta(struct agxv_cmd_buffer *cmd_buffer, uint32_t attachment_count,
                const VkRenderingAttachmentInfo *attachments, bool store,
                bool partial_render, uint32_t *bind)
{
   struct agx_meta_key key = {
      .tib = cmd_buffer->state.tib,
   };

   struct agx_usc_builder b =
      agx_alloc_usc_control(&cmd_buffer->shader_pool, attachment_count + 1);

   bool needs_sampler = false;
   unsigned uniforms = 0, textures = 0;

   for (uint32_t i = 0; i < attachment_count; i++) {
      if (partial_render)
         key.op[i] = store ? AGX_META_OP_STORE : AGX_META_OP_LOAD;
      else if (store)
         key.op[i] = translate_store_op(attachments[i].storeOp);
      else
         key.op[i] = translate_load_op(attachments[i].loadOp);

      switch (key.op[i]) {
      case AGX_META_OP_LOAD: {
         VK_FROM_HANDLE(agxv_image_view, view, attachments[i].imageView);
         textures = MAX2(textures, i + 1);
         agx_usc_pack(&b, TEXTURE, cfg) {
            cfg.start = i;
            cfg.count = 1;
            cfg.buffer =
               agx_pool_upload(&cmd_buffer->cmd_pool, &view->texture_desc,
                               sizeof(view->texture_desc));
         };
         needs_sampler = true;
         break;
      }

      case AGX_META_OP_CLEAR: {
         unsigned base = 4 + (8 * i);
         uniforms = MAX2(uniforms, base + 8);
         agx_usc_uniform(
            &b, base, 8,
            agx_pool_upload(&cmd_buffer->cmd_pool,
                            &attachments[i].clearValue.color,
                            sizeof(attachments[i].clearValue.color)));
         break;
      }

      case AGX_META_OP_STORE: {
         VK_FROM_HANDLE(agxv_image_view, view, attachments[i].imageView);
         textures = MAX2(textures, i + 1);
         agx_usc_pack(&b, TEXTURE, cfg) {
            cfg.start = i;
            cfg.count = 1;
            cfg.buffer = agx_pool_upload(&cmd_buffer->cmd_pool, &view->pbe_desc,
                                         sizeof(view->pbe_desc));
         };
         break;
      }
      default:
         break;
      }
   }

   if (needs_sampler) {
      struct agx_ptr sampler =
         agx_pool_alloc_aligned(&cmd_buffer->cmd_pool, AGX_SAMPLER_LENGTH, 64);

      agx_pack(sampler.cpu, SAMPLER, cfg) {
         cfg.magnify_linear = true;
         cfg.minify_linear = false;
         cfg.mip_filter = AGX_MIP_FILTER_NONE;
         cfg.wrap_s = AGX_WRAP_CLAMP_TO_EDGE;
         cfg.wrap_t = AGX_WRAP_CLAMP_TO_EDGE;
         cfg.wrap_r = AGX_WRAP_CLAMP_TO_EDGE;
         cfg.pixel_coordinates = true;
         cfg.compare_func = AGX_COMPARE_FUNC_ALWAYS;
      }

      agx_usc_pack(&b, SAMPLER, cfg) {
         cfg.start = 0;
         cfg.count = 1;
         cfg.buffer = sampler.gpu;
      }
   }

   struct agx_meta_shader *shader =
      agx_get_meta_shader(&cmd_buffer->device->meta_cache, &key);

   agx_usc_pack(&b, REGISTERS, cfg) {
      cfg.register_count = shader->info.nr_gprs;
      cfg.unk_1 = true;
   }
   agx_usc_tilebuffer(&b, &cmd_buffer->state.tib);
   agx_usc_pack(&b, SHADER, cfg) {
      cfg.code = shader->ptr;
   }

   if (shader->info.has_preamble) {
      agx_usc_pack(&b, PRESHADER, cfg) {
         cfg.code = shader->bo->ptr.gpu + shader->info.preamble_offset;
      }
   } else {
      agx_usc_pack(&b, NO_PRESHADER, cfg)
         ;
   }

   agx_pack(bind, COUNTS, cfg) {
      cfg.uniforms = uniforms;
      cfg.texture_states = textures;
      cfg.sampler_states = needs_sampler ? 1 : 0;
      cfg.preshader_gprs = shader->info.nr_preamble_gprs;
      cfg.unknown = store ? 0 : 0xFFFF;
   }

   return agx_usc_fini(&b);
}

static void
agxv_attachment_init(struct agxv_attachment *att,
                     const VkRenderingAttachmentInfo *info)
{
   if (info == NULL || info->imageView == VK_NULL_HANDLE) {
      *att = (struct agxv_attachment){
         .iview = NULL,
      };
      return;
   }

   VK_FROM_HANDLE(agxv_image_view, iview, info->imageView);
   *att = (struct agxv_attachment){
      .vk_format = iview->vk.format,
      .iview = iview,
   };

   if (info->resolveMode != VK_RESOLVE_MODE_NONE) {
      VK_FROM_HANDLE(agxv_image_view, res_iview, info->resolveImageView);
      att->resolve_mode = info->resolveMode;
      att->resolve_iview = res_iview;
   }
}

/* FIXME:
 * In AGXV we don't implement renderpasses for now. This means we are
 * potentially leaving performance on the table, since AGX is a tiler.
 *
 * Instead we implement dynamic rendering and common code implements
 * renderpasses for us ontop of this. There are talks of making the common
 * implementation more appropriate for tilers but that isn't in place for now
 * however this is *fine* to just get things working.
 *
 * Ideally we want some way to avoid writing to the framebuffer between each
 * subpass in common code.
 */
VKAPI_ATTR void VKAPI_CALL
agxv_CmdBeginRendering(VkCommandBuffer commandBuffer,
                       const VkRenderingInfo *pRenderingInfo)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);

   struct agxv_rendering_state *render = &cmd_buffer->state.render;

   memset(render, 0, sizeof(*render));
   render->flags = pRenderingInfo->flags;
   render->area = pRenderingInfo->renderArea;
   render->view_mask = pRenderingInfo->viewMask;
   render->layer_count = pRenderingInfo->layerCount;
   render->color_att_count = pRenderingInfo->colorAttachmentCount;

   for (uint32_t i = 0; i < render->color_att_count; i++) {
      agxv_attachment_init(&render->color_att[i],
                           &pRenderingInfo->pColorAttachments[i]);
   }

   agxv_attachment_init(&render->depth_att, pRenderingInfo->pDepthAttachment);

   agxv_attachment_init(&render->stencil_att,
                        pRenderingInfo->pStencilAttachment);

   struct agxv_job *job = agxv_job_create(cmd_buffer, AGXV_JOB_GRAPHICS,
                                          pRenderingInfo->colorAttachmentCount);
   if (!job)
      vk_error(cmd_buffer, VK_ERROR_OUT_OF_HOST_MEMORY);

   job->render_buf.fb_width = pRenderingInfo->renderArea.extent.width;
   job->render_buf.fb_height = pRenderingInfo->renderArea.extent.height;

   enum pipe_format tib_formats[8];

   job->render_buf.flags |= ASAHI_RENDER_PROCESS_EMPTY_TILES;
   for (uint32_t i = 0; i < pRenderingInfo->colorAttachmentCount; i++) {
      VK_FROM_HANDLE(agxv_image_view, view,
                     pRenderingInfo->pColorAttachments[i].imageView);

      job->attachments[i].order = 1;
      job->attachments[i].size = view->image->layout.size_B;
      job->attachments[i].pointer = agxv_image_addr(view->image, 0);

      if (pRenderingInfo->pColorAttachments[i].loadOp ==
          VK_ATTACHMENT_LOAD_OP_CLEAR) {
         job->render_buf.flags |= ASAHI_RENDER_PROCESS_EMPTY_TILES;
      }

      tib_formats[i] = vk_format_to_pipe_format(view->vk.format);
   }

   cmd_buffer->state.tib = agx_build_tilebuffer_layout(
      tib_formats, pRenderingInfo->colorAttachmentCount, 1);

   struct agx_tilebuffer_layout *tib = &cmd_buffer->state.tib;
   job->render_buf.utile_width = tib->tile_size.width;
   job->render_buf.utile_height = tib->tile_size.height;

   job->render_buf.samples = tib->nr_samples;
   job->render_buf.layers = pRenderingInfo->layerCount;

   job->render_buf.sample_size = tib->sample_size_B;

   job->render_buf.tib_blocks =
      ALIGN_POT(agx_tilebuffer_total_size(tib), 2048) / 2048;
   job->render_buf.iogpu_unk_214 = 0xc000;

   /* TODO: don't use 1 << 9 when don't need to load? Needs more testing. */
   job->render_buf.isp_bgobjvals = 0x300;

   if (pRenderingInfo->pDepthAttachment) {
      job->render_buf.isp_bgobjdepth =
         fui(pRenderingInfo->pDepthAttachment->clearValue.depthStencil.depth);
   } else {
      job->render_buf.isp_bgobjdepth = 0;
   }

   if (pRenderingInfo->pStencilAttachment) {
      job->render_buf.isp_bgobjvals |=
         pRenderingInfo->pStencilAttachment->clearValue.depthStencil.stencil;
   }

   job->render_buf.load_pipeline =
      agxv_build_meta(cmd_buffer, pRenderingInfo->colorAttachmentCount,
                      pRenderingInfo->pColorAttachments, false, false,
                      &job->render_buf.load_pipeline_bind);

   job->render_buf.store_pipeline =
      agxv_build_meta(cmd_buffer, pRenderingInfo->colorAttachmentCount,
                      pRenderingInfo->pColorAttachments, true, false,
                      &job->render_buf.store_pipeline_bind);

   job->render_buf.partial_reload_pipeline =
      agxv_build_meta(cmd_buffer, pRenderingInfo->colorAttachmentCount,
                      pRenderingInfo->pColorAttachments, false, true,
                      &job->render_buf.partial_reload_pipeline_bind);

   job->render_buf.partial_store_pipeline =
      agxv_build_meta(cmd_buffer, pRenderingInfo->colorAttachmentCount,
                      pRenderingInfo->pColorAttachments, true, true,
                      &job->render_buf.partial_store_pipeline_bind);

   cmd_buffer->state.job = job;

   float tan_60 = 1.732051f;
   job->render_buf.merge_upper_x = fui(tan_60 / job->render_buf.fb_width);
   job->render_buf.merge_upper_y = fui(tan_60 / job->render_buf.fb_height);
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdBindVertexBuffers2(VkCommandBuffer commandBuffer, uint32_t firstBinding,
                           uint32_t bindingCount, const VkBuffer *pBuffers,
                           const VkDeviceSize *pOffsets,
                           const VkDeviceSize *pSizes,
                           const VkDeviceSize *pStrides)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);

   if (pStrides) {
      vk_cmd_set_vertex_binding_strides(&cmd_buffer->vk, firstBinding,
                                        bindingCount, pStrides);
   }

   if (bindingCount) {
      cmd_buffer->state.dirty.buffers = true;
      cmd_buffer->state.dirty.vs = true;
   }

   for (uint32_t i = 0; i < bindingCount; i++) {
      VK_FROM_HANDLE(agxv_buffer, buffer, pBuffers[i]);
      cmd_buffer->state.buffers[firstBinding + i] =
         agxv_buffer_addr(buffer) + pOffsets[i];
   }
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdBindIndexBuffer(VkCommandBuffer commandBuffer, VkBuffer _buffer,
                        VkDeviceSize offset, VkIndexType type)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   VK_FROM_HANDLE(agxv_buffer, buffer, _buffer);
   cmd_buffer->state.index_buffer = agxv_buffer_addr(buffer) + offset;
   cmd_buffer->state.index_size_B = vk_index_type_to_bytes(type);
}

uint32_t
agxv_emit_shader(struct agxv_cmd_buffer *cmd_buffer,
                 enum pipe_shader_type stage)
{
   struct agx_shader_info *info =
      &cmd_buffer->state.pipeline->stages[stage].info;
   struct agx_bo *bo = cmd_buffer->state.pipeline->stages[stage].bo;
   struct agxv_pipeline *pipeline = cmd_buffer->state.pipeline;

   /* FIXME: these are only needed when buffers or sets are dirty. */
   uint64_t pushs =
      agx_pool_upload_aligned(&cmd_buffer->cmd_pool, cmd_buffer->state.push,
                              sizeof(cmd_buffer->state.push), 64);
   uint64_t push_buf =
      agx_pool_upload_aligned(&cmd_buffer->cmd_pool, &pushs, sizeof(pushs), 64);

   uint64_t buffers =
      agx_pool_upload_aligned(&cmd_buffer->cmd_pool, cmd_buffer->state.buffers,
                              sizeof(cmd_buffer->state.buffers), 64);

   uint64_t sets = agx_pool_upload_aligned(
      &cmd_buffer->cmd_pool, cmd_buffer->state.descriptors.set_vas,
      sizeof(uint64_t) * AGXV_MAX_SETS, 64);

   uint32_t push_base = cmd_buffer->state.push_base;
   uint32_t sets_base = cmd_buffer->state.descriptors.sets_base;
   uint32_t push_len = sets_base - push_base;

   /* We must get the buffer base from the pipeline not calculate it here to
    * maintain pipeline layout compatibility
    */
   uint32_t sets_len = pipeline->buffer_base - sets_base;

   /* Descriptor counts */
   uint32_t texture_count = cmd_buffer->state.descriptors.ts_count;
   uint32_t sampler_count = cmd_buffer->state.descriptors.ss_count;

   /* Include the internal txf sampler */
   uint32_t internal_sampler_count = sampler_count;
   if (info->uses_txf) {
      internal_sampler_count =
         MAX2(internal_sampler_count, info->txf_sampler + 1);
   }

   struct agx_usc_builder b = agx_alloc_usc_control(
      &cmd_buffer->shader_pool,
      !!push_len + !!sets_len + (AGXV_MAX_SETS * 2 /* textures and samples */) +
         1 /* internal sampler */ + !!pipeline->buffer_len);

   if (stage == MESA_SHADER_COMPUTE) {
      unsigned shared_size = info->local_size;

      agx_usc_pack(&b, SHARED, cfg) {
         cfg.layout = AGX_SHARED_LAYOUT_VERTEX_COMPUTE;
         cfg.bytes_per_threadgroup = shared_size > 0 ? shared_size : 65536;
         cfg.uses_shared_memory = shared_size > 0;
      }
   } else if (stage == MESA_SHADER_FRAGMENT) {
      agx_usc_tilebuffer(&b, &cmd_buffer->state.tib);
   } else {
      agx_usc_shared_none(&b);
   }

   if (push_len)
      agx_usc_uniform(&b, push_base, 4, push_buf);

   if (sets_len) {
      agx_usc_uniform(&b, sets_base, 4, sets);

      for (unsigned i = 0; i < AGXV_MAX_SETS; ++i) {
         struct agxv_descriptor_set *set =
            cmd_buffer->state.descriptors.sets[i];

         struct agxv_descriptor_set_layout *layout =
            set ? set->layout : cmd_buffer->state.descriptors.pushed_layouts[i];

         if (!layout)
            continue;

         if (layout->texture_count) {
            agx_usc_pack(&b, TEXTURE, cfg) {
               cfg.start = layout->texture_index;
               cfg.count = layout->texture_count;

               if (set) {
                  cfg.buffer = agx_pool_upload_aligned(
                     &cmd_buffer->cmd_pool, set->textures,
                     AGX_TEXTURE_LENGTH * cfg.count, 64);
               } else {
                  cfg.buffer = cmd_buffer->state.descriptors.pushed_textures[i];
               }
            }
         }

         if (layout->sampler_count) {
            agx_usc_pack(&b, SAMPLER, cfg) {
               cfg.start = layout->sampler_index;
               cfg.count = layout->sampler_count;

               if (set) {
                  cfg.buffer = agx_pool_upload_aligned(
                     &cmd_buffer->cmd_pool, set->samplers,
                     AGX_SAMPLER_LENGTH * cfg.count, 64);
               } else {
                  cfg.buffer = cmd_buffer->state.descriptors.pushed_samplers[i];
               }
            }
         }
      }
   }

   if (info->uses_txf) {
      struct agx_sampler_packed sampler;

      agx_pack(&sampler, SAMPLER, cfg) {
         /* Allow mipmapping. This is respected by txf, weirdly. */
         cfg.mip_filter = AGX_MIP_FILTER_NEAREST;

         /* Out-of-bounds reads must return 0 */
         cfg.wrap_s = AGX_WRAP_CLAMP_TO_BORDER;
         cfg.wrap_t = AGX_WRAP_CLAMP_TO_BORDER;
         cfg.wrap_r = AGX_WRAP_CLAMP_TO_BORDER;
         cfg.border_colour = AGX_BORDER_COLOUR_TRANSPARENT_BLACK;
      }

      agx_usc_pack(&b, SAMPLER, cfg) {
         cfg.start = info->txf_sampler;
         cfg.count = 1;
         cfg.buffer = agx_pool_upload_aligned(&cmd_buffer->cmd_pool, &sampler,
                                              sizeof(sampler), 8);
      }
   }

   if (pipeline->buffer_len)
      agx_usc_uniform(&b, pipeline->buffer_base, pipeline->buffer_len, buffers);

   agx_usc_pack(&b, SHADER, cfg) {
      cfg.code = bo->ptr.gpu;
      cfg.unk_2 = 2;

      if (stage == MESA_SHADER_FRAGMENT)
         cfg.loads_varyings = info->varyings.fs.nr_bindings > 0;
   }

   agx_usc_pack(&b, REGISTERS, cfg) {
      cfg.register_count = info->nr_gprs;
   }

   if (info->has_preamble) {
      agx_usc_pack(&b, PRESHADER, cfg) {
         cfg.code = bo->ptr.gpu + info->preamble_offset;
      }
   } else {
      agx_usc_pack(&b, NO_PRESHADER, cfg)
         ;
   }
   return agx_usc_fini(&b);
}

static unsigned
upload_scissor(struct agxv_cmd_buffer *cmd_buffer,
               struct vk_dynamic_graphics_state *state)
{
   /* Allocate a new scissor descriptor */
   unsigned index = cmd_buffer->state.scissor.size / AGX_SCISSOR_LENGTH;
   void *ptr = util_dynarray_grow_bytes(&cmd_buffer->state.scissor, 1,
                                        AGX_SCISSOR_LENGTH);

   /* By default, scissor to the viewport */
   uint16_t min_x = state->vp.viewports[0].x;
   uint16_t min_y = state->vp.viewports[0].y;
   uint16_t max_x = min_x + state->vp.viewports[0].width;
   uint16_t max_y = min_y + state->vp.viewports[0].height;

   if (state->vp.scissor_count) {
      assert(state->vp.scissor_count == 1);
      VkRect2D scissor = state->vp.scissors[0];

      uint16_t sciss_min_x = scissor.offset.x;
      uint16_t sciss_min_y = scissor.offset.y;
      uint16_t sciss_max_x = sciss_min_x + scissor.extent.width;
      uint16_t sciss_max_y = sciss_min_y + scissor.extent.height;

      /* Intersect scissor with viewport */
      min_x = MAX2(sciss_min_x, min_x);
      min_y = MAX2(sciss_min_y, min_y);
      max_x = MIN2(sciss_max_x, max_x);
      max_y = MIN2(sciss_max_y, max_y);
   }

   agx_pack(ptr, SCISSOR, cfg) {
      cfg.min_x = min_x;
      cfg.min_y = min_y;
      cfg.max_x = max_x;
      cfg.max_y = max_y;
      cfg.min_z = state->vp.viewports[0].minDepth;
      cfg.max_z = state->vp.viewports[0].maxDepth;
   }

   return index;
}

static enum agx_zs_func
translate_zs_func(VkCompareOp op)
{
   switch (op) {
   case VK_COMPARE_OP_NEVER:
      return AGX_ZS_FUNC_NEVER;
   case VK_COMPARE_OP_LESS:
      return AGX_ZS_FUNC_LESS;
   case VK_COMPARE_OP_EQUAL:
      return AGX_ZS_FUNC_EQUAL;
   case VK_COMPARE_OP_LESS_OR_EQUAL:
      return AGX_ZS_FUNC_LEQUAL;
   case VK_COMPARE_OP_GREATER:
      return AGX_ZS_FUNC_GREATER;
   case VK_COMPARE_OP_NOT_EQUAL:
      return AGX_ZS_FUNC_NOT_EQUAL;
   case VK_COMPARE_OP_GREATER_OR_EQUAL:
      return AGX_ZS_FUNC_GEQUAL;
   case VK_COMPARE_OP_ALWAYS:
      return AGX_ZS_FUNC_ALWAYS;
   default:
      unreachable("invalid compare");
   }
}

static enum agx_object_type
topology_to_object_type(VkPrimitiveTopology topology)
{
   switch (topology) {
   case VK_PRIMITIVE_TOPOLOGY_POINT_LIST:
      return AGX_OBJECT_TYPE_POINT_SPRITE_UV01;

   case VK_PRIMITIVE_TOPOLOGY_LINE_LIST:
   case VK_PRIMITIVE_TOPOLOGY_LINE_STRIP:
      return AGX_OBJECT_TYPE_LINE;

   default:
      return AGX_OBJECT_TYPE_TRIANGLE;
   }
}

static void
agxv_emit_ppp(struct agxv_cmd_buffer *cmd_buffer)
{
   uint8_t *buf = cmd_buffer->state.encoder_current;

   struct agxv_graphics_pipeline *pipeline = container_of(
      cmd_buffer->state.pipeline, struct agxv_graphics_pipeline, base);

   struct vk_dynamic_graphics_state *state =
      &cmd_buffer->vk.dynamic_graphics_state;
   BITSET_WORD *dirty = state->dirty;

#define IS_DIRTY(ST) BITSET_TEST(dirty, MESA_VK_DYNAMIC_##ST)

   struct AGX_PPP_HEADER ppp_header = {
      .fragment_control =
         cmd_buffer->state.dirty.pipeline || IS_DIRTY(DS_DEPTH_TEST_ENABLE) ||
         IS_DIRTY(DS_STENCIL_TEST_ENABLE) || IS_DIRTY(RS_DEPTH_BIAS_ENABLE),

      .fragment_control_2 =
         cmd_buffer->state.dirty.pipeline || IS_DIRTY(IA_PRIMITIVE_TOPOLOGY),

      .fragment_front_face =
         cmd_buffer->state.dirty.pipeline || IS_DIRTY(DS_STENCIL_REFERENCE) ||
         IS_DIRTY(DS_DEPTH_COMPARE_OP) || IS_DIRTY(DS_DEPTH_TEST_ENABLE) ||
         IS_DIRTY(DS_DEPTH_WRITE_ENABLE) || IS_DIRTY(RS_LINE_WIDTH),

      .fragment_front_face_2 =
         cmd_buffer->state.dirty.pipeline || IS_DIRTY(IA_PRIMITIVE_TOPOLOGY),

      /* TODO: */
      .fragment_front_stencil = false,

      .fragment_back_face = cmd_buffer->state.dirty.pipeline ||
                            IS_DIRTY(DS_STENCIL_REFERENCE) ||
                            IS_DIRTY(RS_LINE_WIDTH),

      .fragment_back_face_2 =
         cmd_buffer->state.dirty.pipeline || IS_DIRTY(IA_PRIMITIVE_TOPOLOGY),

      /* TODO: */
      .fragment_back_stencil = false,

      .depth_bias_scissor = IS_DIRTY(VP_SCISSORS),

      /* TODO: update less frequently */
      .region_clip = cmd_buffer->state.dirty.pipeline,

      .viewport = cmd_buffer->state.dirty.pipeline || IS_DIRTY(VP_VIEWPORTS),

      /* TODO: update less frequently */
      .w_clamp = cmd_buffer->state.dirty.pipeline,

      .output_select = cmd_buffer->state.dirty.pipeline,

      .varying_counts_32 = cmd_buffer->state.dirty.pipeline,
      .varying_counts_16 = cmd_buffer->state.dirty.pipeline,

      .cull = cmd_buffer->state.dirty.pipeline || IS_DIRTY(RS_CULL_MODE) ||
              IS_DIRTY(RS_FRONT_FACE),

      /* TODO: this isn't correct; this should be updated less freqently than
       * on pipeline bind
       */
      .cull_2 = cmd_buffer->state.dirty.pipeline,
      .fragment_shader = cmd_buffer->state.dirty.fs,
      .occlusion_query = false,
      .occlusion_query_2 = false,
      .output_unknown = cmd_buffer->state.dirty.pipeline,
      .output_size = cmd_buffer->state.dirty.pipeline,
      .varying_word_2 = cmd_buffer->state.dirty.pipeline,
   };

#undef IS_DIRTY

   struct agx_shader_info *vs_info =
      &cmd_buffer->state.pipeline->stages[MESA_SHADER_VERTEX].info;
   struct agx_shader_info *fs_info =
      &cmd_buffer->state.pipeline->stages[MESA_SHADER_FRAGMENT].info;

   enum agx_object_type object_type =
      topology_to_object_type(state->ia.primitive_topology);

   struct agx_ppp_update ppp =
      agx_new_ppp_update(&cmd_buffer->cmd_pool, ppp_header);

#define ppp_push_if_dirty(name, NAME, cfg)                                     \
   if (ppp_header.name)                                                        \
      agx_ppp_push(&ppp, NAME, cfg)

   uint8_t *start_head = ppp.head;

   ppp_push_if_dirty(fragment_control, FRAGMENT_CONTROL, cfg) {
      cfg.stencil_test_enable = state->ds.stencil.test_enable;
      cfg.depth_bias_enable = state->rs.depth_bias.enable;
      cfg.visibility_mode = AGX_VISIBILITY_MODE_NONE;

      /* TODO: Optimize */
      cfg.two_sided_stencil = state->ds.stencil.test_enable;

      /* TODO: Optimize */
      cfg.scissor_enable = true;
   }

   ppp_push_if_dirty(fragment_control_2, FRAGMENT_CONTROL, cfg) {
      cfg.tag_write_disable = fs_info->tag_write_disable;
      cfg.pass_type = agx_pass_type_for_shader(fs_info);

      cfg.disable_tri_merging = true ||
                                (object_type != AGX_OBJECT_TYPE_TRIANGLE) ||
                                fs_info->disable_tri_merging;
   }

   ppp_push_if_dirty(fragment_front_face, FRAGMENT_FACE, cfg) {
      cfg.disable_depth_write = !state->ds.depth.write_enable;

      cfg.depth_function = translate_zs_func(state->ds.depth.test_enable
                                                ? state->ds.depth.compare_op
                                                : VK_COMPARE_OP_ALWAYS);

      cfg.stencil_reference = state->ds.stencil.front.reference;
      cfg.line_width = agx_pack_line_width(state->rs.line.width);
      cfg.polygon_mode = AGX_POLYGON_MODE_FILL;
   }

   if (ppp_header.fragment_front_face_2)
      agx_ppp_fragment_face_2(&ppp, object_type, fs_info);

   ppp_push_if_dirty(fragment_front_stencil, FRAGMENT_STENCIL, cfg)
      ;

   ppp_push_if_dirty(fragment_back_face, FRAGMENT_FACE, cfg) {
      cfg.stencil_reference = state->ds.stencil.back.reference;
      cfg.line_width = agx_pack_line_width(state->rs.line.width);
      cfg.polygon_mode = AGX_POLYGON_MODE_FILL;
   }

   if (ppp_header.fragment_back_face_2)
      agx_ppp_fragment_face_2(&ppp, object_type, fs_info);

   ppp_push_if_dirty(fragment_back_stencil, FRAGMENT_STENCIL, cfg)
      ;

   ppp_push_if_dirty(depth_bias_scissor, DEPTH_BIAS_SCISSOR, cfg) {
      cfg.scissor = upload_scissor(cmd_buffer, state);
   }

   ppp_push_if_dirty(region_clip, REGION_CLIP, cfg) {
      /* TODO: Enable region clipping PERF */
      cfg.max_x = 8;
      cfg.max_y = 8;
      cfg.enable = false;
   }

   ppp_push_if_dirty(viewport, VIEWPORT, cfg) {
      assert(state->vp.viewport_count == 1 && "multiviewport todo");
      float x = state->vp.viewports[0].x;
      float y = state->vp.viewports[0].y;

      float half_width = 0.5f * state->vp.viewports[0].width;
      float half_height = 0.5f * state->vp.viewports[0].height;

      float n = state->vp.viewports[0].minDepth;
      float f = state->vp.viewports[0].maxDepth;

      cfg.translate_x = half_width + x;
      cfg.translate_y = half_height + y;
      cfg.translate_z = n;
      cfg.scale_x = half_width;
      cfg.scale_y = half_height;
      cfg.scale_z = f - n;
   }

   ppp_push_if_dirty(w_clamp, W_CLAMP, cfg)
      cfg.w_clamp = 0.000001f;

   ppp_push_if_dirty(output_select, OUTPUT_SELECT, cfg) {
      cfg.varyings = !!fs_info->varyings.fs.nr_bindings;
      cfg.point_size = vs_info->writes_psiz;
      cfg.frag_coord_z = fs_info->varyings.fs.reads_z;
   }

   ppp_push_if_dirty(varying_counts_32, VARYING_COUNTS, cfg) {
      cfg.smooth = vs_info->varyings.vs.num_32_smooth;
      cfg.flat = vs_info->varyings.vs.num_32_flat;
      cfg.linear = vs_info->varyings.vs.num_32_linear;
   }

   ppp_push_if_dirty(varying_counts_16, VARYING_COUNTS, cfg) {
      cfg.smooth = vs_info->varyings.vs.num_16_smooth;
      cfg.flat = vs_info->varyings.vs.num_16_flat;
      cfg.linear = vs_info->varyings.vs.num_16_linear;
   }

   ppp_push_if_dirty(cull, CULL, cfg) {
      cfg.cull_front = state->rs.cull_mode & VK_CULL_MODE_FRONT_BIT;
      cfg.cull_back = state->rs.cull_mode & VK_CULL_MODE_BACK_BIT;
      cfg.flat_shading_vertex = AGX_PPP_VERTEX_0;
      cfg.depth_clip = true;
      cfg.depth_clamp = false;
      cfg.front_face_ccw =
         state->rs.front_face == VK_FRONT_FACE_COUNTER_CLOCKWISE;
   }

   ppp_push_if_dirty(cull_2, CULL_2, cfg)
      ;

   ppp_push_if_dirty(fragment_shader, FRAGMENT_SHADER, cfg) {
      cfg.pipeline = agxv_emit_shader(cmd_buffer, MESA_SHADER_FRAGMENT);

      cfg.uniform_register_count = fs_info->push_count;
      cfg.preshader_register_count = fs_info->nr_preamble_gprs;

      cfg.texture_state_register_count = cmd_buffer->state.descriptors.ts_count;
      cfg.sampler_state_register_count = agx_translate_sampler_state_count(
         MAX2(cmd_buffer->state.descriptors.ss_count,
              fs_info->uses_txf ? fs_info->txf_sampler + 1 : 0),
         false);

      cfg.cf_binding_count = fs_info->varyings.fs.nr_bindings;
      if (cfg.cf_binding_count)
         cfg.cf_bindings = pipeline->cf_bindings->ptr.gpu;
   };

   ppp_push_if_dirty(occlusion_query, FRAGMENT_OCCLUSION_QUERY, cfg)
      ;

   ppp_push_if_dirty(occlusion_query_2, FRAGMENT_OCCLUSION_QUERY_2, cfg)
      ;

   ppp_push_if_dirty(output_unknown, OUTPUT_UNKNOWN, cfg)
      ;

   ppp_push_if_dirty(output_size, OUTPUT_SIZE, cfg)
      cfg.count = vs_info->varyings.vs.nr_index;

   ppp_push_if_dirty(varying_word_2, VARYING_2, cfg)
      ;

#undef ppp_push_if_dirty
   if (ppp.head > start_head)
      agx_ppp_fini(&buf, &ppp);

   cmd_buffer->state.encoder_current = buf;
}

static void
agxv_emit(struct agxv_cmd_buffer *cmd_buffer)
{
   if (cmd_buffer->state.dirty.vs) {
      struct agx_shader_info *info =
         &cmd_buffer->state.pipeline->stages[MESA_SHADER_VERTEX].info;
      uint8_t *buf = cmd_buffer->state.encoder_current;

      agxv_push(buf, VDM_STATE, cfg) {
         cfg.vertex_shader_word_0_present = true;
         cfg.vertex_shader_word_1_present = true;
         cfg.vertex_outputs_present = true;
         cfg.vertex_unknown_present = true;
      }

      agxv_push(buf, VDM_STATE_VERTEX_SHADER_WORD_0, cfg) {
         cfg.uniform_register_count = info->push_count;
         cfg.preshader_register_count = info->nr_preamble_gprs;

         cfg.texture_state_register_count =
            cmd_buffer->state.descriptors.ts_count;
         cfg.sampler_state_register_count = agx_translate_sampler_state_count(
            MAX2(cmd_buffer->state.descriptors.ss_count,
                 info->uses_txf ? info->txf_sampler + 1 : 0),
            false);
      };

      agxv_push(buf, VDM_STATE_VERTEX_SHADER_WORD_1, cfg) {
         cfg.pipeline = agxv_emit_shader(cmd_buffer, MESA_SHADER_VERTEX);
      }

      agxv_push(buf, VDM_STATE_VERTEX_OUTPUTS, cfg) {
         cfg.output_count_1 = info->varyings.vs.nr_index;
         cfg.output_count_2 = cfg.output_count_1;
      }

      agxv_push(buf, VDM_STATE_VERTEX_UNKNOWN, cfg) {
         cfg.flat_shading_control = AGX_VDM_VERTEX_0;
      }

      cmd_buffer->state.encoder_current = buf;

      memset(cmd_buffer->state.encoder_current, 0, 4);
      cmd_buffer->state.encoder_current += 4;
   }

   agxv_emit_ppp(cmd_buffer);

   cmd_buffer->state.dirty.vs = false;
   cmd_buffer->state.dirty.fs = false;
   cmd_buffer->state.dirty.pipeline = false;
   cmd_buffer->state.dirty.sets = false;
   cmd_buffer->state.dirty.buffers = false;
   cmd_buffer->state.dirty.push = false;
}

static const enum agx_primitive prim_map[] = {
   [VK_PRIMITIVE_TOPOLOGY_POINT_LIST] = AGX_PRIMITIVE_POINTS,
   [VK_PRIMITIVE_TOPOLOGY_LINE_LIST] = AGX_PRIMITIVE_LINES,
   [VK_PRIMITIVE_TOPOLOGY_LINE_STRIP] = AGX_PRIMITIVE_LINE_STRIP,
   [VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST] = AGX_PRIMITIVE_TRIANGLES,
   [VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP] = AGX_PRIMITIVE_TRIANGLE_STRIP,
   [VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN] = AGX_PRIMITIVE_TRIANGLE_FAN,
   [VK_PRIMITIVE_TOPOLOGY_META_RECT_LIST_MESA] = AGX_PRIMITIVE_TRIANGLES,
};

VKAPI_ATTR void VKAPI_CALL
agxv_CmdDraw(VkCommandBuffer commandBuffer, uint32_t vertexCount,
             uint32_t instanceCount, uint32_t firstVertex,
             uint32_t firstInstance)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);

   struct vk_dynamic_graphics_state *state =
      &cmd_buffer->vk.dynamic_graphics_state;

   if (!vertexCount)
      return;

   if (!instanceCount)
      return;

   /* FIXME: handle cmd_buffer resizing */
   agxv_emit(cmd_buffer);

   uint8_t *buf = cmd_buffer->state.encoder_current;

   agxv_push(buf, INDEX_LIST, cfg) {
      cfg.primitive = prim_map[state->ia.primitive_topology];
      cfg.instance_count_present = true;
      cfg.index_count_present = true;
      cfg.start_present = true;
   }

   agxv_push(buf, INDEX_LIST_COUNT, cfg)
      cfg.count = vertexCount;

   agxv_push(buf, INDEX_LIST_INSTANCES, cfg)
      cfg.count = instanceCount;

   agxv_push(buf, INDEX_LIST_START, cfg)
      cfg.start = firstVertex;

   cmd_buffer->state.encoder_current = buf;

   vk_dynamic_graphics_state_clear_dirty(
      &cmd_buffer->vk.dynamic_graphics_state);
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdDrawIndexed(VkCommandBuffer commandBuffer, uint32_t indexCount,
                    uint32_t instanceCount, uint32_t firstIndex,
                    int32_t vertexOffset, uint32_t firstInstance)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);

   struct vk_dynamic_graphics_state *state =
      &cmd_buffer->vk.dynamic_graphics_state;

   /* FIXME: handle cmd_buffer resizing */
   agxv_emit(cmd_buffer);

   uint8_t *buf = cmd_buffer->state.encoder_current;
   uint8_t index_size_B = cmd_buffer->state.index_size_B;
   uint64_t index_buffer =
      cmd_buffer->state.index_buffer + firstIndex * index_size_B;

   agxv_push(buf, VDM_STATE, cfg)
      cfg.restart_index_present = true;

   agxv_push(buf, VDM_STATE_RESTART_INDEX, cfg) {
      if (index_size_B == 4)
         cfg.value = 0xFFFFFFFF;
      else if (index_size_B == 2)
         cfg.value = 0xFFFF;
      else
         cfg.value = 0xFF;
   }

   agxv_push(buf, INDEX_LIST, cfg) {
      cfg.index_buffer_hi = index_buffer >> 32;
      cfg.primitive = prim_map[state->ia.primitive_topology];
      cfg.instance_count_present = true;
      cfg.index_count_present = true;
      cfg.start_present = true;
      cfg.index_buffer_present = true;
      cfg.index_buffer_size_present = true;
      cfg.index_size = agx_translate_index_size(index_size_B);
      cfg.restart_enable = state->ia.primitive_restart_enable;
   }

   agxv_push(buf, INDEX_LIST_BUFFER_LO, cfg)
      cfg.buffer_lo = (uint32_t)index_buffer;

   agxv_push(buf, INDEX_LIST_COUNT, cfg)
      cfg.count = indexCount;

   agxv_push(buf, INDEX_LIST_INSTANCES, cfg)
      cfg.count = instanceCount;

   agxv_push(buf, INDEX_LIST_START, cfg)
      cfg.start = vertexOffset;

   agxv_push(buf, INDEX_LIST_BUFFER_SIZE, cfg)
      cfg.size = ALIGN_POT(indexCount * index_size_B, 4);

   cmd_buffer->state.encoder_current = buf;

   vk_dynamic_graphics_state_clear_dirty(
      &cmd_buffer->vk.dynamic_graphics_state);
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdDrawIndirect(VkCommandBuffer commandBuffer, VkBuffer buffer,
                     VkDeviceSize offset, uint32_t drawCount, uint32_t stride)
{
   unreachable("stub");
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdDrawIndexedIndirect(VkCommandBuffer commandBuffer, VkBuffer buffer,
                            VkDeviceSize offset, uint32_t drawCount,
                            uint32_t stride)
{
   unreachable("stub");
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdEndRendering(VkCommandBuffer commandBuffer)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   uint8_t *buf = cmd_buffer->state.encoder_current;
   agxv_push(buf, VDM_STREAM_TERMINATE, cfg)
      ;

   /* Upload scissor/depth bias arrays now that we're done emitting into them */
   uint64_t scissor = agx_pool_upload_aligned(
      &cmd_buffer->cmd_pool, cmd_buffer->state.scissor.data,
      cmd_buffer->state.scissor.size, 64);

   uint64_t zbias = agx_pool_upload_aligned(
      &cmd_buffer->cmd_pool, cmd_buffer->state.depth_bias.data,
      cmd_buffer->state.depth_bias.size, 64);

   cmd_buffer->state.job->render_buf.scissor_array = scissor;
   cmd_buffer->state.job->render_buf.depth_bias_array = zbias;

   if (cmd_buffer->device->pdev->dev.debug & AGX_DBG_TRACE) {
      char buf[128];
      snprintf(buf, sizeof(buf), "agx.%s", __func__);
      setenv("AGXDECODE_DUMP_FILE", buf, true);
      agxdecode_drm_cmd_render(&cmd_buffer->device->pdev->dev.params,
                               &cmd_buffer->state.job->render_buf, true);
      agxdecode_next_frame();
   }
}
