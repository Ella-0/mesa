/*
 * Copyright 2023 Valve Corporation
 * Copyright 2023 Ella Stanforth
 *
 * based in part on other drivers that are:
 * Copyright 2016 Red Hat.
 * Copyright 2016 Bas Nieuwenhuizen
 * Copyright 2015 Intel Corporation
 *
 * SPDX-License-Identifier: MIT
 */

#include "agxv_image.h"

#include "agxv_device.h"
#include "agxv_device_memory.h"

#include "vk_format.h"

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateImage(VkDevice _device, const VkImageCreateInfo *pCreateInfo,
                 const VkAllocationCallbacks *pAllocator, VkImage *pImage)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   struct agxv_image *image;

   image =
      vk_image_create(&device->vk, pCreateInfo, pAllocator, sizeof(*image));

   if (!image)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   image->layout.sample_count_sa = 1;
   image->layout.width_px = image->vk.extent.width;
   image->layout.height_px = image->vk.extent.height;

   if (image->vk.array_layers > 1)
      image->layout.depth_px = image->vk.array_layers;
   else
      image->layout.depth_px = image->vk.extent.depth;

   image->layout.format = vk_format_to_pipe_format(image->vk.format);
   image->layout.levels = image->vk.mip_levels;
   image->layout.tiling = image->vk.tiling == VK_IMAGE_TILING_OPTIMAL
                             ? AIL_TILING_TWIDDLED
                             : AIL_TILING_LINEAR;

   ail_make_miptree(&image->layout);

   *pImage = agxv_image_to_handle(image);

   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_GetImageMemoryRequirements2(VkDevice _device,
                                 const VkImageMemoryRequirementsInfo2 *pInfo,
                                 VkMemoryRequirements2 *pMemoryRequirements)
{
   VK_FROM_HANDLE(agxv_image, image, pInfo->image);
   pMemoryRequirements->memoryRequirements = (VkMemoryRequirements){
      .size = image->layout.size_B,
      .memoryTypeBits = 1,
      .alignment = 0x4000, /* FIXME: using page size for now */
   };

   vk_foreach_struct_const(ext, pInfo->pNext) {
      agxv_debug_ignored_stype(ext->sType);
   }
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_BindImageMemory2(VkDevice _device, uint32_t bindInfoCount,
                      const VkBindImageMemoryInfo *pBindInfos)
{
   VK_FROM_HANDLE(agxv_device, device, _device);

   for (uint32_t i = 0; i < bindInfoCount; ++i) {
      VK_FROM_HANDLE(agxv_device_memory, mem, pBindInfos[i].memory);
      VK_FROM_HANDLE(agxv_image, image, pBindInfos[i].image);

      /* Ignore this struct on Android, we cannot access swapchain structures
       * there. */
#ifdef AGXV_USE_WSI_PLATFORM
      const VkBindImageMemorySwapchainInfoKHR *swapchain_info =
         vk_find_struct_const(pBindInfos[i].pNext,
                              BIND_IMAGE_MEMORY_SWAPCHAIN_INFO_KHR);

      if (swapchain_info && swapchain_info->swapchain != VK_NULL_HANDLE) {
         struct agxv_image *swapchain_img =
            agxv_image_from_handle(wsi_common_get_image(
               swapchain_info->swapchain, swapchain_info->imageIndex));

         image->bindings[0].bo = swapchain_img->bindings[0].bo;
         image->bindings[0].offset = swapchain_img->bindings[0].offset;
         continue;
      }
#endif

      if (mem->bo->size) {
         VkImageMemoryRequirementsInfo2 info = {
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2,
            .image = pBindInfos[i].image,
         };
         VkMemoryRequirements2 reqs = {
            .sType = VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2,
         };

         agxv_GetImageMemoryRequirements2(_device, &info, &reqs);

         if (pBindInfos[i].memoryOffset + reqs.memoryRequirements.size >
             mem->bo->size) {
            return vk_errorf(device, VK_ERROR_UNKNOWN,
                             "Device memory object too small for the image.\n");
         }
      }

      if (image->disjoint) {
         const VkBindImagePlaneMemoryInfo *plane_info = vk_find_struct_const(
            pBindInfos[i].pNext, BIND_IMAGE_PLANE_MEMORY_INFO);

         switch (plane_info->planeAspect) {
         case VK_IMAGE_ASPECT_PLANE_0_BIT:
            image->bindings[0].bo = mem->bo;
            image->bindings[0].offset = pBindInfos[i].memoryOffset;
            break;
         case VK_IMAGE_ASPECT_PLANE_1_BIT:
            image->bindings[1].bo = mem->bo;
            image->bindings[1].offset = pBindInfos[i].memoryOffset;
            break;
         case VK_IMAGE_ASPECT_PLANE_2_BIT:
            image->bindings[2].bo = mem->bo;
            image->bindings[2].offset = pBindInfos[i].memoryOffset;
            break;
         default:
            break;
         }
      } else {
         image->bindings[0].bo = mem->bo;
         image->bindings[0].offset = pBindInfos[i].memoryOffset;
      }
   }
   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_DestroyImage(VkDevice _device, VkImage _image,
                  const VkAllocationCallbacks *pAllocator)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_image, image, _image);

   if (!image)
      return;

   vk_free2(&device->vk.alloc, pAllocator, image);
}

VKAPI_ATTR void VKAPI_CALL
agxv_GetImageSubresourceLayout(VkDevice device, VkImage _image,
                               const VkImageSubresource *pSubresource,
                               VkSubresourceLayout *pLayout)
{
   VK_FROM_HANDLE(agxv_image, image, _image);

   *pLayout = (VkSubresourceLayout){
      .offset = ail_get_layer_level_B(&image->layout, pSubresource->arrayLayer,
                                      pSubresource->mipLevel),
      .size = image->layout.layer_stride_B,
      .rowPitch = ail_get_wsi_stride_B(&image->layout, pSubresource->mipLevel),
      .arrayPitch = image->layout.layer_stride_B,
      .depthPitch = image->layout.layer_stride_B,
   };
}
