/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_event.h"

#include "agxv_device.h"

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateEvent(VkDevice _device, const VkEventCreateInfo *pCreateInfo,
                 const VkAllocationCallbacks *pAllocator, VkEvent *pEvent)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   struct agxv_event *event;

   event = vk_object_zalloc(&device->vk, pAllocator, sizeof(*event),
                            VK_OBJECT_TYPE_EVENT);
   if (!event)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   *pEvent = agxv_event_to_handle(event);
   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_DestroyEvent(VkDevice _device, VkEvent _event,
                  const VkAllocationCallbacks *pAllocator)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_event, event, _event);

   if (!event)
      return;

   vk_object_free(&device->vk, pAllocator, event);
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_GetEventStatus(VkDevice device, VkEvent _event)
{
   // VK_FROM_HANDLE(agxv_event, event, _event);

   unreachable("stub");
   return VK_EVENT_SET;
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_SetEvent(VkDevice device, VkEvent _event)
{
   VK_FROM_HANDLE(agxv_event, event, _event);

   unreachable("stub");
   return VK_SUCCESS;
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_ResetEvent(VkDevice device, VkEvent _event)
{
   VK_FROM_HANDLE(agxv_event, event, _event);
   unreachable("stub");

   return VK_SUCCESS;
}
