/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#ifndef AGXV_EVENT
#define AGXV_EVENT 1

#include "agxv_private.h"

#include "vulkan/runtime/vk_object.h"

struct agxv_event {
   struct vk_object_base base;
};

VK_DEFINE_NONDISP_HANDLE_CASTS(agxv_event, base, VkEvent, VK_OBJECT_TYPE_EVENT)

#endif
