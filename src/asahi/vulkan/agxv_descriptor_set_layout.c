/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_descriptor_set_layout.h"
#include "vulkan/vulkan_core.h"
#include "agxv_descriptor_set.h"

#include "agxv_device.h"

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateDescriptorSetLayout(
   VkDevice _device, const VkDescriptorSetLayoutCreateInfo *pCreateInfo,
   const VkAllocationCallbacks *pAllocator, VkDescriptorSetLayout *pSetLayout)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   uint32_t num_bindings = 0;
   for (uint32_t i = 0; i < pCreateInfo->bindingCount; i++) {
      const VkDescriptorSetLayoutBinding *binding = &pCreateInfo->pBindings[i];
      num_bindings = MAX2(num_bindings, binding->binding + 1);
   }

   VK_MULTIALLOC(ma);
   VK_MULTIALLOC_DECL(&ma, struct agxv_descriptor_set_layout, layout, 1);
   VK_MULTIALLOC_DECL(&ma, struct agxv_descriptor_set_binding_layout,
                      __bindings, num_bindings);

   if (!vk_descriptor_set_layout_multizalloc(&device->vk, &ma))
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   layout->binding_count = num_bindings;

   for (uint32_t i = 0; i < pCreateInfo->bindingCount; i++) {
      const VkDescriptorSetLayoutBinding *binding = &pCreateInfo->pBindings[i];
      layout->binding[binding->binding].type = binding->descriptorType;
      layout->binding[binding->binding].array_size = binding->descriptorCount;
   }

   uint32_t offset = 0;
   uint8_t texture_index = 0;
   uint8_t sampler_index = 0;
   for (uint32_t i = 0; i < layout->binding_count; i++) {
      /* skip empty bindings */
      if (layout->binding[i].array_size == 0)
         continue;

      uint32_t stride = agxv_binding_stride(layout->binding[i].type);
      layout->binding[i].offset = offset;
      layout->binding[i].stride = stride;
      offset += stride * layout->binding[i].array_size;

      switch (layout->binding[i].type) {
      case VK_DESCRIPTOR_TYPE_SAMPLER:
      case VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER:
         layout->binding[i].sampler_index = sampler_index;
         sampler_index += layout->binding[i].array_size;
         break;
      default:
         break;
      }

      switch (layout->binding[i].type) {
      case VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE:
      case VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER:
      case VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER:
         layout->binding[i].texture_index = texture_index;
         texture_index += layout->binding[i].array_size;
         break;

      /* Writeable images get both a texture and a PBE descriptor */
      case VK_DESCRIPTOR_TYPE_STORAGE_IMAGE:
      case VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER:
         layout->binding[i].texture_index = texture_index;
         texture_index += layout->binding[i].array_size * 2;
         break;

      default:
         break;
      }
   };

   layout->sampler_count = sampler_index;
   layout->texture_count = texture_index;

   layout->size = offset;
   *pSetLayout = agxv_descriptor_set_layout_to_handle(layout);

   return VK_SUCCESS;
}
