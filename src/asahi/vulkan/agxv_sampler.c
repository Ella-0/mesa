/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_sampler.h"
#include "vulkan/vulkan_core.h"

#include "agx_bo.h"
#include "agx_pack.h"
#include "agxv_device.h"
#include "agxv_physical_device.h"

static enum agx_mip_filter
agx_mip_filter_from_vk(VkSamplerMipmapMode filter)
{
   enum agx_mip_filter map[] = {
      [VK_SAMPLER_MIPMAP_MODE_NEAREST] = AGX_MIP_FILTER_NEAREST,
      [VK_SAMPLER_MIPMAP_MODE_LINEAR] = AGX_MIP_FILTER_LINEAR,
   };

   assert(filter < ARRAY_SIZE(map));
   return map[filter];
}

static enum agx_wrap
agx_wrap_from_vk(VkSamplerAddressMode wrap)
{
   enum agx_wrap map[] = {
      [VK_SAMPLER_ADDRESS_MODE_REPEAT] = AGX_WRAP_REPEAT,
      [VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT] = AGX_WRAP_MIRRORED_REPEAT,
      [VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE] = AGX_WRAP_CLAMP_TO_EDGE,
      [VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER] = AGX_WRAP_CLAMP_TO_BORDER,
      [VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE] =
         AGX_WRAP_MIRRORED_CLAMP_TO_EDGE,
   };

   assert(wrap < ARRAY_SIZE(map));
   return map[wrap];
}

static enum agx_compare_func
agx_compare_func_from_vk(VkCompareOp op)
{
   enum agx_compare_func map[] = {
      [VK_COMPARE_OP_NEVER] = AGX_COMPARE_FUNC_NEVER,
      [VK_COMPARE_OP_LESS] = AGX_COMPARE_FUNC_LESS,
      [VK_COMPARE_OP_EQUAL] = AGX_COMPARE_FUNC_EQUAL,
      [VK_COMPARE_OP_LESS_OR_EQUAL] = AGX_COMPARE_FUNC_LEQUAL,
      [VK_COMPARE_OP_GREATER] = AGX_COMPARE_FUNC_GREATER,
      [VK_COMPARE_OP_NOT_EQUAL] = AGX_COMPARE_FUNC_NOT_EQUAL,
      [VK_COMPARE_OP_GREATER_OR_EQUAL] = AGX_COMPARE_FUNC_GEQUAL,
      [VK_COMPARE_OP_ALWAYS] = AGX_COMPARE_FUNC_ALWAYS,
   };

   assert(op < ARRAY_SIZE(map));
   return map[op];
}

static enum agx_border_colour
agx_border_colour_from_vk(VkBorderColor colour)
{
   enum agx_border_colour map[] = {
      [VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK] =
         AGX_BORDER_COLOUR_TRANSPARENT_BLACK,
      [VK_BORDER_COLOR_INT_TRANSPARENT_BLACK] =
         AGX_BORDER_COLOUR_TRANSPARENT_BLACK,
      [VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK] = AGX_BORDER_COLOUR_OPAQUE_BLACK,
      [VK_BORDER_COLOR_INT_OPAQUE_BLACK] = AGX_BORDER_COLOUR_OPAQUE_BLACK,
      [VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE] = AGX_BORDER_COLOUR_OPAQUE_WHITE,
      [VK_BORDER_COLOR_INT_OPAQUE_WHITE] = AGX_BORDER_COLOUR_OPAQUE_WHITE,

      /* TODO: VK_EXT_custom_border_colour */
   };

   assert(colour < ARRAY_SIZE(map));
   return map[colour];
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateSampler(VkDevice _device, const VkSamplerCreateInfo *pCreateInfo,
                   const VkAllocationCallbacks *pAllocator, VkSampler *pSampler)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   struct agxv_sampler *sampler;
   VkResult result;

   sampler = vk_object_zalloc(&device->vk, pAllocator, sizeof(*sampler),
                              VK_OBJECT_TYPE_SAMPLER);
   if (!sampler)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   agx_pack(&sampler->desc, SAMPLER, cfg) {
      cfg.minimum_lod = pCreateInfo->minLod;
      cfg.maximum_lod = pCreateInfo->maxLod;

      if (pCreateInfo->anisotropyEnable) {
         cfg.maximum_anisotropy = util_next_power_of_two(
            MAX2((uint32_t)pCreateInfo->maxAnisotropy, 1));
      }

      cfg.magnify_linear = pCreateInfo->magFilter == VK_FILTER_LINEAR;
      cfg.minify_linear = pCreateInfo->minFilter == VK_FILTER_LINEAR;
      cfg.mip_filter = agx_mip_filter_from_vk(pCreateInfo->mipmapMode);

      cfg.wrap_s = agx_wrap_from_vk(pCreateInfo->addressModeU);
      cfg.wrap_t = agx_wrap_from_vk(pCreateInfo->addressModeV);
      cfg.wrap_r = agx_wrap_from_vk(pCreateInfo->addressModeW);

      cfg.pixel_coordinates = pCreateInfo->unnormalizedCoordinates;

      if (pCreateInfo->compareEnable) {
         cfg.compare_enable = true;
         cfg.compare_func = agx_compare_func_from_vk(pCreateInfo->compareOp);
      } else {
         cfg.compare_enable = false;
      }

      cfg.border_colour = agx_border_colour_from_vk(pCreateInfo->borderColor);
      cfg.seamful_cube_maps = !!(
         pCreateInfo->flags & VK_SAMPLER_CREATE_NON_SEAMLESS_CUBE_MAP_BIT_EXT);
   }

   *pSampler = agxv_sampler_to_handle(sampler);

   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_DestroySampler(VkDevice _device, VkSampler _sampler,
                    const VkAllocationCallbacks *pAllocator)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_sampler, sampler, _sampler);

   if (!sampler)
      return;

   vk_object_free(&device->vk, pAllocator, sampler);
}
