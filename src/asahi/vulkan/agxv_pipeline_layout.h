/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#ifndef AGXV_PIPELINE_LAYOUT
#define AGXV_PIPELINE_LAYOUT 1

#include "agxv_private.h"

#include "vulkan/runtime/vk_pipeline_layout.h"

struct agxv_pipeline_layout {
   struct vk_pipeline_layout vk;

   /* Size of push constants in bytes */
   uint32_t push_size;

   /* Total number of samplers and textures across all set layouts */
   uint32_t texture_count;
   uint32_t sampler_count;

   /* Base index of textures and samplers for each set */
   uint32_t texture_map[VK_MESA_PIPELINE_LAYOUT_MAX_SETS];
   uint32_t sampler_map[VK_MESA_PIPELINE_LAYOUT_MAX_SETS];
};

VK_DEFINE_NONDISP_HANDLE_CASTS(agxv_pipeline_layout, vk.base, VkPipelineLayout,
                               VK_OBJECT_TYPE_PIPELINE_LAYOUT);

#endif
