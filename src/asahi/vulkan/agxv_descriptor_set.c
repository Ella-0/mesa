/*
 * Copyright 2023 Valve Corporation
 * Copyright 2023 Ella Stanforth
 * Copyright 2023 Collabora Ltd
 * SPDX-License-Identifier: MIT
 */

#include "agxv_descriptor_set.h"

#include "asahi/lib/agx_bo.h"
#include "asahi/lib/agx_device.h"
#include "agxv_buffer.h"
#include "agxv_descriptor_pool.h"
#include "agxv_descriptor_set_layout.h"
#include "agxv_device.h"
#include "agxv_device_memory.h"
#include "agxv_image_view.h"
#include "agxv_physical_device.h"
#include "agxv_sampler.h"

static VkResult
agxv_descriptor_set_create(struct agxv_device *device,
                           struct agxv_descriptor_pool *pool,
                           struct agxv_descriptor_set_layout *layout,
                           VkDescriptorSet *pDescriptorSet)
{
   uint64_t offset = pool->current_offset;

   if (offset + layout->size > pool->size)
      return vk_error(pool, VK_ERROR_OUT_OF_POOL_MEMORY);

   struct agxv_descriptor_set *set;

   set = vk_object_zalloc(&device->vk, NULL, sizeof(*set),
                          VK_OBJECT_TYPE_DESCRIPTOR_SET);
   if (!set)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   vk_descriptor_set_layout_ref(&layout->vk);

   if (pool->entry_count == pool->max_entry_count) {
      vk_object_free(&device->vk, NULL, set);
      return vk_error(pool, VK_ERROR_OUT_OF_POOL_MEMORY);
   }

   set->bo_offset = pool->current_offset;
   set->layout = layout;

   if (layout->size > 0) {
      set->bo = pool->bo;
      set->bo_size = pool->bo->size;
      set->mapped_ptr = set->bo->ptr.cpu + pool->current_offset;
   } else {
      set->bo = NULL;
      set->bo_size = 0;
      set->mapped_ptr = NULL;
   }

   pool->entries[pool->entry_count].offset = offset;
   pool->entries[pool->entry_count].size = layout->size;
   pool->entries[pool->entry_count].set = set;
   pool->entry_count++;

   pool->current_offset += layout->size;

   *pDescriptorSet = agxv_descriptor_set_to_handle(set);

   return VK_SUCCESS;
}

static void
agxv_descriptor_set_destroy(struct agxv_device *device,
                            struct agxv_descriptor_pool *pool,
                            struct agxv_descriptor_set *set, bool free_bo)
{
   if (free_bo) {
      for (int i = 0; i < pool->entry_count; ++i) {
         if (pool->entries[i].set == set) {
            memmove(&pool->entries[i], &pool->entries[i + 1],
                    sizeof(pool->entries[i]) * (pool->entry_count - i - 1));
            --pool->entry_count;
            break;
         }
      }
   }

   vk_descriptor_set_layout_unref(&device->vk, &set->layout->vk);

   vk_object_free(&device->vk, NULL, set);
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_AllocateDescriptorSets(VkDevice _device,
                            const VkDescriptorSetAllocateInfo *pAllocateInfo,
                            VkDescriptorSet *pDescriptorSets)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_descriptor_pool, pool, pAllocateInfo->descriptorPool);

   VkResult result = VK_SUCCESS;

   uint32_t i;
   for (i = 0; i < pAllocateInfo->descriptorSetCount; i++) {
      VK_FROM_HANDLE(agxv_descriptor_set_layout, layout,
                     pAllocateInfo->pSetLayouts[i]);
      result =
         agxv_descriptor_set_create(device, pool, layout, &pDescriptorSets[i]);
      if (result != VK_SUCCESS)
         break;
   }

   if (result != VK_SUCCESS) {
      agxv_FreeDescriptorSets(_device, pAllocateInfo->descriptorPool, i,
                              pDescriptorSets);
      for (i = 0; i < pAllocateInfo->descriptorSetCount; i++)
         pDescriptorSets[i] = VK_NULL_HANDLE;
   }

   return result;
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_FreeDescriptorSets(VkDevice _device, VkDescriptorPool descriptorPool,
                        uint32_t descriptorSetCount,
                        const VkDescriptorSet *pDescriptorSets)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_descriptor_pool, pool, descriptorPool);

   for (uint32_t i = 0; i < descriptorSetCount; i++) {
      VK_FROM_HANDLE(agxv_descriptor_set, set, pDescriptorSets[i]);

      if (set)
         agxv_descriptor_set_destroy(device, pool, set, true);
   }

   return VK_SUCCESS;
}

static inline void *
desc_ubo_data(struct agxv_descriptor_set *set, uint32_t binding, uint32_t elem,
              uint32_t *size_out)
{
   const struct agxv_descriptor_set_binding_layout *binding_layout =
      &set->layout->binding[binding];

   uint32_t offset = binding_layout->offset + elem * binding_layout->stride;
   assert(offset < set->bo_size);

   if (size_out != NULL)
      *size_out = set->bo_size - offset;

   return (char *)set->mapped_ptr + offset;
}

static void
write_desc(struct agxv_descriptor_set *set, uint32_t binding, uint32_t elem,
           const void *desc_data, size_t desc_size)
{
   ASSERTED uint32_t dst_size;
   void *dst = desc_ubo_data(set, binding, elem, &dst_size);
   assert(desc_size <= dst_size);
   memcpy(dst, desc_data, desc_size);
}

static void
write_buffer_desc(struct agxv_descriptor_set *set,
                  const VkDescriptorBufferInfo *pBufferInfo, uint32_t binding,
                  uint32_t elem)
{
   VK_FROM_HANDLE(agxv_buffer, buffer, pBufferInfo->buffer);

   const struct agxv_buffer_descriptor desc = {
      .ptr = agxv_buffer_addr(buffer) + pBufferInfo->offset,
      .size =
         agxv_buffer_range(buffer, pBufferInfo->offset, pBufferInfo->range),
   };
   write_desc(set, binding, elem, &desc, sizeof(desc));
}

static void
write_dynamic_buffer_desc(struct agxv_descriptor_set *set,
                          const VkDescriptorBufferInfo *const info,
                          uint32_t binding, uint32_t elem)
{
   unreachable("todo");

#if 0
   VK_FROM_HANDLE(agxv_buffer, buffer, info->buffer);

   const struct agxv_descriptor_set_binding_layout *binding_layout =
      &set->layout->binding[binding];

   const struct agxv_addr_range addr_range =
      agxv_buffer_addr_range(buffer, info->offset, info->range);
   assert(addr_range.range <= UINT32_MAX);

   struct agxv_buffer_address *desc =
      &set->dynamic_buffers[binding_layout->dynamic_buffer_index + elem];
   *desc = (struct agxv_buffer_address){
      .base_addr = addr_range.addr,
      .size = addr_range.range,
   };
#endif
}

static void
write_inline_uniform_data(struct agxv_descriptor_set *set,
                          const VkWriteDescriptorSetInlineUniformBlock *info,
                          uint32_t binding, uint32_t offset)
{
   assert(set->layout->binding[binding].stride == 1);
   write_desc(set, binding, offset, info->pData, info->dataSize);
}

static void
write_sampler_desc(struct agxv_descriptor_set *set,
                   const VkDescriptorImageInfo *const info, uint32_t binding,
                   uint32_t elem, struct agx_sampler_packed *set_samplers)
{
   VK_FROM_HANDLE(agxv_sampler, sampler, info->sampler);

   uint8_t sampler_index = set->layout->binding[binding].sampler_index + elem;
   assert(sampler_index < ARRAY_SIZE(set->samplers));

   STATIC_ASSERT(sizeof(sampler->desc) == sizeof(set_samplers[sampler_index]));
   memcpy(&set_samplers[sampler_index], &sampler->desc, sizeof(sampler->desc));
}

static void
write_raw_texture_desc(struct agxv_descriptor_set *set,
                       struct agx_texture_packed *raw, uint32_t binding,
                       uint32_t elem, struct agx_texture_packed *set_textures)
{
   uint8_t texture_index = set->layout->binding[binding].texture_index + elem;
   assert(texture_index < ARRAY_SIZE(set->textures));

   STATIC_ASSERT(sizeof(*raw) == sizeof(set_textures[texture_index]));
   memcpy(&set_textures[texture_index], raw, sizeof(*raw));
}

static void
write_texture_desc(struct agxv_descriptor_set *set,
                   const VkDescriptorImageInfo *const info, uint32_t binding,
                   uint32_t elem, struct agx_texture_packed *set_textures)
{
   VK_FROM_HANDLE(agxv_image_view, texture, info->imageView);

   write_raw_texture_desc(set, &texture->texture_desc, binding, elem,
                          set_textures);
}

static void
write_buffer_view_desc(struct agxv_descriptor_set *set,
                       const VkBufferView bufferView, uint32_t binding,
                       uint32_t elem, struct agx_texture_packed *set_textures)
{
   VK_FROM_HANDLE(agxv_buffer_view, buffer_view, bufferView);

   write_raw_texture_desc(set, &buffer_view->texture_desc, binding, elem,
                          set_textures);
}

static void
write_buffer_view_and_pbe_desc(struct agxv_descriptor_set *set,
                               const VkBufferView bufferView, uint32_t binding,
                               uint32_t elem,
                               struct agx_texture_packed *set_textures)
{
   VK_FROM_HANDLE(agxv_buffer_view, buffer_view, bufferView);

   write_buffer_view_desc(set, bufferView, binding, elem, set_textures);

   write_raw_texture_desc(set,
                          (struct agx_texture_packed *)&buffer_view->pbe_desc,
                          binding, elem + 1, set_textures);
}

static void
write_texture_and_pbe_desc(struct agxv_descriptor_set *set,
                           const VkDescriptorImageInfo *const info,
                           uint32_t binding, uint32_t elem,
                           struct agx_texture_packed *set_textures)
{
   VK_FROM_HANDLE(agxv_image_view, texture, info->imageView);

   write_texture_desc(set, info, binding, elem, set_textures);

   write_raw_texture_desc(set, (struct agx_texture_packed *)&texture->pbe_desc,
                          binding, elem + 1, set_textures);
}

VKAPI_ATTR void VKAPI_CALL
agxv_UpdateDescriptorSets(VkDevice device, uint32_t descriptorWriteCount,
                          const VkWriteDescriptorSet *pDescriptorWrites,
                          uint32_t descriptorCopyCount,
                          const VkCopyDescriptorSet *pDescriptorCopies)
{
   for (uint32_t w = 0; w < descriptorWriteCount; w++) {
      const VkWriteDescriptorSet *write = &pDescriptorWrites[w];
      VK_FROM_HANDLE(agxv_descriptor_set, set, write->dstSet);

      switch (write->descriptorType) {
      case VK_DESCRIPTOR_TYPE_SAMPLER:
         for (uint32_t j = 0; j < write->descriptorCount; j++) {
            write_sampler_desc(set, write->pImageInfo + j, write->dstBinding,
                               write->dstArrayElement + j, set->samplers);
         }
         break;

      case VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE:
      case VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT:
         for (uint32_t j = 0; j < write->descriptorCount; j++) {
            write_texture_desc(set, write->pImageInfo + j, write->dstBinding,
                               write->dstArrayElement + j, set->textures);
         }
         break;

      case VK_DESCRIPTOR_TYPE_STORAGE_IMAGE:
         for (uint32_t j = 0; j < write->descriptorCount; j++) {
            write_texture_and_pbe_desc(
               set, write->pImageInfo + j, write->dstBinding,
               write->dstArrayElement + j, set->textures);
         }
         break;

      case VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER:
         for (uint32_t j = 0; j < write->descriptorCount; j++) {
            write_sampler_desc(set, write->pImageInfo + j, write->dstBinding,
                               write->dstArrayElement + j, set->samplers);

            write_texture_desc(set, write->pImageInfo + j, write->dstBinding,
                               write->dstArrayElement + j, set->textures);
         }
         break;

      case VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER:
         for (uint32_t j = 0; j < write->descriptorCount; j++) {
            write_buffer_view_desc(set, write->pTexelBufferView[j],
                                   write->dstBinding,
                                   write->dstArrayElement + j, set->textures);
         }
         break;

      case VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER:
         for (uint32_t j = 0; j < write->descriptorCount; j++) {
            write_buffer_view_and_pbe_desc(
               set, write->pTexelBufferView[j], write->dstBinding,
               write->dstArrayElement + j, set->textures);
         }
         break;

      case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
      case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER:
         for (uint32_t j = 0; j < write->descriptorCount; j++) {
            write_buffer_desc(set, write->pBufferInfo + j, write->dstBinding,
                              write->dstArrayElement + j);
         }
         break;

      case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC:
      case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC:
         for (uint32_t j = 0; j < write->descriptorCount; j++) {
            write_dynamic_buffer_desc(set, write->pBufferInfo + j,
                                      write->dstBinding,
                                      write->dstArrayElement + j);
         }
         break;

      case VK_DESCRIPTOR_TYPE_INLINE_UNIFORM_BLOCK: {
         const VkWriteDescriptorSetInlineUniformBlock *write_inline =
            vk_find_struct_const(write->pNext,
                                 WRITE_DESCRIPTOR_SET_INLINE_UNIFORM_BLOCK);
         assert(write_inline->dataSize == write->descriptorCount);
         write_inline_uniform_data(set, write_inline, write->dstBinding,
                                   write->dstArrayElement);
         break;
      }

      default:
         break;
      }
   }

   for (uint32_t i = 0; i < descriptorCopyCount; i++) {
      const VkCopyDescriptorSet *copy = &pDescriptorCopies[i];
      VK_FROM_HANDLE(agxv_descriptor_set, src, copy->srcSet);
      VK_FROM_HANDLE(agxv_descriptor_set, dst, copy->dstSet);

      const struct agxv_descriptor_set_binding_layout *src_binding_layout =
         &src->layout->binding[copy->srcBinding];
      const struct agxv_descriptor_set_binding_layout *dst_binding_layout =
         &dst->layout->binding[copy->dstBinding];

      if (dst_binding_layout->stride > 0 && src_binding_layout->stride > 0) {
         for (uint32_t j = 0; j < copy->descriptorCount; j++) {
            ASSERTED uint32_t dst_max_size, src_max_size;
            void *dst_map = desc_ubo_data(
               dst, copy->dstBinding, copy->dstArrayElement + j, &dst_max_size);
            const void *src_map = desc_ubo_data(
               src, copy->srcBinding, copy->srcArrayElement + j, &src_max_size);
            const uint32_t copy_size =
               MIN2(dst_binding_layout->stride, src_binding_layout->stride);
            assert(copy_size <= dst_max_size && copy_size <= src_max_size);
            memcpy(dst_map, src_map, copy_size);
         }
      }

      switch (src_binding_layout->type) {
      case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC:
      case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC: {
#if 0
         const uint32_t dst_dyn_start =
            dst_binding_layout->dynamic_buffer_index + copy->dstArrayElement;
         const uint32_t src_dyn_start =
            src_binding_layout->dynamic_buffer_index + copy->srcArrayElement;
         typed_memcpy(&dst->dynamic_buffers[dst_dyn_start],
                      &src->dynamic_buffers[src_dyn_start],
                      copy->descriptorCount);
#endif
         unreachable("todo");
         break;
      }
      default:
         break;
      }
   }
}

void
agxv_push_descriptor_set_update(struct agxv_push_descriptor_set *push_set,
                                struct agxv_descriptor_set_layout *layout,
                                uint32_t write_count,
                                const VkWriteDescriptorSet *writes)
{
   struct agxv_descriptor_set set = {
      .layout = layout,
      .bo_size = sizeof(push_set->data),
      .mapped_ptr = push_set->data,
   };

   for (uint32_t w = 0; w < write_count; w++) {
      const VkWriteDescriptorSet *write = &writes[w];
      assert(write->dstSet == VK_NULL_HANDLE);

      switch (write->descriptorType) {
      case VK_DESCRIPTOR_TYPE_SAMPLER:
         for (uint32_t j = 0; j < write->descriptorCount; j++) {
            write_sampler_desc(&set, write->pImageInfo + j, write->dstBinding,
                               write->dstArrayElement + j, push_set->samplers);
         }
         break;

      case VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE:
      case VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT:
         for (uint32_t j = 0; j < write->descriptorCount; j++) {
            write_texture_desc(&set, write->pImageInfo + j, write->dstBinding,
                               write->dstArrayElement + j, push_set->textures);
         }
         break;

      case VK_DESCRIPTOR_TYPE_STORAGE_IMAGE:
         for (uint32_t j = 0; j < write->descriptorCount; j++) {
            write_texture_and_pbe_desc(
               &set, write->pImageInfo + j, write->dstBinding,
               write->dstArrayElement + j, push_set->textures);
         }
         break;

      case VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER:
         for (uint32_t j = 0; j < write->descriptorCount; j++) {
            write_sampler_desc(&set, write->pImageInfo + j, write->dstBinding,
                               write->dstArrayElement + j, push_set->samplers);

            write_texture_desc(&set, write->pImageInfo + j, write->dstBinding,
                               write->dstArrayElement + j, push_set->textures);
         }
         break;

      case VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER:
         for (uint32_t j = 0; j < write->descriptorCount; j++) {
            write_buffer_view_desc(
               &set, write->pTexelBufferView[j], write->dstBinding,
               write->dstArrayElement + j, push_set->textures);
         }
         break;

      case VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER:
         for (uint32_t j = 0; j < write->descriptorCount; j++) {
            write_buffer_view_and_pbe_desc(
               &set, write->pTexelBufferView[j], write->dstBinding,
               write->dstArrayElement + j, push_set->textures);
         }
         break;

      case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
      case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER:
         for (uint32_t j = 0; j < write->descriptorCount; j++) {
            write_buffer_desc(&set, write->pBufferInfo + j, write->dstBinding,
                              write->dstArrayElement + j);
         }
         break;

      default:
         break;
      }
   }
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateDescriptorPool(VkDevice _device,
                          const VkDescriptorPoolCreateInfo *pCreateInfo,
                          const VkAllocationCallbacks *pAllocator,
                          VkDescriptorPool *pDescriptorPool)
{
   VK_FROM_HANDLE(agxv_device, device, _device);

   VK_MULTIALLOC(ma);
   VK_MULTIALLOC_DECL(&ma, struct agxv_descriptor_pool, pool, 1);
   VK_MULTIALLOC_DECL(&ma, struct agxv_descriptor_pool_entry, entries,
                      pCreateInfo->maxSets);

   pool = vk_object_multizalloc(&device->vk, &ma, pAllocator,
                                VK_OBJECT_TYPE_DESCRIPTOR_POOL);

   if (!pool)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   size_t size = 0;
   for (unsigned i = 0; i < pCreateInfo->poolSizeCount; i++) {
      const VkDescriptorPoolSize *pool_size = &pCreateInfo->pPoolSizes[i];
      size += agxv_binding_stride(pool_size->type) * pool_size->descriptorCount;
   }

   pool->bo = NULL;
   pool->size = size;
   pool->max_entry_count = pCreateInfo->maxSets;

   if (size > 0) {
      pool->bo = agx_bo_create(&device->pdev->dev, size, AGX_BO_WRITEBACK,
                               "Descriptor Pool");
      if (!pool->bo) {
         vk_object_free(&device->vk, pAllocator, pool);
         return vk_error(device, VK_ERROR_OUT_OF_DEVICE_MEMORY);
      }
   }

   *pDescriptorPool = agxv_descriptor_pool_to_handle(pool);
   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_DestroyDescriptorPool(VkDevice _device, VkDescriptorPool _pool,
                           const VkAllocationCallbacks *pAllocator)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_descriptor_pool, pool, _pool);

   if (!_pool)
      return;

   // TODO: cleanup set entries XXX leak
   if (pool->bo) {
      agx_bo_unreference(pool->bo);
   }
   vk_object_free(&device->vk, pAllocator, pool);
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_ResetDescriptorPool(VkDevice _device, VkDescriptorPool descriptorPool,
                         VkDescriptorPoolResetFlags flags)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_descriptor_pool, pool, descriptorPool);

   for (uint32_t i = 0; i < pool->entry_count; i++) {
      agxv_descriptor_set_destroy(device, pool, pool->entries[i].set, false);
   }

   pool->entry_count = 0;
   pool->current_offset = 0;

   return VK_SUCCESS;
}

static void
agxv_descriptor_set_write_template(
   struct agxv_descriptor_set *set,
   const struct vk_descriptor_update_template *template, const void *data)
{
   for (uint32_t i = 0; i < template->entry_count; i++) {
      const struct vk_descriptor_template_entry *entry = &template->entries[i];
      const struct agxv_descriptor_set_binding_layout *binding_layout =
         &set->layout->binding[entry->binding];

      switch (entry->type) {
      case VK_DESCRIPTOR_TYPE_SAMPLER:
      case VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER:
      case VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE:
         for (unsigned j = 0; j < entry->array_count; j++) {
            const VkDescriptorImageInfo *info =
               data + entry->offset + j * entry->stride;

            if ((entry->type == VK_DESCRIPTOR_TYPE_SAMPLER ||
                 entry->type == VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER) &&
                !binding_layout->immutable_samplers) {

               write_sampler_desc(set, info, entry->binding,
                                  entry->array_element + j, set->samplers);
            }

            if (entry->type == VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE ||
                entry->type == VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER) {

               write_texture_desc(set, info, entry->binding,
                                  entry->array_element + j, set->textures);
            }
         }
         break;

      case VK_DESCRIPTOR_TYPE_STORAGE_IMAGE:
         for (uint32_t j = 0; j < entry->array_count; j++) {
            const VkDescriptorImageInfo *info =
               data + entry->offset + j * entry->stride;

            write_texture_and_pbe_desc(set, info, entry->binding,
                                       entry->array_element + j, set->textures);
         }
         break;

      case VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT:
         for (uint32_t j = 0; j < entry->array_count; j++) {
            const VkDescriptorImageInfo *info =
               data + entry->offset + j * entry->stride;

            write_texture_desc(set, info, entry->binding,
                               entry->array_element + j, set->textures);
         }
         break;

      case VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER:
         for (uint32_t j = 0; j < entry->array_count; j++) {
            const VkBufferView *bview =
               data + entry->offset + j * entry->stride;

            write_buffer_view_desc(set, *bview, entry->binding,
                                   entry->array_element + j, set->textures);
         }
         break;

      case VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER:
         for (uint32_t j = 0; j < entry->array_count; j++) {
            const VkBufferView *bview =
               data + entry->offset + j * entry->stride;

            write_buffer_view_and_pbe_desc(set, *bview, entry->binding,
                                           entry->array_element + j,
                                           set->textures);
         }
         break;

      case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
      case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER:
         for (uint32_t j = 0; j < entry->array_count; j++) {
            const VkDescriptorBufferInfo *info =
               data + entry->offset + j * entry->stride;

            write_buffer_desc(set, info, entry->binding,
                              entry->array_element + j);
         }
         break;

      case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC:
      case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC:
         for (uint32_t j = 0; j < entry->array_count; j++) {
            const VkDescriptorBufferInfo *info =
               data + entry->offset + j * entry->stride;

            write_dynamic_buffer_desc(set, info, entry->binding,
                                      entry->array_element + j);
         }
         break;

      case VK_DESCRIPTOR_TYPE_INLINE_UNIFORM_BLOCK:
         write_desc(set, entry->binding, entry->array_element,
                    data + entry->offset, entry->array_count);
         break;

      default:
         break;
      }
   }
}

VKAPI_ATTR void VKAPI_CALL
agxv_UpdateDescriptorSetWithTemplate(
   VkDevice device, VkDescriptorSet descriptorSet,
   VkDescriptorUpdateTemplate descriptorUpdateTemplate, const void *pData)
{
   VK_FROM_HANDLE(agxv_descriptor_set, set, descriptorSet);
   VK_FROM_HANDLE(vk_descriptor_update_template, template,
                  descriptorUpdateTemplate);

   agxv_descriptor_set_write_template(set, template, pData);
}

void
agxv_push_descriptor_set_update_template(
   struct agxv_push_descriptor_set *push_set,
   struct agxv_descriptor_set_layout *layout,
   const struct vk_descriptor_update_template *template, const void *data)
{
   struct agxv_descriptor_set tmp_set = {
      .layout = layout,
      .bo_size = sizeof(push_set->data),
      .mapped_ptr = push_set->data,
   };
   agxv_descriptor_set_write_template(&tmp_set, template, data);
}
