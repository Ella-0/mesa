/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "util/u_dynarray.h"
#include "agxv_descriptor_set_layout.h"
#ifndef AGXV_CMD_BUFFER
#define AGXV_CMD_BUFFER 1

#include "agxv_private.h"

#include "asahi/lib/agx_tilebuffer.h"
#include "asahi/lib/pool.h"
#include "drm-uapi/asahi_drm.h"

#include "vulkan/runtime/vk_command_buffer.h"

#include "agxv_device_memory.h"
#include "agxv_limits.h"

enum agxv_job_type { AGXV_JOB_GRAPHICS, AGXV_JOB_COMPUTE };

struct agxv_barrier {
   bool compute;
   bool render;
};

struct agxv_job {
   struct list_head link;
   enum agxv_job_type type;
   struct agxv_barrier barrier;
   union {
      struct drm_asahi_cmd_compute compute_buf;
      struct drm_asahi_cmd_render render_buf;
   };
   struct drm_asahi_attachment attachments[];
};

struct agxv_descriptor_state {
   uint64_t set_vas[AGXV_MAX_SETS];

   struct agxv_descriptor_set *sets[AGXV_MAX_SETS];
   struct agx_bo *sets_bo;
   uint32_t sets_dirty;
   uint32_t sets_base;

   uint32_t ts_count;
   uint32_t ss_count;

   struct agxv_push_descriptor_set *push[AGXV_MAX_SETS];
   uint32_t push_dirty;

   struct agxv_descriptor_set_layout *pushed_layouts[AGXV_MAX_SETS];
   uint64_t pushed_textures[AGXV_MAX_SETS];
   uint64_t pushed_samplers[AGXV_MAX_SETS];
};

struct agxv_attachment {
   VkFormat vk_format;
   struct agxv_image_view *iview;

   VkResolveModeFlagBits resolve_mode;
   struct agxv_image_view *resolve_iview;
};

struct agxv_rendering_state {
   VkRenderingFlagBits flags;

   VkRect2D area;
   uint32_t layer_count;
   uint32_t view_mask;

   uint32_t color_att_count;
   struct agxv_attachment color_att[MAX_RTS];
   struct agxv_attachment depth_att;
   struct agxv_attachment stencil_att;
};

struct agxv_state {
   /* current job that's being assembled */
   struct agxv_job *job;

   struct agxv_barrier barrier;

   struct agxv_rendering_state render;

   struct agx_ptr encoder;
   /* current encoder pointer */
   uint8_t *encoder_current;

   uint8_t push[AGXV_MAX_PUSH];
   struct agx_bo *push_bo;
   uint32_t push_base;

   struct agxv_descriptor_state descriptors;

   uint64_t buffers[AGXV_MAX_VBS];
   struct agx_bo *buffers_bo;

   struct agx_tilebuffer_layout tib;
   struct agxv_pipeline *pipeline;

   /* Scissor and depth-bias descriptors, uploaded at submit time */
   struct util_dynarray scissor, depth_bias;

   uint64_t index_buffer;
   uint8_t index_size_B;

   struct {
      /* If vertex buffers have been updated, on VDM state emission we need to
       * upload buffers to a new bo and replace the buffers_bo in the job with
       * the new bo. Otherwise we use the old bo from the job initialised on
       * job creation.
       */
      bool buffers : 1;
      bool sets    : 1;
      bool push    : 1;

      /* If shader state needs updating */
      bool vs : 1;
      bool fs : 1;
      bool cs : 1;

      bool pipeline : 1;
   } dirty;
};

struct agxv_cmd_buffer {
   struct vk_command_buffer vk;

   struct agxv_device *device;

   /* allocates command buffer memory */
   struct agx_pool cmd_pool;
   struct agxv_device_memory cmd_pool_mem;
   /* allocates memory for load/store shaders */
   struct agx_pool shader_pool;
   /* allocates memory < 4GB */
   struct agx_pool pipeline_pool;

   struct agxv_state state;

   struct list_head jobs;
};

VK_DEFINE_HANDLE_CASTS(agxv_cmd_buffer, vk.base, VkCommandBuffer,
                       VK_OBJECT_TYPE_COMMAND_BUFFER)

struct agxv_job *agxv_job_create(struct agxv_cmd_buffer *cmd_buffer,
                                 enum agxv_job_type type,
                                 uint32_t attachment_count);

uint32_t agxv_emit_shader(struct agxv_cmd_buffer *cmd_buffer,
                          enum pipe_shader_type stage);

static inline struct agxv_descriptor_state *
agxv_get_descriptors_state(struct agxv_cmd_buffer *cmd,
                           VkPipelineBindPoint bind_point)
{
#if 0
   switch (bind_point) {
   case VK_PIPELINE_BIND_POINT_GRAPHICS:
      return &cmd->state.gfx.descriptors;
   case VK_PIPELINE_BIND_POINT_COMPUTE:
      return &cmd->state.cs.descriptors;
   default:
      unreachable("Unhandled bind point");
   }
#endif
   return &cmd->state.descriptors;
};

#endif
