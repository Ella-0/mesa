/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_device.h"

#include "agxv_instance.h"
#include "agxv_physical_device.h"

#include "vulkan/runtime/vk_common_entrypoints.h"
#include "vulkan/wsi/wsi_common.h"

#include "agxv_buffer.h"
#include "agxv_cmd_buffer.h"

extern const struct vk_command_buffer_ops agxv_cmd_buffer_ops;

static VkResult
cmd_bind_map_buffer(struct vk_command_buffer *vk_cmd_buffer,
                    struct vk_meta_device *meta, VkBuffer _buffer, void **map)
{
   VK_FROM_HANDLE(agxv_buffer, buffer, _buffer);
   struct agxv_cmd_buffer *cmd_buffer =
      container_of(vk_cmd_buffer, struct agxv_cmd_buffer, vk);

   // TODO: this really is not correct

   VkDeviceMemory deviceMemory;

   agxv_AllocateMemory(agxv_device_to_handle(cmd_buffer->device),
                       &(VkMemoryAllocateInfo){
                          .allocationSize = buffer->vk.size,
                       },
                       NULL, &deviceMemory);

   vk_common_BindBufferMemory(agxv_device_to_handle(cmd_buffer->device),
                              _buffer, deviceMemory, 0);

   agxv_MapMemory(agxv_device_to_handle(cmd_buffer->device), deviceMemory, 0,
                  buffer->vk.size, 0, map);

   return VK_SUCCESS;
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateDevice(VkPhysicalDevice physicalDevice,
                  const VkDeviceCreateInfo *pCreateInfo,
                  const VkAllocationCallbacks *pAllocator, VkDevice *pDevice)
{
   VK_FROM_HANDLE(agxv_physical_device, physical_device, physicalDevice);
   VkResult result = VK_ERROR_OUT_OF_HOST_MEMORY;
   struct agxv_device *device;

   device = vk_zalloc2(&physical_device->instance->vk.alloc, pAllocator,
                       sizeof(*device), 8, VK_SYSTEM_ALLOCATION_SCOPE_DEVICE);
   if (!device)
      return vk_error(physical_device, VK_ERROR_OUT_OF_HOST_MEMORY);

   struct vk_device_dispatch_table dispatch_table;
   vk_device_dispatch_table_from_entrypoints(&dispatch_table,
                                             &agxv_device_entrypoints, true);
   vk_device_dispatch_table_from_entrypoints(&dispatch_table,
                                             &wsi_device_entrypoints, false);

   result = vk_device_init(&device->vk, &physical_device->vk, &dispatch_table,
                           pCreateInfo, pAllocator);
   if (result != VK_SUCCESS)
      goto fail_alloc;

   result = vk_meta_device_init(&device->vk, &device->meta);
   if (result != VK_SUCCESS)
      goto fail_device;
   device->meta.max_bind_map_buffer_size_B = 0x1000;
   device->meta.cmd_bind_map_buffer = cmd_bind_map_buffer;

   device->pdev = physical_device;
   result = agxv_queue_init(device, &device->queue,
                            &pCreateInfo->pQueueCreateInfos[0], 0);
   if (result != VK_SUCCESS)
      goto fail_meta;

   vk_device_set_drm_fd(&device->vk, physical_device->dev.fd);

   device->vk.command_buffer_ops = &agxv_cmd_buffer_ops;
   agx_meta_init(&device->meta_cache, &physical_device->dev);

   *pDevice = agxv_device_to_handle(device);

   return VK_SUCCESS;
fail_meta:
   vk_meta_device_finish(&device->vk, &device->meta);
fail_device:
   vk_device_finish(&device->vk);
fail_alloc:
   vk_free(&device->vk.alloc, device);
   return result;
}

VKAPI_ATTR void VKAPI_CALL
agxv_DestroyDevice(VkDevice _device, const VkAllocationCallbacks *pAllocator)
{
   VK_FROM_HANDLE(agxv_device, device, _device);

   if (!device)
      return;

   agxv_queue_finish(device, &device->queue);
   vk_meta_device_finish(&device->vk, &device->meta);
   vk_device_finish(&device->vk);
   vk_free(&device->vk.alloc, device);
}
