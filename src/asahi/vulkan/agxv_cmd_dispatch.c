/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agx_helpers.h"
#include "agxv_compute_pipeline.h"
#include "agxv_private.h"

#include "agxv_cmd_buffer.h"
#include "agxv_pipeline.h"

VKAPI_ATTR void VKAPI_CALL
agxv_CmdDispatch(VkCommandBuffer commandBuffer, uint32_t groupCountX,
                 uint32_t groupCountY, uint32_t groupCountZ)
{
   /* TODO: batching dispatches together into single CDM streams */
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   struct agxv_job *job = agxv_job_create(cmd_buffer, AGXV_JOB_COMPUTE, 0);
   uint8_t *buf = cmd_buffer->state.encoder_current;

   struct agx_shader_info *info =
      &cmd_buffer->state.pipeline->stages[MESA_SHADER_COMPUTE].info;

   struct agxv_compute_pipeline *pipeline =
      agxv_as_compute_pipeline(cmd_buffer->state.pipeline);

   uint32_t size_x = groupCountX * pipeline->local_size[0];
   uint32_t size_y = groupCountY * pipeline->local_size[1];
   uint32_t size_z = groupCountZ * pipeline->local_size[2];

   if (size_x >= 1 && size_y >= 1 && size_z >= 1) {
      agxv_push(buf, CDM_HEADER, cfg) {
         cfg.mode = AGX_CDM_MODE_DIRECT;
         cfg.uniform_register_count = info->push_count;
         cfg.preshader_register_count = info->nr_preamble_gprs;

         cfg.texture_state_register_count =
            cmd_buffer->state.descriptors.ts_count;
         cfg.sampler_state_register_count = agx_translate_sampler_state_count(
            MAX2(cmd_buffer->state.descriptors.ss_count,
                 info->uses_txf ? info->txf_sampler + 1 : 0),
            false);

         cfg.pipeline = agxv_emit_shader(cmd_buffer, MESA_SHADER_COMPUTE);
      }

      agxv_push(buf, CDM_GLOBAL_SIZE, cfg) {
         cfg.x = groupCountX * pipeline->local_size[0];
         cfg.y = groupCountY * pipeline->local_size[1];
         cfg.z = groupCountZ * pipeline->local_size[2];
      }

      agxv_push(buf, CDM_LOCAL_SIZE, cfg) {
         cfg.x = pipeline->local_size[0];
         cfg.y = pipeline->local_size[1];
         cfg.z = pipeline->local_size[2];
      }

      agxv_push(buf, CDM_LAUNCH, cfg)
         ;
   }

   agxv_push(buf, CDM_STREAM_TERMINATE, cfg)
      ;

   cmd_buffer->state.job = job;
   job->compute_buf.encoder_end = (uintptr_t)buf;
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdDispatchIndirect(VkCommandBuffer commandBuffer, VkBuffer buffer,
                         VkDeviceSize offset)
{
   unreachable("stub");
}
