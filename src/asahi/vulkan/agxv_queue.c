/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_queue.h"

#include "agx_bo.h"
#include "agxv_cmd_buffer.h"
#include "agxv_device.h"
#include "agxv_physical_device.h"

#include "asahi/lib/decode.h"

#include "vk_drm_syncobj.h"

static void
agxv_process_syncs(struct drm_asahi_sync *in_syncs,
                   struct drm_asahi_sync *out_syncs, struct vk_sync_wait *waits,
                   struct vk_sync_signal *signals, uint32_t wait_count,
                   uint32_t signal_count)
{
   for (uint32_t i = 0; i < wait_count; i++) {
      /* TODO: timeline syncs */
      in_syncs[i] = (struct drm_asahi_sync){
         .sync_type = DRM_ASAHI_SYNC_SYNCOBJ,
         .handle = vk_sync_as_drm_syncobj(waits[i].sync)->syncobj,
      };
   }

   for (uint32_t i = 0; i < signal_count; i++) {
      /* TODO: timeline syncs */
      out_syncs[i] = (struct drm_asahi_sync){
         .sync_type = DRM_ASAHI_SYNC_SYNCOBJ,
         .handle = vk_sync_as_drm_syncobj(signals[i].sync)->syncobj,
      };
   }
}

static VkResult
agxv_queue_init_null(struct agxv_queue *queue)
{
   queue->null_encoder =
      agx_bo_create(&queue->device->pdev->dev, AGX_CDM_STREAM_TERMINATE_LENGTH,
                    0, "null_job");

   if (!queue->null_encoder)
      return vk_error(&queue->vk, VK_ERROR_OUT_OF_DEVICE_MEMORY);

   agx_pack(queue->null_encoder->ptr.cpu, CDM_STREAM_TERMINATE, cfg)
      ;

   queue->null_cmd = (struct drm_asahi_cmd_compute){
      .iogpu_unk_40 = 0x1c,
      .iogpu_unk_44 = 0xffffffff,
      .cmd_id = agx_get_global_id(&queue->device->pdev->dev),
      .encoder_id = agx_get_global_id(&queue->device->pdev->dev),
      .encoder_ptr = queue->null_encoder->ptr.gpu,
      .encoder_end =
         queue->null_encoder->ptr.gpu + AGX_CDM_STREAM_TERMINATE_LENGTH,
   };

   return VK_SUCCESS;
}

static VkResult
agxv_submit_null_job(struct agxv_queue *queue, struct drm_asahi_sync *in_syncs,
                     struct drm_asahi_sync *out_syncs, uint32_t in_sync_count,
                     uint32_t out_sync_count)
{
   /* Barriers are all 0 initialised to wait on all subqueues not just
    * compute otherwise a QueueWaitIdle could unblock before all render
    * commands have finished.
    */
   struct drm_asahi_command cmd = {
      .cmd_type = DRM_ASAHI_CMD_COMPUTE,
      .cmd_buffer = (uintptr_t)&queue->null_cmd,
      .cmd_buffer_size = sizeof(queue->null_cmd),
   };

   struct drm_asahi_submit submit = {
      .queue_id = queue->id,
      .command_count = 1,
      .commands = (uintptr_t)&cmd,
      .in_syncs = (uintptr_t)in_syncs,
      .out_syncs = (uintptr_t)out_syncs,
      .in_sync_count = in_sync_count,
      .out_sync_count = out_sync_count,
   };

   int fd = queue->device->pdev->dev.fd;

   int ret = drmIoctl(fd, DRM_IOCTL_ASAHI_SUBMIT, &submit);

   if (ret) {
      return vk_errorf(queue, VK_ERROR_DEVICE_LOST,
                       "DRM_IOCTL_ASAHI_SUBMIT failed: %m\n");
   }
   return VK_SUCCESS;
}

static void
agxv_queue_init_cmds(struct vk_queue_submit *queue_submit,
                     struct agxv_cmd_buffer **cmd_buffers,
                     struct drm_asahi_command *cmds,
                     uint32_t max_commands_per_submission)
{
   uint32_t cmd_idx = 0;

   uint32_t last_jobs[DRM_ASAHI_SUBQUEUE_COUNT];
   memset(last_jobs, 0, sizeof(last_jobs));

   for (uint32_t j = 0; j < queue_submit->command_buffer_count; j++) {
      uint32_t barriers[DRM_ASAHI_SUBQUEUE_COUNT];
      for (uint32_t sq_idx = 0; sq_idx < DRM_ASAHI_SUBQUEUE_COUNT; sq_idx++) {
         barriers[sq_idx] = DRM_ASAHI_BARRIER_NONE;
      }

      struct agxv_cmd_buffer *cmd_buffer = cmd_buffers[j];
      list_for_each_entry_safe(struct agxv_job, job, &cmd_buffer->jobs, link) {
         if (job->barrier.compute) {
            barriers[DRM_ASAHI_SUBQUEUE_COMPUTE] =
               last_jobs[DRM_ASAHI_SUBQUEUE_COMPUTE];
         }

         if (job->barrier.render) {
            barriers[DRM_ASAHI_SUBQUEUE_RENDER] =
               last_jobs[DRM_ASAHI_SUBQUEUE_RENDER];
         }

         memcpy(cmds[cmd_idx].barriers, barriers, sizeof(barriers));

         switch (job->type) {
         case AGXV_JOB_GRAPHICS: {
            cmds[cmd_idx].cmd_type = DRM_ASAHI_CMD_RENDER;
            cmds[cmd_idx].cmd_buffer = (uintptr_t)&job->render_buf;
            cmds[cmd_idx].cmd_buffer_size = sizeof(job->render_buf);
            last_jobs[DRM_ASAHI_SUBQUEUE_RENDER]++;

            if (cmd_buffer->device->pdev->dev.debug & AGX_DBG_TRACE) {
               agxdecode_drm_cmd_render(&cmd_buffer->device->pdev->dev.params,
                                        &job->render_buf, true);
               agxdecode_next_frame();
            }
            break;
         }
         case AGXV_JOB_COMPUTE: {
            cmds[cmd_idx].cmd_type = DRM_ASAHI_CMD_COMPUTE;
            cmds[cmd_idx].cmd_buffer = (uintptr_t)&job->compute_buf;
            cmds[cmd_idx].cmd_buffer_size = sizeof(job->compute_buf);
            last_jobs[DRM_ASAHI_SUBQUEUE_COMPUTE]++;

            if (cmd_buffer->device->pdev->dev.debug & AGX_DBG_TRACE) {
               agxdecode_drm_cmd_compute(&cmd_buffer->device->pdev->dev.params,
                                         &job->compute_buf, true);
               agxdecode_next_frame();
            }
            break;
         }
         default:
            unreachable("invalid job type");
         }

         cmd_idx++;
         if (cmd_idx % max_commands_per_submission == 0) {
            for (uint32_t sq_idx = 0; sq_idx < DRM_ASAHI_SUBQUEUE_COUNT;
                 sq_idx++) {

               /* If there's been a pipeline barrier in this command buffer
                * before the submission split, we need to specify that jobs
                * after the submission split depend on previous jobs.
                */
               if (barriers[sq_idx] != DRM_ASAHI_BARRIER_NONE)
                  barriers[sq_idx] = 0;

               last_jobs[sq_idx] = 0;
            }
         }
      };
   }
}

static void
agxv_queue_assemble_submit_chain()
{
}

static VkResult
submit_cmd_buffers(struct agxv_queue *queue,
                   struct vk_queue_submit *queue_submit)
{
   uint32_t cmd_count = 0;
   struct agxv_cmd_buffer *cmd_buffers[queue_submit->command_buffer_count];
   for (unsigned i = 0; i < queue_submit->command_buffer_count; i++) {
      struct agxv_cmd_buffer *cmd_buffer = container_of(
         queue_submit->command_buffers[i], struct agxv_cmd_buffer, vk);
      cmd_count += list_length(&cmd_buffer->jobs);
      cmd_buffers[i] = cmd_buffer;
   }

   struct drm_asahi_sync in_syncs[queue_submit->wait_count];
   struct drm_asahi_sync out_syncs[queue_submit->signal_count];
   memset(in_syncs, 0, sizeof(in_syncs));
   memset(out_syncs, 0, sizeof(out_syncs));
   agxv_process_syncs(in_syncs, out_syncs, queue_submit->waits,
                      queue_submit->signals, queue_submit->wait_count,
                      queue_submit->signal_count);

   /* Submitting 0 commands in a submit is not valid. as such we need to
    * submit a job that does nothing to signal any sync objects
    */
   if (!cmd_count) {
      return agxv_submit_null_job(queue, in_syncs, out_syncs,
                                  queue_submit->wait_count,
                                  queue_submit->signal_count);
   }

   struct drm_asahi_params_global *params = &queue->device->pdev->dev.params;

   uint32_t submit_count =
      DIV_ROUND_UP(cmd_count, params->max_commands_per_submission);

   struct drm_asahi_command cmds[cmd_count];
   memset(cmds, 0, sizeof(cmds));
   agxv_queue_init_cmds(queue_submit, cmd_buffers, cmds,
                        params->max_commands_per_submission);

   for (uint32_t i = 0; i < submit_count; i++) {
      struct drm_asahi_submit submit = {
         .queue_id = queue->id,
         .command_count = cmd_count,
         .commands = (uintptr_t)cmds,
         .in_syncs = (uintptr_t)in_syncs,
         .out_syncs = (uintptr_t)out_syncs,
         .in_sync_count = queue_submit->wait_count,
         .out_sync_count = submit_count == 1 ? queue_submit->signal_count : 0,
      };
      int fd = queue->device->pdev->dev.fd;

      int ret = drmIoctl(fd, DRM_IOCTL_ASAHI_SUBMIT, &submit);

      if (ret) {
         return vk_errorf(queue, VK_ERROR_DEVICE_LOST,
                          "DRM_IOCTL_ASAHI_SUBMIT failed: %m\n");
      }
   }

   if (submit_count > 1) {
      agxv_submit_null_job(queue, in_syncs, out_syncs, queue_submit->wait_count,
                           queue_submit->signal_count);
   }
   return VK_SUCCESS;
}

static VkResult
agxv_queue_submit(struct vk_queue *vk_queue, struct vk_queue_submit *submit)
{
   struct agxv_queue *queue = container_of(vk_queue, struct agxv_queue, vk);
   struct agx_device *dev = &queue->device->pdev->dev;

   VkResult result = VK_SUCCESS;

   result = submit_cmd_buffers(queue, submit);

   if (result != VK_SUCCESS)
      return result;

   return VK_SUCCESS;
}

VkResult
agxv_queue_init(struct agxv_device *device, struct agxv_queue *queue,
                const VkDeviceQueueCreateInfo *pCreateInfo,
                uint32_t index_in_family)
{
   VkResult result;

   result =
      vk_queue_init(&queue->vk, &device->vk, pCreateInfo, index_in_family);

   if (result != VK_SUCCESS)
      return result;

   queue->vk.driver_submit = agxv_queue_submit;
   queue->device = device;

   queue->id = agx_create_command_queue(
      &device->pdev->dev,
      DRM_ASAHI_QUEUE_CAP_RENDER | DRM_ASAHI_QUEUE_CAP_COMPUTE);

   result = agxv_queue_init_null(queue);
   if (result != VK_SUCCESS) {
      vk_queue_finish(&queue->vk);
      return result;
   }

   return VK_SUCCESS;
}

void
agxv_queue_finish(struct agxv_device *device, struct agxv_queue *queue)
{
   struct drm_asahi_queue_destroy queue_destroy = {.queue_id = queue->id};
   drmIoctl(device->pdev->dev.fd, DRM_ASAHI_QUEUE_DESTROY, &queue_destroy);
   agx_bo_unreference(queue->null_encoder);
   vk_queue_finish(&queue->vk);
}
