/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

#include "agxv_query_pool.h"

#include "agxv_device.h"
#include "agxv_physical_device.h"

#include "asahi/lib/agx_bo.h"

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateQueryPool(VkDevice _device, const VkQueryPoolCreateInfo *pCreateInfo,
                     const VkAllocationCallbacks *pAllocator,
                     VkQueryPool *pQueryPool)
{
   VK_FROM_HANDLE(agxv_device, device, _device);

   struct agxv_query_pool *pool = vk_object_zalloc(
      &device->vk, pAllocator, sizeof(*pool), VK_OBJECT_TYPE_QUERY_POOL);
   if (!pool)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   pool->bo = agx_bo_create(&device->pdev->dev,
                            pCreateInfo->queryCount * sizeof(uint64_t), 0,
                            "Query Pool");

   if (!pool->bo) {
      vk_object_free(&device->vk, pAllocator, pool);
      return vk_error(device, VK_ERROR_OUT_OF_DEVICE_MEMORY);
   }

   *pQueryPool = agxv_query_pool_to_handle(pool);

   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_DestroyQueryPool(VkDevice _device, VkQueryPool queryPool,
                      const VkAllocationCallbacks *pAllocator)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_query_pool, pool, queryPool);

   if (!pool)
      return;

   agx_bo_unreference(pool->bo);
   vk_object_free(&device->vk, pAllocator, pool);
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_GetQueryPoolResults(VkDevice _device, VkQueryPool queryPool,
                         uint32_t firstQuery, uint32_t queryCount,
                         size_t dataSize, void *pData, VkDeviceSize stride,
                         VkQueryResultFlags flags)
{
   unreachable("stub");
}
